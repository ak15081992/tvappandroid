package com.app.couchpotato.dagger.component

import com.app.couchpotato.helperutils.ApiHelper
import com.app.couchpotato.dagger.AppScope
import com.app.couchpotato.dagger.module.ModuleApiInterface

import dagger.Component

@AppScope
@Component(modules = [ModuleApiInterface::class])
interface AppComponents {

    fun getApiHelper(): ApiHelper
}