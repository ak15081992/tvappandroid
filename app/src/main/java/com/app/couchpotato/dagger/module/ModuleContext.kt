package com.app.couchpotato.dagger.module

import android.content.Context
import com.app.couchpotato.dagger.AppScope

import javax.inject.Named

import dagger.Module
import dagger.Provides

@Module
class ModuleContext(private val mContext: Context) {

    @Named("application_context")
    @AppScope
    @Provides
    fun context(): Context {
        return mContext.applicationContext
    }
}