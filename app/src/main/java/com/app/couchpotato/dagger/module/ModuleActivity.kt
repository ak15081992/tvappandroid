package com.app.couchpotato.dagger.module

import android.app.Activity
import android.content.Context
import com.app.couchpotato.dagger.AppScope

import javax.inject.Named

import dagger.Module
import dagger.Provides

@Module
class ModuleActivity(private val mContext: Activity) {

    @Named("activity_context")
    @AppScope
    @Provides
    fun context(): Context {
        return mContext
    }
}