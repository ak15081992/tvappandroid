package com.app.couchpotato.helperutils

import com.app.couchpotato.commonmodel.CategoryList
import com.app.couchpotato.commonmodel.UserModel
import com.app.couchpotato.ui.demanddetail.model.ResponseVideoDetail
import com.app.couchpotato.ui.moviesondemand.model.ResponseMovieCategory
import com.app.couchpotato.ui.moviesondemand.model.ResponseMovies
import com.app.couchpotato.ui.season.model.ResponseSeasonList
import com.app.couchpotato.ui.tvondemand.model.ResponseTVSeries
import com.app.couchpotato.ui.tvondemand.model.ResponseTVSeriesCategoryList
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface ApiHelper {
    /**
     * USername -bluestone
     * password - bluestone
     */

    @POST("player_api.php")
    @FormUrlEncoded
    fun login(
        @Field("username") username: String,
        @Field("password") password: String
    ): Call<UserModel>

    @POST("player_api.php")
    @FormUrlEncoded
    fun getLiveCategories(
        @Field("username") username: String,
        @Field("password") password: String,
        @Field("action") action: String = "get_live_categories"
    ): Call<ArrayList<CategoryList>>

    @POST("xmltv.php")
    @FormUrlEncoded
    fun getAllEPGDate(
        @Field("username") username: String,
        @Field("password") password: String
    ): Call<ResponseBody>

    @POST("player_api.php")
    @FormUrlEncoded
    fun getChannelData(
        @Field("username") username: String,
        @Field("password") password: String,
        @Field("action") action: String = "get_live_streams"
    ): Call<ResponseBody>

    @POST("player_api.php")
    @FormUrlEncoded
    fun getMoviesOnDemandCategoryList(
        @Field("username") username: String,
        @Field("password") password: String,
        @Field("action") action: String = "get_vod_categories"
    ): Call<ArrayList<ResponseMovieCategory>>

    @POST("player_api.php")
    @FormUrlEncoded
    fun getMoviesList(
        @Field("username") username: String,
        @Field("password") password: String,
        @Field("action") action: String = "get_vod_streams"
    ): Call<ArrayList<ResponseMovies>>

    @POST("player_api.php")
    @FormUrlEncoded
    fun getSeriesOnDemandCategoryList(
        @Field("username") username: String,
        @Field("password") password: String,
        @Field("action") action: String = "get_series_categories"
    ): Call<ArrayList<ResponseTVSeriesCategoryList>>

    @POST("player_api.php")
    @FormUrlEncoded
    fun getSeriesList(
        @Field("username") username: String,
        @Field("password") password: String,
        @Field("action") action: String = "get_series"
    ): Call<ArrayList<ResponseTVSeries>>

    @POST("player_api.php")
    @FormUrlEncoded
    fun getVideoInfo(
        @Field("username") username: String,
        @Field("password") password: String,
        @Field("action") action: String = "get_vod_info",
        @Field("vod_id") videoId: String?=null
    ): Call<ResponseVideoDetail>

    @POST("player_api.php")
    @FormUrlEncoded
    fun getSeasonList(
        @Field("username") username: String,
        @Field("password") password: String,
        @Field("action") action: String = "get_series_info",
        @Field("series_id") seriesId: String?=null
    ): Call<ResponseSeasonList>

    @POST("player_api.php")
    @FormUrlEncoded
    fun getSeasonListResponseBody(
        @Field("username") username: String,
        @Field("password") password: String,
        @Field("action") action: String = "get_series_info",
        @Field("series_id") seriesId: String?=null
    ): Call<ResponseBody>

}
