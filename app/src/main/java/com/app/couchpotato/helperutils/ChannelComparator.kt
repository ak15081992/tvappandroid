package com.app.couchpotato.helperutils

import com.app.couchpotato.commonmodel.ChannelItem
import java.util.*

class ChannelComparator : Comparator<ChannelItem> {

    override fun compare(left: ChannelItem, right: ChannelItem): Int {
        return if (Utils.getUtils().getBoolean(Constants.ORDER_BY_NUMBER)
        ) {
            when {
                left.m_sTvNum!!.isEmpty() -> -1
                right.m_sTvNum!!.isEmpty() -> 1
                else -> Integer.compare(
                    left.m_sTvNum!!.toInt(),
                    right.m_sTvNum!!.toInt()
                )
            }
        } else {
            left.m_sTvName!!.compareTo(right.m_sTvName!!)
        }
    }
}