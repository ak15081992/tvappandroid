package com.app.couchpotato.helperutils

class Constants {

    companion object {
        const val COUCH_POTATO_PREF="COUCH_POTATO_PREF"
        const val TYPE_ADD=1
        const val TYPE_REPLACE=2
        const val KEY_USERMODEL="KEY_USERMODEL"
        const val KEY_MOVIE_ON_DEMAND_CATEGORY_LIST="KEY_MOVIE_ON_DEMAND_CATEGORY_LIST"
        const val KEY_SERIES_ON_DEMAND_CATEGORY_LIST="KEY_SERIES_ON_DEMAND_CATEGORY_LIST"
        const val KEY_MOVIE_ON_DEMAND_LIST="KEY_MOVIE_ON_DEMAND_LIST"
        const val PARENTAL_LOCK = "ParentalLock"
        const val HARDWARE_ACCELERATION="HARDWARE_ACCELERATION"
        const val FAVORITE_ARRAY="FAVORITE_ARRAY"
        const val KEY_TV_SERIES="KEY_TV_SERIES"
        const val KEY_SEASON="KEY_SEASON"
        const val KEY_SERIES_ID="KEY_SERIES_ID"
        const val KEY_SEASON_NO="KEY_SEASON_NO"

        // for Stream
        const val ITEM_STREAM_NUM = "num"
        const val ITEM_NAME = "name"
        const val ITEM_STREAM_TYPE = "stream_type"
        const val ITEM_STREAM_ID = "stream_id"
        const val ITEM_STREAM_ICON = "stream_icon"
        const val ITEM_EPG_CHANNEL_ID = "epg_channel_id"
        const val ITEM_ADDED = "added"
        const val ITEM_CUSTOM_SID = "custom_sid"
        const val ITEM_TV_ARCHIVE = "tv_archive"
        const val ITEM_DIRECT_SOURCE = "direct_source"
        const val ITEM_TV_ARCHIVE_DURATION = "tv_archive_duration"
        // for Stream

        /**
         * On Demand
         */
        const val KEY_VIDEOINDEX="KEY_VIDEOINDEX"
        const val KEY_VIDEO_ID="KEY_VIDEO_ID"

        const val ITEM_CATEGORY_ID = "category_id"
        const val ORDER_BY_NUMBER="ORDER_BY_NUMBER"

        const val ENLARGE = "Enlarge"

        private val MAX_HEAP_SIZE = Runtime.getRuntime().maxMemory().toInt()
        const val MAX_DISK_CACHE_SIZE = 40 * 1024 * 1024
        val MAX_MEMORY_CACHE_SIZE = MAX_HEAP_SIZE / 4

        var UPDATE_TIME_DELAY = 500
        const val RECONNECT_NEXT_TIME_OUT = 5000
        const val SEEK_OFFSET = 10000
    }
}