package com.app.couchpotato.helperutils;

import com.app.couchpotato.commonmodel.ItemTopic;

import java.util.Comparator;

public class TopicComparator implements Comparator<ItemTopic>
{
    public int compare(ItemTopic left, ItemTopic right) {
        return Long.compare(left.m_dateTopicStart.getTime(), right.m_dateTopicStart.getTime());
    }
}
