package com.app.couchpotato.helperutils

import android.annotation.SuppressLint
import android.app.Activity
import android.app.ActivityManager
import android.content.Context
import android.content.Context.ACTIVITY_SERVICE
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.content.res.Resources
import android.graphics.*
import android.media.ExifInterface
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.Uri
import android.provider.MediaStore
import android.text.TextUtils
import android.util.Base64
import android.util.DisplayMetrics
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityOptionsCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.app.couchpotato.MyApp
import com.app.couchpotato.R
import com.app.couchpotato.commonmodel.UserModel
import com.app.couchpotato.dagger.module.ModuleApiInterface
import com.app.couchpotato.image.RoundRectTransform
import com.google.android.material.snackbar.Snackbar
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import com.tapadoo.alerter.Alerter
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.*
import java.net.HttpURLConnection
import java.net.URL
import java.nio.ByteBuffer
import java.security.KeyManagementException
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.security.SecureRandom
import java.security.cert.X509Certificate
import java.sql.Timestamp
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import javax.net.ssl.HttpsURLConnection
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager

class Utils {

    var AdultLiveCategoryNumber = ""

    /**
     * Validations Helper Starts
     */
    fun isCommonInputLayoutValidated(tie: EditText, layout: View, error: String): Boolean {
        if (tie.text.trim().isEmpty()) {
            tie.requestFocus()
            showSnackBar(layout, tie.context.resources.getColor(R.color.colorRedError), error)
            return false
        }
        return true
    }

    fun convertTime(time: Long): String? {
        val timestamp = Timestamp(time * 1000)
        val date = Date(timestamp.time)
        @SuppressLint("SimpleDateFormat") val format: DateFormat =
            SimpleDateFormat("dd/MM/yyyy")
        return format.format(date)
    }

    fun UTCStringToLocalString(strTime: String?): String? {
        @SuppressLint("SimpleDateFormat") val df =
            SimpleDateFormat("yyyyMMddHHmmss Z")
        return try {
            val date = df.parse(strTime)
            df.timeZone = TimeZone.getDefault()
            df.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
            null
        }
    }

    fun makeAvaiableUrl(url: String): String? {
        var retUrl = url.replace(" ".toRegex(), "%20")
        if (!retUrl.contains("https://")) retUrl = retUrl.replace("https:/".toRegex(), "https://")
        if (!retUrl.contains("http://")) retUrl = retUrl.replace("http:/".toRegex(), "http://")
        return retUrl
    }

    fun getSHA1String(input: String): String {
        val HEX_CHARS = "0123456789ABCDEF"
        val bytes = MessageDigest
            .getInstance("SHA-1")
            .digest(input.toByteArray())
        val result = StringBuilder(bytes.size * 2)
        bytes.forEach {
            val i = it.toInt()
            result.append(HEX_CHARS[i shr 4 and 0x0f])
            result.append(HEX_CHARS[i and 0x0f])
        }

        return result.toString().toLowerCase()
    }

    fun showAlerter(mAlerter: Alerter, color: Int, icon: Int, message: String) {
        if (Alerter.isShowing) {
            Alerter.hide()
        }
        mAlerter.apply {
            setIcon(icon)
            setText(message)
            setDuration(1500)
            setBackgroundColorRes(color)
            show()
        }
    }

    fun isEmailInputLayoutvalidated(
        context: Context, layout: View,
        tie: EditText
    ): Boolean {
        if (!isCommonInputLayoutValidated(
                tie, layout,
                context.resources.getString(R.string.error_email)
            )
        ) {
            return false
        }
        if (!isValidEmail(tie.text!!.trim())) {
            showSnackBar(
                layout, context.resources.getColor(R.color.colorRedError),
                context.resources.getString(R.string.error_email)
            )
            tie.requestFocus()
            return false
        }
        return true
    }

    fun getDateWithServerTimeStamp(input: String): Date? {
        val dateFormat = SimpleDateFormat(
            "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",
            Locale.getDefault()
        )
        dateFormat.timeZone = TimeZone.getTimeZone(TimeZone.getDefault().displayName)
        return try {
            dateFormat.parse(input)
        } catch (e: ParseException) {
            null
        }
    }

    fun getStringTimeStampWithDate(input: String): String {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",
            Locale.getDefault())
        dateFormat.timeZone = TimeZone.getTimeZone(TimeZone.getDefault().displayName)
        return dateFormat.format(input)
    }

    /**
     * returns 01 Jan 2020
     */
    fun getRequiredDateFormatForAnnouncement(utc: String): String {
        val mUtc=getDateWithServerTimeStamp(utc)
        return (mUtc?.toGMTString()?.split(" ")?.get(0)) +" "+(mUtc?.toGMTString()?.split(" ")?.get(1))+" "+(mUtc?.toGMTString()?.split(" ")?.get(2))
    }


    fun isValidEmail(target: CharSequence): Boolean {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches()
    }
    /**
     * Validation Helper Ends
     */

    /**
     * Utils Methods
     */
    fun getUniqueID(): String {
        return UUID.randomUUID().toString()
    }

    /**
     * Shared Pref Helper Starts
     */
    private var mEditor: SharedPreferences.Editor? = null

    fun setString(key: String, value: String) {
        mEditor = MyApp.mSharedPref.edit()
        mEditor?.putString(key, value)
        mEditor?.apply()
    }

    fun setBoolean(key: String, value: Boolean) {
        mEditor = MyApp.mSharedPref.edit()
        mEditor?.putBoolean(key, value)
        mEditor?.apply()
    }

    fun getString(key: String): String? {
        return MyApp.mSharedPref.getString(key, "null")
    }

    fun getBoolean(key: String): Boolean {
        return MyApp.mSharedPref.getBoolean(key, true)
    }

    fun setInt(key: String, value: Int) {
        mEditor = MyApp.mSharedPref.edit()
        mEditor?.putInt(key, value)
        mEditor?.apply()
    }

    fun getFloat(key: String): Float? {
        return MyApp.mSharedPref.getFloat(key, 404f)
    }

    fun setFloat(key: String, value: Float) {
        mEditor = MyApp.mSharedPref.edit()
        mEditor?.putFloat(key, value)
        mEditor?.apply()
    }

    fun getInt(key: String): Int? {
        return MyApp.mSharedPref.getInt(key, 404)
    }

    fun clearSharedPref() {
        mEditor = MyApp.mSharedPref.edit()
        mEditor?.clear()
        mEditor?.apply()
    }
    /**
     * Shared Pref Helper Ends
     */

    /**
     * Runtime Permission Helper Starts
     */
    fun isPermissionGranted(mActivity: Activity, mPermission: String): Boolean {
        if (ContextCompat.checkSelfPermission(
                mActivity, mPermission
            )
            != PackageManager.PERMISSION_GRANTED
        ) {
            return false
        }
        return true
    }

    fun requestPermission(mActivity: Activity, mPermission: String, requestCode: Int) {
        ActivityCompat.requestPermissions(
            mActivity,
            arrayOf(mPermission), requestCode
        )
    }
    /**
     * Runtime Permission Helper Ends
     */

    /**
     * Activity Intent Fire Helper Starts
     */
    fun fireActivityIntent(
        sourceActivity: Activity,
        mIntent: Intent,
        isFinish: Boolean,
        isForward: Boolean
    ) {
        sourceActivity.startActivity(mIntent)
        if (isFinish) {
            sourceActivity.finish()
        }
        if (isForward) {
            sourceActivity.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left)
        } else {
            sourceActivity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right)
        }
    }

    fun fireActivityWithTransition(
        sourceActivity: Activity,
        mIntent: Intent,
        view: View,
        tag: String
    ) {
        sourceActivity.startActivity(
            mIntent,
            ActivityOptionsCompat.makeSceneTransitionAnimation(
                sourceActivity,
                view,
                tag
            ).toBundle()
        )
    }

    fun finishActivity(
        sourceActivity: Activity,
        isForward: Boolean
    ) {
        sourceActivity.finish()
        if (isForward) {
            sourceActivity.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left)
        } else {
            sourceActivity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right)
        }
    }

    fun isLastActivity(mContext: Context, className: String): Boolean {
        val mngr = mContext.getSystemService(ACTIVITY_SERVICE) as ActivityManager
        val taskList = mngr.getRunningTasks(10)

        if (taskList[0].numActivities == 1 && taskList[0].topActivity?.className == className) {
            return true
        }
        return false
    }

    fun isStackContainsActivity(mContext: Context, className: String): Boolean {
        val mngr = mContext.getSystemService(ACTIVITY_SERVICE) as ActivityManager
        val taskList = mngr.getRunningTasks(10)

        for (items in taskList) {
            if (items.baseActivity?.className == className) {
                return true
            }
        }
        return false
    }

    /**
     * Service Helper Methods Starts
     */
    fun isServiceRunning(mContext: Context, serviceClassName: String): Boolean {
        val manager = mContext.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for (service in manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClassName == service.service.className) {
                return true
            }
        }
        return false
    }

    fun fireBrowser(mContext: Context, url: String) {
        mContext.startActivity(Intent(Intent.ACTION_VIEW).setData(Uri.parse(url)))
    }


    /**
     * Fragment Helper Starts
     */
    lateinit var fragmentTransaction: FragmentTransaction
    /**
     * @currentFragment To check Current Fragment
     */
    lateinit var currentFragment: Fragment

    fun fragTransition(
        fragmentManager: FragmentManager, frame: Int, fragment: Fragment,
        type: Int, tag: String, addtobackstack: Boolean
    ) {
        if (fragmentManager.findFragmentById(frame) != null) {
            currentFragment = fragmentManager.findFragmentById(frame)!!
            if (currentFragment.tag!!.toString().equals(tag)) {
                return@fragTransition
            }
        }
        if (fragmentManager.backStackEntryCount > 0) {
            for (i in 0 until fragmentManager.getBackStackEntryCount()) {
                fragmentManager.popBackStack()
            }
        }
        fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.setCustomAnimations(
            android.R.animator.fade_in,
            android.R.animator.fade_out
        )
        if (type == Constants.TYPE_ADD) {
            fragmentTransaction.add(frame, fragment, tag)
        }
        if (type == Constants.TYPE_REPLACE) {
            fragmentTransaction.replace(frame, fragment, tag)
        }
        fragmentTransaction.commit()
    }

    fun addAndHideFragment(
        frame: Int,
        fragment: Fragment,
        fragmentManager: FragmentManager,
        tag: String
    ) {
        fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.add(frame, fragment, tag)
        fragmentTransaction.hide(fragment)
        fragmentTransaction.commitAllowingStateLoss()
    }

    fun showFragment(fragment: Fragment, fragmentManager: FragmentManager) {
        fragmentTransaction = fragmentManager.beginTransaction()
        /*fragmentTransaction.setCustomAnimations(
                android.R.animator.fade_in,
                android.R.animator.fade_out
        )*/
        fragmentTransaction.show(fragment)
        fragmentTransaction.commitAllowingStateLoss()
    }

    fun hideFragment(fragment: Fragment, fragmentManager: FragmentManager) {
        fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.hide(fragment)
        fragmentTransaction.commitAllowingStateLoss()
    }
    /**
     * Fragment Helper Ends
     */

    /**
     * Conversion Helper Starts
     */
    fun bitmapToBase64(bmp: Bitmap): String? {
        bmp.compress(Bitmap.CompressFormat.PNG, 100, bos)
        byteArray = bos.toByteArray()
        return Base64.encodeToString(byteArray, Base64.DEFAULT)

    }

    fun getRoundedCornerBitmap(bitmap: Bitmap, pixel: Int): Bitmap {
        val output = Bitmap.createBitmap(
            bitmap.width, bitmap
                .height, Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(output)
        val color = 0xff424242
        val paint = Paint()
        val rect = Rect(0, 0, bitmap.width, bitmap.height)
        val rectF = RectF(rect)
        paint.isAntiAlias = true
        canvas.drawARGB(0, 0, 0, 0)
        paint.color = color.toInt()
        canvas.drawRoundRect(rectF, pixel.toFloat(), pixel.toFloat(), paint)
        paint.xfermode = PorterDuffXfermode(PorterDuff.Mode.SRC_IN)
        canvas.drawBitmap(bitmap, rect, rect, paint)
        return output
    }

    private var byteArray: ByteArray? = null
    fun base64ToBitmap(base64: String): Bitmap? {
        byteArray = Base64.decode(base64, Base64.DEFAULT)
        return BitmapFactory.decodeByteArray(byteArray, 0, byteArray?.size!!)
    }

    fun isBitmapSame(bitmap1: Bitmap, bitmap2: Bitmap): Boolean {
        val buffer1 = ByteBuffer.allocate(bitmap1.getHeight() * bitmap1.getRowBytes());
        bitmap1.copyPixelsToBuffer(buffer1);

        val buffer2 = ByteBuffer.allocate(bitmap2.getHeight() * bitmap2.getRowBytes());
        bitmap2.copyPixelsToBuffer(buffer2);

        return Arrays.equals(buffer1.array(), buffer2.array());
    }

    private var displayMetrics: DisplayMetrics? = null
    fun dpToPx(dp: Int, resources: Resources): Int {
        displayMetrics = resources.getDisplayMetrics()
        return Math.round(dp * (displayMetrics?.xdpi!! / DisplayMetrics.DENSITY_DEFAULT))
    }

    private var f: File? = null
    private var bos: ByteArrayOutputStream = ByteArrayOutputStream()
    private var bitmapdata: ByteArray? = null
    private var fos: FileOutputStream? = null
    fun FileFromBitmap(mContext: Context, bitmap: Bitmap): File {
        try {
            f = File(mContext.cacheDir, "profile_" + System.currentTimeMillis());
            f?.createNewFile();
            bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos)
            bitmapdata = bos.toByteArray()
            fos = FileOutputStream(f)
            fos?.write(bitmapdata)
            fos?.flush()
            fos?.close()

            return f as File
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return f as File
    }

    private var filePart: MultipartBody.Part? = null
    fun MultipartFromFile(file: File, filename: String): MultipartBody.Part {
        filePart = MultipartBody.Part.createFormData(
            filename,
            file.name,
            RequestBody.create(
                MediaType.parse("image/*"),
                file
            )
        )
        return filePart as MultipartBody.Part
    }

    fun stringToRequestBody(input: String): RequestBody {
        return RequestBody.create(MediaType.parse("text/plain"), input)
    }

    private var connectivityManager: ConnectivityManager? = null
    fun isOnline(mActivity: Activity): Boolean {
        connectivityManager = mActivity.getSystemService(
            Context
                .CONNECTIVITY_SERVICE
        ) as ConnectivityManager
        val info = connectivityManager?.getAllNetworkInfo();
        if (info != null) {
            for (i in info) {
                if (i.state == NetworkInfo.State.CONNECTED) {
                    return true
                }
            }
        }
        return false
    }

    lateinit var mUrl: URL
    private var mHTTPURLConn: HttpURLConnection? = null
    private var mInputStream: InputStream? = null
    fun bitmapFromURLString(url: String): Bitmap? {
        Thread().run {
            return try {
                mUrl = URL(url)
                mHTTPURLConn = mUrl.openConnection() as HttpURLConnection?;
                mHTTPURLConn?.setDoInput(true);
                mHTTPURLConn?.connect();
                mInputStream = mHTTPURLConn?.getInputStream();
                BitmapFactory.decodeStream(mInputStream);
            } catch (e: IOException) {
                null
            }
        }
    }
    /**
     * Conversion Helper Ends
     */

    /**
     * UI Helper Starts
     */
    private var snackbar: Snackbar? = null

    fun showSnackBar(layout: View, color: Int, error: String) {
        snackbar = Snackbar.make(
            layout,
            error,
            Snackbar.LENGTH_SHORT
        )
        snackbar?.view?.setBackgroundColor(color)
        val tv =
            snackbar?.view?.findViewById<TextView>(com.google.android.material.R.id.snackbar_text) as TextView
        //tv.typeface = MyApp.fontNanumGothicRegular
        tv.maxLines = 4
        snackbar?.show()
    }

    /**
     * return height of statusbar
     */
    fun getStatusBarHeight(mContext: Context): Int {
        var result = 0
        val resourceId = mContext.resources.getIdentifier("status_bar_height", "dimen", "android")
        if (resourceId > 0) {
            result = mContext.resources.getDimensionPixelSize(resourceId)
        }
        return result
    }

    fun setMargins(view: View, left: Int, top: Int, right: Int, bottom: Int) {
        if (view.layoutParams is ViewGroup.MarginLayoutParams) {
            val p = view.layoutParams as ViewGroup.MarginLayoutParams
            p.setMargins(left, top, right, bottom)
            view.requestLayout()
        }
    }

    private var imm: InputMethodManager? = null
    fun hideKeyboard(activity: Activity) {
        imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        var view = activity.currentFocus
        if (view == null) {
            view = View(activity)
        }
        imm!!.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun hideKeyboardFromView(context: Context, view: View) {
        imm = context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm?.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun showKeyboard(activity: Activity) {
        imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        var view = activity.currentFocus
        if (view == null) {
            view = View(activity)
        }
        imm!!.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
    }

    fun getImageUri(inContext: Context, inImage: Bitmap): Uri {
        bos = ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bos);
        val path = MediaStore.Images.Media.insertImage(
            inContext.getContentResolver(),
            inImage,
            "Title",
            null
        );
        return Uri.parse(path);
    }

    fun getBitmap(path: String): Bitmap {
        val rotation = getImageOrientation(path)
        val matrix = Matrix()
        matrix.postRotate(rotation.toFloat())

        val options = BitmapFactory.Options()
        options.inSampleSize = 1
        val sourceBitmap = BitmapFactory.decodeFile(path, options)

        return Bitmap.createBitmap(
            sourceBitmap, 0, 0, sourceBitmap.width, sourceBitmap.height, matrix,
            true
        )
    }

    fun getBitmapFromURL(url: String): Bitmap? {
        return try {
            BitmapFactory.decodeStream(URL(url).openConnection().getInputStream());
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }
    }

    fun getImageOrientation(imagePath: String): Int {
        var rotate = 0
        try {
            val imageFile = File(imagePath)
            val exif = ExifInterface(imageFile.absolutePath)
            val orientation = exif.getAttributeInt(
                ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_NORMAL
            )

            when (orientation) {
                ExifInterface.ORIENTATION_ROTATE_270 -> rotate = 270
                ExifInterface.ORIENTATION_ROTATE_180 -> rotate = 180
                ExifInterface.ORIENTATION_ROTATE_90 -> rotate = 90
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return rotate
    }

    /**
     * Return DOB Format 13-Jan-2018
     */
    private var month_st: String = "null"

    fun getDOBFormat(date: String, month: Int, year: String): String {
        when (month) {
            1 -> month_st = "Jan"
            2 -> month_st = "Feb"
            3 -> month_st = "Mar"
            4 -> month_st = "Apr"
            5 -> month_st = "May"
            6 -> month_st = "Jun"
            7 -> month_st = "Jul"
            8 -> month_st = "Aug"
            9 -> month_st = "Sep"
            10 -> month_st = "Oct"
            11 -> month_st = "Nov"
            12 -> month_st = "Dec"
        }
        return "$date $month_st $year"
    }

    fun getAPIDOBFormat(inputFormat: String): String {

        val mMonth = when (inputFormat.split(" ")[1]) {
            "Jan" -> "01"
            "Feb" -> "02"
            "Mar" -> "03"
            "Apr" -> "04"
            "May" -> "05"
            "Jun" -> "06"
            "Jul" -> "07"
            "Aug" -> "08"
            "Sep" -> "09"
            "Oct" -> "10"
            "Nov" -> "11"
            "Dec" -> "12"
            else -> ""
        }
        return inputFormat.split(" ")[2] + "/" + mMonth + "/" + inputFormat.split(" ")[0]
    }

    fun getEditProfileDOBFormat(dob: String): String {
        val mMonth = when (dob.split("/")[1]) {
            "01" -> "Jan"
            "02" -> "Feb"
            "03" -> "Mar"
            "04" -> "Apr"
            "05" -> "May"
            "06" -> "Jun"
            "07" -> "Jul"
            "08" -> "Aug"
            "09" -> "Sep"
            "10" -> "Oct"
            "11" -> "Nov"
            "12" -> "Dec"
            else -> ""
        }
        return dob.split("/")[2]+" "+mMonth+" "+dob.split("/")[0]
    }

    fun getMonthAsIntDualDigit(monthString: String): String {
        return when(monthString) {
            "January"->"01"
            "February"->"02"
            "March"->"03"
            "April"->"04"
            "May"->"05"
            "June"->"06"
            "July"->"07"
            "August"->"08"
            "September"->"09"
            "October"->"10"
            "November"->"11"
            "December"->"12"
            else -> "null"
        }
    }

    fun logoutWork() {
        setString(Constants.KEY_USERMODEL,"null")
    }

    fun getScreenHeight(mActivity: Activity): Int {
        val displayMetrics = DisplayMetrics()
        mActivity.windowManager.defaultDisplay.getMetrics(displayMetrics)
        return displayMetrics.heightPixels

    }

    fun getScreenWidth(mActivity: Activity): Int {
        val displayMetrics = DisplayMetrics()
        mActivity.windowManager.defaultDisplay.getMetrics(displayMetrics)
        return displayMetrics.widthPixels
    }

    fun getShortTime(timeMillis: Long): String? {
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = timeMillis
        @SuppressLint("SimpleDateFormat") val formatter =
            SimpleDateFormat("hh:mm a")
        return formatter.format(calendar.time)
    }

    @SuppressLint("SimpleDateFormat")
    fun getYear(timeMillis: Long): String? {
        val cal: Calendar = Calendar.getInstance()
        val tz: TimeZone = cal.timeZone
        val sdf = SimpleDateFormat("YYYY")
        sdf.timeZone = tz
        val timestamp: Long = timeMillis
        val localTime: String =
            sdf.format(Date(timestamp * 1000)) // I assume your timestamp is in seconds and you're converting to milliseconds?
        return localTime
    }

    fun getWeekdayName(dateMillis: Long): String? {
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = dateMillis
        return android.text.format.DateFormat.format("EEEE", calendar.time) as String
    }

    fun disableSSLCertificateChecking() {
        val trustAllCerts =
            arrayOf<TrustManager>(object : X509TrustManager {
                override fun getAcceptedIssuers(): Array<X509Certificate>? {
                    return null
                }

                @SuppressLint("TrustAllX509TrustManager")
                override fun checkClientTrusted(
                    arg0: Array<X509Certificate>,
                    arg1: String
                ) {
                    // Not implemented
                }

                @SuppressLint("TrustAllX509TrustManager")
                override fun checkServerTrusted(
                    arg0: Array<X509Certificate>,
                    arg1: String
                ) {
                    // Not implemented
                }
            })
        try {
            val sc = SSLContext.getInstance("TLS")
            sc.init(null, trustAllCerts, SecureRandom())
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.socketFactory)
        } catch (e: KeyManagementException) {
            e.printStackTrace()
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        }
    }

    @SuppressLint("StaticFieldLeak")
    private var picasso: Picasso? = null

    fun loadImageInto(
        context: Context?,
        url: String?,
        width: Int,
        height: Int,
        target: Target?
    ) {
        picasso=Picasso.Builder(context!!).build()
        picasso!!.load(url)
            .resize(width, height)
            .transform(RoundRectTransform())
            .into(target!!)
    }

    fun makeStreamURL(
        context: Context?,
        streamID: String
    ): String? {
        return ModuleApiInterface.BASE_URL.toString() + "live/" + UserModel.getUserModel().userInfo?.username + "/" + UserModel.getUserModel().userInfo?.password + "/" + streamID + ".m3u8"
    }

    var TYPE_NOT_CONNECTED = 0
    fun getConnectivityStatus(context: Context): Int {
        val cm = context
            .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        if (null != activeNetwork) {
            val TYPE_WIFI = 1
            if (activeNetwork.type == ConnectivityManager.TYPE_WIFI) return TYPE_WIFI
            val TYPE_ETHERNET = 2
            if (activeNetwork.type == ConnectivityManager.TYPE_ETHERNET) return TYPE_ETHERNET
        }
        return TYPE_NOT_CONNECTED
    }


    /**
     * UI Helper Ends
     */

    companion object {
        var mInstance: Utils? = null

        fun getUtils(): Utils {
            if (mInstance != null) {
                return mInstance as Utils
            } else {
                mInstance = Utils()
                return mInstance!!
            }
        }
    }
}