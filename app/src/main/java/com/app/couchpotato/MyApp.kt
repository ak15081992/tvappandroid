package com.app.couchpotato

import android.app.Activity
import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.graphics.Typeface
import com.app.couchpotato.dagger.component.AppComponents
import com.app.couchpotato.dagger.component.DaggerAppComponents
import com.app.couchpotato.dagger.module.ModuleContext
import com.app.couchpotato.helperutils.Constants
import com.rohitss.uceh.UCEHandler

class MyApp : Application() {

    /**
     * AppComponent of Dagger
     */
    lateinit var mAppComponent: AppComponents

    companion object {
        lateinit var mMyApp: MyApp
        lateinit var mSharedPref: SharedPreferences
        lateinit var fontMontserratRegular: Typeface
        lateinit var fontMontserratMedium: Typeface
        lateinit var fontMontserratSemiBold: Typeface
        lateinit var fontMontserratBold: Typeface

        fun get(activity: Activity): MyApp {
            return activity.application as MyApp
        }
    }

    override fun onCreate() {
        super.onCreate()
        mMyApp = this
       // UCEHandler.Builder(applicationContext).build()

        mSharedPref = getSharedPreferences(Constants.COUCH_POTATO_PREF.toString(), Context.MODE_PRIVATE)

        //mAppComponent Init
        mAppComponent = DaggerAppComponents.builder()
            .moduleContext(ModuleContext(this))
            .build()

        fontMontserratRegular=Typeface.createFromAsset(
            applicationContext.assets,
            "fonts/Montserrat_Regular.ttf"
        )
        fontMontserratMedium=Typeface.createFromAsset(
            applicationContext.assets,
            "fonts/Montserrat_Medium.ttf"
        )
        fontMontserratSemiBold=Typeface.createFromAsset(
            applicationContext.assets,
            "fonts/Montserrat_SemiBold.ttf"
        )
        fontMontserratBold=Typeface.createFromAsset(
            applicationContext.assets,
            "fonts/Montserrat_Bold.ttf"
        )
    }

    /**
     * Returns @mAppComponent
     */
    fun getAppComponents(): AppComponents {
        return mAppComponent
    }
}