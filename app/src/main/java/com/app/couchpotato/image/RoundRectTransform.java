package com.app.couchpotato.image;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;

import com.squareup.picasso.Transformation;

public class RoundRectTransform implements Transformation {
    public final static int BORDER_RADIUS = 5;

    @Override
    public Bitmap transform(Bitmap source) {
        Bitmap bitmap = Bitmap.createBitmap(source.getWidth(), source.getHeight(), source.getConfig());

        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint();
        BitmapShader shader = new BitmapShader(source, BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
        paint.setShader(shader);
        paint.setAntiAlias(true);

        canvas.drawRoundRect(0, 0, source.getWidth(), source.getHeight(), BORDER_RADIUS, BORDER_RADIUS, paint);

        source.recycle();
        source = null;
        return bitmap;
    }

    @Override
    public String key() {
        return "square";
    }

}