package com.app.couchpotato.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.app.couchpotato.R;

import java.util.ArrayList;

public class MainMenuAdapter extends BaseAdapter {

    private ArrayList<String> reportList;
    private Context context;

    public MainMenuAdapter(Context context, ArrayList<String> reportList) {
        this.context = context;
        this.reportList = reportList;
    }

    @SuppressLint({"ViewHolder", "InflateParams"})
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        convertView = inflater.inflate(R.layout.item_menu, null);

        TextView tvReport = convertView.findViewById(R.id.ID_TXTVIEW_CHANNEL);
        tvReport.setText(reportList.get(position));

        return convertView;
    }
    @Override
    public int getCount() {
        return reportList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
}
