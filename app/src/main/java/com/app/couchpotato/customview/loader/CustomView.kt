package com.app.couchpotato.customview.loader

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.util.AttributeSet
import android.widget.RelativeLayout

open class CustomView(context: Context, attrs: AttributeSet) : RelativeLayout(context, attrs) {

    private val disabledBackgroundColor = Color.parseColor("#00FFFFFF")
    internal var beforeBackground: Int = 0

    private var animation = false

    override fun setEnabled(enabled: Boolean) {
        super.setEnabled(enabled)
        if (enabled)
            setBackgroundColor(beforeBackground)
        else
            setBackgroundColor(disabledBackgroundColor)
        invalidate()
    }

    override fun onAnimationStart() {
        super.onAnimationStart()
        animation = true
    }

    override fun onAnimationEnd() {
        super.onAnimationEnd()
        animation = false
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        if (animation)
            invalidate()
    }
}
