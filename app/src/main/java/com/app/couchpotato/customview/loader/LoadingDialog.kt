package com.app.couchpotato.customview.loader

import android.app.Dialog
import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Handler
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager
import com.app.couchpotato.R
import kotlinx.android.synthetic.main.dialog_loader.*

import java.lang.Exception

class LoadingDialog {

    private var mProgress: Dialog? = null

    lateinit var wmlp: WindowManager.LayoutParams

    fun showLoader(con: Context) {
        mProgress = Dialog(con)
        mProgress?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        mProgress?.window!!.setBackgroundDrawable(ColorDrawable(android.graphics.Color.TRANSPARENT))
        wmlp = mProgress!!.window!!.attributes

        wmlp.gravity = Gravity.CENTER
        mProgress?.window!!.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        mProgress?.setCancelable(false)
        mProgress?.setContentView(R.layout.dialog_loader)

        mProgress?.txt_processing?.setPadding(30, 0, 0, 0)
        mProgress?.txt_processing?.visibility = View.GONE
        Handler().postDelayed({
            mProgress?.txt_processing?.visibility = View.VISIBLE
            }, 2800)

        mProgress?.show()
    }

    fun setMessagePadding(left:Int, top: Int, rights: Int, bottom: Int ) {
        mProgress?.txt_processing?.setPadding(left,top,rights,bottom)
    }

    fun setMessage(msg: String) {
        mProgress?.txt_processing?.text=msg
    }

    fun setBackgroundDrawable(drawable: Drawable) {
        mProgress?.ll_main_container?.background=drawable
    }

    fun setBackgroundColor(color: Int) {
        mProgress?.ll_main_container?.setBackgroundColor(color)
    }

    fun setCancelable(status: Boolean) {
        mProgress?.setCancelable(status)
    }

    fun dismissLoader() {
        if (mProgress != null) {
            try {
                mProgress?.dismiss()
            }catch (e : Exception ){}
            mProgress = null
        }
    }

    companion object {

        var mInstance: LoadingDialog? = null

        fun getLoader(): LoadingDialog {
            return if (mInstance != null) {
                mInstance as LoadingDialog
            } else {
                mInstance = LoadingDialog()
                mInstance!!
            }
        }
    }
}
