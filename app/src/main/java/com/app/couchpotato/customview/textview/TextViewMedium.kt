package com.app.couchpotato.customview.textview

import android.content.Context
import android.util.AttributeSet
import android.widget.TextView
import com.app.couchpotato.MyApp

class TextViewMedium : TextView {

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) :
            super(context, attrs, defStyleAttr) {
        init()
    }

    private fun init() {
        if (!isInEditMode) {
            typeface = MyApp.fontMontserratMedium
        }
    }
}