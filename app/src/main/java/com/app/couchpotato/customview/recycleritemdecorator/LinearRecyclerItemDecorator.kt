package com.app.couchpotato.customview.recycleritemdecorator

import android.graphics.Rect
import android.util.Log
import androidx.recyclerview.widget.RecyclerView

class LinearRecyclerItemDecorator(
    val _top: Int,
    val _bottom: Int,
    val _start: Int,
    val _end: Int,
    val _extra: Int=0,
    val totalSize: Int,
    val type: Int
) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, itemPosition: Int, parent: RecyclerView) {
        /**
         * Type 0 for Vertical Layout
         * Type 1 fro Horizontal Layout
         */
        with(outRect) {
            if (itemPosition==0) {
                if (type==1) {
                    left=_extra
                    top=_top
                    bottom=_bottom
                    right=_end
                    return
                }
            }
            if (itemPosition==totalSize-1) {
                if (type==1) {
                    top=_top
                    bottom=_bottom
                    left=_start
                    right = _extra
                } else {
                    top=_top
                    bottom=_extra
                    left=_start
                    right=_end
                }
                return
            }
            top=_top
            bottom=_bottom
            left=_start
            right=_end
        }

    }
}

