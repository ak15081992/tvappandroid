package com.app.couchpotato.customview

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Handler
import android.util.AttributeSet
import android.view.*
import android.view.GestureDetector.SimpleOnGestureListener
import android.view.View.OnKeyListener
import android.widget.Scroller
import com.app.couchpotato.R
import com.app.couchpotato.commonmodel.*
import com.app.couchpotato.helperutils.Constants
import com.app.couchpotato.helperutils.Utils
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import java.util.*

/**
 * Classic EPG, electronic program guide, that scrolls both horizontal, vertical and diagonal.
 * It utilize onDraw() to draw the graphic on screen. So there are some private helper methods calculating positions etc.
 * Listed on Y-axis are channels and X-axis are programs/events. Data is added to EPG by using setEPGData()
 * and pass in an EPGData implementation. A click listener can be added using setEPGClickListener().
 * Created by Kristoffer, http://kmdev.se
 */
class EPGView @JvmOverloads constructor(
    context: Context?,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ViewGroup(context, attrs, defStyleAttr) {
    private val mClipRect: Rect
    private val mDrawingRect: Rect
    private val mMeasuringRect: Rect
    private val mPaint: Paint
    private val mScroller: Scroller
    private val mGestureDetector: GestureDetector
    private val mChannelLayoutMargin: Int
    private val mChannelLayoutPadding: Int
    private val mChannelNumberWidth: Int
    private val mChannelLayoutHeight: Int
    private val mChannelLayoutWidth: Int
    private val mChannelLayoutBackground: Int
    private val mEventLayoutBackground: Int
    private val mEventLayoutBackgroundCurrent: Int
    private val mEventLayoutTextColor: Int
    private val mEventLayoutTextSize: Int
    private val mTimeBarLineWidth: Int
    private val mTimeBarLineColor: Int
    private val mTimeBarHeight: Int
    private val mTimeBarTextSize: Int
    private val mResetButtonSize: Int
    private val mResetButtonMargin: Int
    private var mClickListener: EPGClickListener? = null
    private var mMaxHorizontalScroll = 0
    private var mMaxVerticalScroll = 0
    private var mMillisPerPixel: Long = 0
    private var mTimeOffset: Long = 0
    private var mTimeLowerBoundary: Long = 0
    private var mTimeUpperBoundary: Long = 0
    var selectedChannelIndex: Int
        private set
    var selectedTopicIndex: Int
        private set
    private var m_bIsScrollLocked: Boolean
    private var m_bIsEnlarge: Boolean
    private val timeLineHandler = Handler()
    private val runnableTimeLineUpdate: Runnable = object : Runnable {
        override fun run() {
            redraw()
            timeLineHandler.postDelayed(this, 5000)
        }
    }
    private var epgData: ArrayChannelItem? = null
    override fun onDraw(canvas: Canvas) {
        if (epgData != null && epgData!!.size > 0) {
            mTimeLowerBoundary = getTimeFrom(scrollX)
            mTimeUpperBoundary = getTimeFrom(scrollX + width)
            val drawingRect = mDrawingRect
            drawingRect.left = scrollX
            drawingRect.top = scrollY
            drawingRect.right = drawingRect.left + width
            drawingRect.bottom = drawingRect.top + height
            drawChannelListItems(canvas, drawingRect)
            drawEvents(canvas, drawingRect)
            drawTimebar(canvas, drawingRect)
            drawTimeLine(canvas, drawingRect)

            // If scroller is scrolling/animating do scroll. This applies when doing a fling.
            if (mScroller.computeScrollOffset()) {
                scrollTo(mScroller.currX, mScroller.currY)
            }
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        recalculateAndRedraw(false)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean {
        return mGestureDetector.onTouchEvent(event)
    }

    override fun onLayout(
        changed: Boolean,
        l: Int,
        t: Int,
        r: Int,
        b: Int
    ) {
    }

    fun processKeyEvent(keyCode: Int, event: KeyEvent): Boolean {
        var dateSelectedTopicStart: Date? = null
        if ((keyCode == KeyEvent.KEYCODE_DPAD_DOWN || keyCode == KeyEvent.KEYCODE_MEDIA_NEXT) && event.action == KeyEvent.ACTION_DOWN) {
            if (epgData!![selectedChannelIndex]!!.m_arrItemTopic[selectedTopicIndex]!!.m_dateTopicStart != null) dateSelectedTopicStart =
                Date(epgData!![selectedChannelIndex]!!.m_arrItemTopic[selectedTopicIndex]!!.m_dateTopicStart.time)
            epgData!![selectedChannelIndex]!!.m_arrItemTopic[selectedTopicIndex]!!.m_bIsSelected =
                false
            selectedChannelIndex++
            if (selectedChannelIndex > epgData!!.size - 1) {
                selectedChannelIndex = 0
            }
            if (dateSelectedTopicStart == null || getXFrom(dateSelectedTopicStart.time) < scrollX + mChannelLayoutWidth) {
                if (dateSelectedTopicStart == null) {
                    dateSelectedTopicStart = Date()
                }
                dateSelectedTopicStart.time = getTimeFrom(scrollX + mChannelLayoutWidth)
            }
            m_bIsScrollLocked = true
            selectTopicByTime(dateSelectedTopicStart)
            m_bIsScrollLocked = false
            return true
        } else if ((keyCode == KeyEvent.KEYCODE_DPAD_UP || keyCode == KeyEvent.KEYCODE_MEDIA_PREVIOUS) && event.action == KeyEvent.ACTION_DOWN) {
            if (epgData!![selectedChannelIndex]!!.m_arrItemTopic[selectedTopicIndex]!!.m_dateTopicStart != null) dateSelectedTopicStart =
                Date(epgData!![selectedChannelIndex]!!.m_arrItemTopic[selectedTopicIndex]!!.m_dateTopicStart.time)
            epgData!![selectedChannelIndex]!!.m_arrItemTopic[selectedTopicIndex]!!.m_bIsSelected =
                false
            selectedChannelIndex--
            if (selectedChannelIndex < 0) {
                selectedChannelIndex = epgData!!.size - 1
            }
            if (dateSelectedTopicStart == null || getXFrom(dateSelectedTopicStart.time) < scrollX + mChannelLayoutWidth) {
                if (dateSelectedTopicStart == null) {
                    dateSelectedTopicStart = Date()
                }
                dateSelectedTopicStart.time = getTimeFrom(scrollX + mChannelLayoutWidth)
            }
            m_bIsScrollLocked = true
            selectTopicByTime(dateSelectedTopicStart)
            m_bIsScrollLocked = false
            return true
        } else if (keyCode == KeyEvent.KEYCODE_DPAD_LEFT && event.action == KeyEvent.ACTION_DOWN) {
            selectedTopicIndex--
            if (selectedTopicIndex > -1 && epgData!![selectedChannelIndex]!!.m_arrItemTopic.size > 1) {
                epgData!![selectedChannelIndex]!!.m_arrItemTopic[selectedTopicIndex + 1]!!.m_bIsSelected =
                    false
                if (epgData!![selectedChannelIndex]!!.m_arrItemTopic[selectedTopicIndex]!!.m_dateTopicStart != null) dateSelectedTopicStart =
                    Date(
                        epgData!![selectedChannelIndex]!!.m_arrItemTopic[selectedTopicIndex]!!.m_dateTopicStart.time
                    )
                selectTopicByTime(dateSelectedTopicStart)
            } else selectedTopicIndex = 0
            return true
        } else if (keyCode == KeyEvent.KEYCODE_DPAD_RIGHT && event.action == KeyEvent.ACTION_DOWN) {
            selectedTopicIndex++
            if (selectedTopicIndex < epgData!![selectedChannelIndex]!!.m_arrItemTopic.size && epgData!![selectedChannelIndex]!!.m_arrItemTopic.size > 1
            ) {
                epgData!![selectedChannelIndex]!!.m_arrItemTopic[selectedTopicIndex - 1]!!.m_bIsSelected =
                    false
                if (epgData!![selectedChannelIndex]!!.m_arrItemTopic[selectedTopicIndex]!!.m_dateTopicStart != null) dateSelectedTopicStart =
                    Date(
                        epgData!![selectedChannelIndex]!!.m_arrItemTopic[selectedTopicIndex]!!.m_dateTopicStart.time
                    )
                selectTopicByTime(dateSelectedTopicStart)
            } else selectedTopicIndex--
            return true
        }
        return false
    }

    fun setKeyListener() {
        val m_keyListener =
            OnKeyListener { v: View?, keyCode: Int, event: KeyEvent ->
                processKeyEvent(
                    keyCode,
                    event
                )
            }

//        setOnKeyListener(m_keyListener);
    }

    private fun getNearestTopicItem(selectedTopicStart: Date?): Int {
        if (epgData!![selectedChannelIndex]!!.m_arrItemTopic.size <= 0) return -1
        var i: Int
        var iIndex = 0
        var lOffset: Long
        var lOffsetNew: Long
        var itemTopic: ItemTopic? = epgData!![selectedChannelIndex]!!.m_arrItemTopic[0]
        if (itemTopic?.m_dateTopicStart == null && itemTopic?.m_dateTopicEnd == null) return 0
        lOffset = selectedTopicStart?.time ?: Calendar.getInstance().time
            .getTime()
        lOffset -= itemTopic.m_dateTopicStart.getTime()

        // When First Item is at the Right of Offset
        if (lOffset < 0) return iIndex
        i = 1
        while (i < epgData!![selectedChannelIndex]!!.m_arrItemTopic.size) {
            itemTopic = epgData!![selectedChannelIndex]!!.m_arrItemTopic[i]
            lOffsetNew = selectedTopicStart?.time ?: Calendar.getInstance().time
                .getTime()
            lOffsetNew -= itemTopic?.m_dateTopicStart?.getTime()!!

            // When Two Items are at the Left of Offset
            if (lOffset > 0 && lOffsetNew > 0) {
                iIndex = i
                lOffset = lOffsetNew
                i++
                continue
            }

            // When First Item is at the Left of Offset
            // When Second Item is at the Right of Offset
            if (lOffsetNew <= 0) {
                lOffsetNew *= -1
                iIndex = if (lOffsetNew == 0L) i else i - 1
                break
            }
            i++
        }
        return iIndex
    }

    private fun selectTopicByTime(selectedTopicStart: Date?) {
        val channelItem: ChannelItem = epgData!![selectedChannelIndex]!!
        selectedTopicIndex = getNearestTopicItem(selectedTopicStart)
        channelItem.m_arrItemTopic.get(selectedTopicIndex)!!.m_bIsSelected = true
        val orgx = scrollX
        val orgy = scrollY
        val left =
            if (channelItem.m_arrItemTopic.get(selectedTopicIndex)!!.m_dateTopicStart == null) orgx else getXFrom(
                channelItem.m_arrItemTopic.get(selectedTopicIndex)!!.m_dateTopicStart.getTime()
            ) - mChannelLayoutWidth - mChannelLayoutMargin
        val right =
            if (channelItem.m_arrItemTopic.get(selectedTopicIndex)!!.m_dateTopicEnd == null) orgx + width else getXFrom(
                channelItem.m_arrItemTopic.get(selectedTopicIndex)!!.m_dateTopicEnd.getTime()
            ) - mChannelLayoutWidth - mChannelLayoutMargin
        val top = getTopFrom(selectedChannelIndex) - mTimeBarHeight - mChannelLayoutMargin
        var bottom = top + mChannelLayoutHeight + mChannelLayoutMargin
        if (m_bIsEnlarge) bottom += 2 * (mChannelLayoutHeight + mChannelLayoutMargin)
        val dx = left - orgx
        val dy = top - orgy
        val dxright = right - orgx - (width - mChannelLayoutWidth - mChannelLayoutMargin)
        val dybottom = bottom - orgy - (height - mTimeBarHeight - mChannelLayoutMargin)
        var offsetx = 0
        var offsety = 0
        if (dx < 0) offsetx = dx else {
            if (dx > width - mChannelLayoutWidth - mChannelLayoutMargin) offsetx =
                dx else if (dxright > 0) offsetx = Math.min(dx, dxright)
        }
        if (dy < 0) offsety = dy else {
            if (dy > height - mTimeBarHeight - mChannelLayoutMargin) {
                offsety = Math.min(dy, mMaxVerticalScroll)
            } else if (dybottom > 0) offsety = Math.min(dy, dybottom)
        }
        if (m_bIsScrollLocked && (channelItem.m_arrItemTopic.get(selectedTopicIndex)!!.m_dateTopicStart == null || channelItem.m_arrItemTopic.get(
                selectedTopicIndex
            )!!.m_dateTopicEnd == null ||
                    isEventVisible(
                        channelItem.m_arrItemTopic.get(selectedTopicIndex)!!.m_dateTopicStart.getTime(),
                        channelItem.m_arrItemTopic.get(selectedTopicIndex)!!.m_dateTopicEnd.getTime()
                    ))
        ) {
            offsetx = 0
        }
        scrollBy(offsetx, offsety)
        redraw()
    }

    fun setCurrentChannel(selectedChannel: Int) {
        var channelItem: ChannelItem? = epgData!![selectedChannelIndex]
        channelItem!!.m_arrItemTopic.get(selectedTopicIndex)!!.m_bIsSelected = false
        selectedChannelIndex = selectedChannel
        channelItem = epgData!![selectedChannelIndex]
        selectedTopicIndex = channelItem!!.currentTopicIndex
        selectTopicByTime(channelItem.m_arrItemTopic.get(selectedTopicIndex)!!.m_dateTopicStart)
    }

    private fun drawTimebar(
        canvas: Canvas,
        drawingRect: Rect
    ) {
        drawingRect.left = scrollX + mChannelLayoutWidth
        drawingRect.top = scrollY
        drawingRect.right = drawingRect.left + width
        drawingRect.bottom = drawingRect.top + mTimeBarHeight
        mClipRect.left = scrollX + mChannelLayoutWidth
        mClipRect.top = scrollY
        mClipRect.right = scrollX + width
        mClipRect.bottom = mClipRect.top + mTimeBarHeight
        canvas.save()
        canvas.clipRect(mClipRect)

        // Background
        mPaint.color = mChannelLayoutBackground
        canvas.drawRect(drawingRect, mPaint)

        // Time stamps
        mPaint.color = mEventLayoutTextColor
        mPaint.textSize = mTimeBarTextSize.toFloat()
        for (i in 0 until HOURS_IN_VIEWPORT_MILLIS / TIME_LABEL_SPACING_MILLIS) {
            // Get time and round to nearest half hour
            val time = TIME_LABEL_SPACING_MILLIS *
                    ((mTimeLowerBoundary + TIME_LABEL_SPACING_MILLIS * i +
                            TIME_LABEL_SPACING_MILLIS / 2) / TIME_LABEL_SPACING_MILLIS)
            canvas.drawText(
                Utils.getUtils().getShortTime(time).toString(),
                getXFrom(time).toFloat(),
                drawingRect.top + ((drawingRect.bottom - drawingRect.top) / 2 + mTimeBarTextSize / 2).toFloat(),
                mPaint
            )
        }
        canvas.restore()
        drawTimebarDayIndicator(canvas, drawingRect)
    }

    private fun drawTimebarDayIndicator(
        canvas: Canvas,
        drawingRect: Rect
    ) {
        drawingRect.left = scrollX
        drawingRect.top = scrollY
        drawingRect.right = drawingRect.left + mChannelLayoutWidth
        drawingRect.bottom = drawingRect.top + mTimeBarHeight

        // Background
        mPaint.color = mChannelLayoutBackground
        canvas.drawRect(drawingRect, mPaint)

        // Text
        mPaint.color = mEventLayoutTextColor
        mPaint.textSize = mTimeBarTextSize.toFloat()
        mPaint.textAlign = Paint.Align.CENTER
        canvas.drawText(
            Utils.getUtils().getWeekdayName(mTimeLowerBoundary).toString(),
            drawingRect.left + ((drawingRect.right - drawingRect.left) / 2).toFloat(),
            drawingRect.top + ((drawingRect.bottom - drawingRect.top) / 2 + mTimeBarTextSize / 2).toFloat(),
            mPaint
        )
        mPaint.textAlign = Paint.Align.LEFT
    }

    private fun drawTimeLine(
        canvas: Canvas,
        drawingRect: Rect
    ) {
        val now = System.currentTimeMillis()
        if (shouldDrawTimeLine(now)) {
            drawingRect.left = getXFrom(now) - mTimeBarLineWidth
            drawingRect.top = scrollY
            drawingRect.right = drawingRect.left + mTimeBarLineWidth
            drawingRect.bottom = drawingRect.top + height
            mPaint.color = mTimeBarLineColor
            //            mPaint.setColor(Color.TRANSPARENT);
            val bitmap =
                (resources.getDrawable(R.drawable.timelinebar) as BitmapDrawable).bitmap
            canvas.drawBitmap(
                bitmap,
                Rect(0, 0, bitmap.width, bitmap.height),
                drawingRect,
                mPaint
            )
            //            canvas.drawRect(drawingRect, mPaint);
        }
    }

    private fun drawEvents(
        canvas: Canvas,
        drawingRect: Rect
    ) {
        val firstPos = firstVisibleChannelPosition
        val lastPos = lastVisibleChannelPosition
        for (pos in firstPos..lastPos) {

            // Set clip rectangle
            mClipRect.left = scrollX + mChannelLayoutWidth + mChannelLayoutMargin
            mClipRect.top = getTopFrom(pos)
            mClipRect.right = scrollX + width
            mClipRect.bottom = mClipRect.top + mChannelLayoutHeight
            if (m_bIsEnlarge) {
                if (pos == selectedChannelIndex) {
                    mClipRect.bottom += 2 * (mChannelLayoutHeight + mChannelLayoutMargin)
                } else if (pos > selectedChannelIndex) {
                    mClipRect.top += 2 * (mChannelLayoutHeight + mChannelLayoutMargin)
                    mClipRect.bottom += 2 * (mChannelLayoutHeight + mChannelLayoutMargin)
                }
            }
            canvas.save()
            canvas.clipRect(mClipRect)

            // Draw each event
            var foundFirst = false
            val epgEvents: ChannelItem? = epgData!![pos]
            for (event in epgEvents!!.m_arrItemTopic) {
                if (event!!.m_dateTopicStart == null || event.m_dateTopicEnd == null || isEventVisible(
                        event.m_dateTopicStart.getTime(),
                        event.m_dateTopicEnd.getTime()
                    )
                ) {
                    drawEvent(canvas, pos, event, drawingRect)
                    foundFirst = true
                } else if (foundFirst) {
                    break
                }
            }
            canvas.restore()
        }
    }

    private fun drawEvent(
        canvas: Canvas,
        channelPosition: Int,
        event: ItemTopic,
        drawingRect: Rect
    ) {
        if (event.m_dateTopicStart != null && event.m_dateTopicEnd != null) setEventDrawingRectangle(
            channelPosition,
            event.m_dateTopicStart.getTime(),
            event.m_dateTopicEnd.getTime(),
            drawingRect
        ) else {
            drawingRect.left = scrollX + mChannelLayoutWidth + mChannelLayoutMargin
            drawingRect.top = getTopFrom(channelPosition)
            if (m_bIsEnlarge && channelPosition > selectedChannelIndex) {
                drawingRect.top += 2 * (mChannelLayoutHeight + mChannelLayoutMargin)
            }
            drawingRect.right = scrollX + width
            drawingRect.bottom = drawingRect.top + mChannelLayoutHeight
        }

        // Background
        var isReminded = false
        for (itemTopic in CommonLists.REMINDER_TOPIC_ARRAY) {
            if (itemTopic.IsSameWith(event)) {
                isReminded = true
                break
            }
        }
        mPaint.color =
            if (event.m_bIsSelected) mEventLayoutBackgroundCurrent else if (isReminded) mTimeBarLineColor else mEventLayoutBackground
        canvas.drawRect(drawingRect, mPaint)

        // Add left and right inner padding
        drawingRect.left += mChannelLayoutPadding
        drawingRect.right -= mChannelLayoutPadding

        // Text
        mPaint.color = mEventLayoutTextColor
        if (event.m_bIsSelected) mPaint.textSize =
            resources.getDimension(R.dimen._14sdp).toFloat() else mPaint.textSize =
            mEventLayoutTextSize.toFloat()

        // Move drawing.top so text will be centered (text is drawn bottom>up)
        mPaint.getTextBounds(event.m_sTitle, 0, event.m_sTitle.length, mMeasuringRect)
        drawingRect.top += (drawingRect.bottom - drawingRect.top) / 2 + mMeasuringRect.height() / 2
        val currentXPos =
            scrollX + mChannelLayoutWidth + mChannelLayoutMargin + mChannelLayoutPadding
        drawingRect.left = Math.max(drawingRect.left, currentXPos)
        var title: String = event.m_sTitle
        title = title.substring(
            0,
            mPaint.breakText(title, true, drawingRect.right - drawingRect.left.toFloat(), null)
        )
        canvas.drawText(title, drawingRect.left.toFloat(), drawingRect.top.toFloat(), mPaint)

        //draw Details
        if (channelPosition == selectedChannelIndex && event.m_bIsSelected) {
            drawEventDetails(canvas, channelPosition, event, drawingRect)
        }
    }

    private fun drawEventDetails(
        canvas: Canvas,
        channelPosition: Int,
        event: ItemTopic,
        drawingRect: Rect
    ) {
        drawingRect.left = scrollX + mChannelLayoutWidth + mChannelLayoutMargin
        drawingRect.top = getTopFrom(channelPosition) + mChannelLayoutHeight + mChannelLayoutMargin
        drawingRect.right = scrollX + width
        drawingRect.bottom = drawingRect.top + 2 * mChannelLayoutHeight + mChannelLayoutMargin
        val drawingLimit = drawingRect.bottom
        var title: String = event.m_sTitle
        //Draw Background
        var isReminded = false
        for (itemTopic in CommonLists.REMINDER_TOPIC_ARRAY) {
            if (itemTopic.IsSameWith(event)) {
                isReminded = true
                break
            }
        }
        mPaint.color =
            if (event.m_bIsSelected) mEventLayoutBackgroundCurrent else if (isReminded) mTimeBarLineColor else mEventLayoutBackground
        mPaint.textSize = mEventLayoutTextSize.toFloat()
        canvas.drawRect(drawingRect, mPaint)
        //Draw Topic Name
        drawingRect.top += mChannelLayoutMargin
        drawingRect.left += mChannelLayoutPadding
        drawingRect.bottom = drawingRect.top + mChannelLayoutHeight / 2
        drawingRect.top += (drawingRect.bottom - drawingRect.top) / 2
        title = title.substring(
            0,
            mPaint.breakText(title, true, drawingRect.right - drawingRect.left.toFloat(), null)
        )
        mPaint.color = mEventLayoutTextColor
        canvas.drawText(
            title,
            drawingRect.left.toFloat(),
            drawingRect.top + mMeasuringRect.height() / 2.toFloat(),
            mPaint
        )

        //Draw Time
        drawingRect.top = drawingRect.bottom
        drawingRect.bottom = drawingRect.top + mChannelLayoutHeight / 4
        title = if (event.m_dateTopicStart == null || event.m_dateTopicEnd == null) "" else {
            Utils.getUtils().getShortTime(event.m_dateTopicStart.getTime())
                .toString() + " - " + Utils.getUtils().getShortTime(event.m_dateTopicEnd.getTime())
        }
        mPaint.color = mEventLayoutTextColor
        mPaint.textSize = resources.getDimension(R.dimen._7sdp).toFloat()
        mPaint.getTextBounds(title, 0, title.length, mMeasuringRect)
        drawingRect.top += (drawingRect.bottom - drawingRect.top) / 2
        title = title.substring(
            0,
            mPaint.breakText(title, true, drawingRect.right - drawingRect.left.toFloat(), null)
        )
        canvas.drawText(
            title,
            drawingRect.right - mMeasuringRect.width() - mChannelLayoutPadding.toFloat(),
            drawingRect.top + mMeasuringRect.height() / 2.toFloat(),
            mPaint
        )

        //Draw Description
        title = event.m_sDescription
        mPaint.textSize = resources.getDimension(R.dimen._8sdp)
        do {
            drawingRect.top = drawingRect.bottom + mChannelLayoutMargin
            drawingRect.bottom = drawingRect.top + mChannelLayoutHeight / 4
            mPaint.getTextBounds(title, 0, title.length, mMeasuringRect)
            drawingRect.top += (drawingRect.bottom - drawingRect.top) / 2
            var drawStr = title.substring(
                0,
                mPaint.breakText(title, true, drawingRect.right - drawingRect.left.toFloat(), null)
            )
            if (drawStr.length < title.length && drawingRect.bottom + mChannelLayoutHeight / 4 > drawingLimit) drawStr =
                drawStr.substring(0, drawStr.length - 3) + "..."
            canvas.drawText(
                drawStr,
                drawingRect.left.toFloat(),
                drawingRect.top + mMeasuringRect.height() / 2.toFloat(),
                mPaint
            )
            title = title.replace(drawStr, "")
        } while (title.length > 0 && drawingRect.bottom + mChannelLayoutHeight / 4 < drawingLimit)

//        title = event.m_sDescription.replace(title, "");
//        if(title.length() > 0) {
//            int length = title.length();
//            drawingRect.top = drawingRect.bottom + mChannelLayoutMargin;
//            drawingRect.bottom = drawingRect.top + mChannelLayoutHeight / 2;
//            mPaint.setColor(mEventLayoutTextColor);
//            mPaint.setTextSize(mEventLayoutTextSize);
//            mPaint.getTextBounds(title, 0, title.length(), mMeasuringRect);
//            drawingRect.top += ((drawingRect.bottom - drawingRect.top) / 2);
//            title = title.substring(0,
//                    mPaint.breakText(title, true, drawingRect.right - drawingRect.left, null));
//            if(title.length() < length) {
//                title = title.substring(0, title.length() - 3) + "...";
//            }
//            canvas.drawText(title, drawingRect.left, drawingRect.top + mMeasuringRect.height()/2, mPaint);
//        }
    }

    private fun setEventDrawingRectangle(
        channelPosition: Int,
        start: Long,
        end: Long,
        drawingRect: Rect
    ) {
        drawingRect.left = getXFrom(start)
        drawingRect.top = getTopFrom(channelPosition)
        if (m_bIsEnlarge && channelPosition > selectedChannelIndex) drawingRect.top += 2 * (mChannelLayoutHeight + mChannelLayoutMargin)
        drawingRect.right = getXFrom(end) - mChannelLayoutMargin
        drawingRect.bottom = drawingRect.top + mChannelLayoutHeight
    }

    private fun drawChannelListItems(
        canvas: Canvas,
        drawingRect: Rect
    ) {
        // Background
        mMeasuringRect.left = scrollX
        mMeasuringRect.top = scrollY
        mMeasuringRect.right = drawingRect.left + mChannelLayoutWidth
        mMeasuringRect.bottom = mMeasuringRect.top + height
        mPaint.color = mChannelLayoutBackground
        val firstPos = firstVisibleChannelPosition
        val lastPos = lastVisibleChannelPosition
        for (pos in firstPos..lastPos) {
            drawChannelItem(canvas, pos, drawingRect)
        }
    }

    private fun drawChannelItem(
        canvas: Canvas,
        position: Int,
        drawingRect: Rect
    ) {
        var drawingRect = drawingRect
        drawingRect.left = scrollX
        drawingRect.top = getTopFrom(position)
        drawingRect.right = drawingRect.left + mChannelLayoutWidth
        drawingRect.bottom = drawingRect.top + mChannelLayoutHeight
        if (m_bIsEnlarge) {
            if (position == selectedChannelIndex) {
                drawingRect.bottom += 2 * (mChannelLayoutHeight + mChannelLayoutMargin)
            } else if (position > selectedChannelIndex) {
                drawingRect.top += 2 * (mChannelLayoutHeight + mChannelLayoutMargin)
                drawingRect.bottom += 2 * (mChannelLayoutHeight + mChannelLayoutMargin)
            }
        }

        //draw Channel Background
        if (position == selectedChannelIndex) mPaint.color =
            mEventLayoutBackgroundCurrent else mPaint.color = mEventLayoutBackground
        canvas.drawRect(drawingRect, mPaint)
        val imageURL = epgData!![position]!!.m_sStreamIcon
        if (m_bIsEnlarge && position == selectedChannelIndex) {
            //draw Channel Number and Channel Name
            drawingRect.top += mChannelLayoutHeight * 1.5.toInt()
            drawingRect.left += mChannelNumberWidth / 4
            drawingRect.right -= mChannelNumberWidth / 4
            mPaint.textSize = resources.getDimension(R.dimen._13sdp).toFloat()
            var channelName = epgData!![position]!!.m_sTvName
            if (CommonLists.MLBCHANNEL.contains(epgData!![position])) channelName =
                "MLB CHANNELS" else if (CommonLists.NBACHANNEL.contains(epgData!![position])) channelName =
                "NBA CHANNELS" else if (CommonLists.NFLCHANNEL.contains(epgData!![position])) channelName =
                "NFL CHANNELS" else if (CommonLists.NHLCHANNEL.contains(epgData!![position])) channelName =
                "NHL CHANNELS" else if (CommonLists.EPLCHANNEL.contains(epgData!![position])) channelName =
                "EPL CHANNELS" else if (CommonLists.PPVCHANNEL.contains(epgData!![position])) channelName =
                "PPV CHANNELS"
            var channelNumber = epgData!![position]!!.m_sTvNum + "  " + channelName
            mPaint.getTextBounds(channelNumber, 0, channelNumber.length, mMeasuringRect)
            val drawTextTop =
                drawingRect.top + ((drawingRect.bottom - drawingRect.top) / 2 + mMeasuringRect.height() / 2)
            val maxWidth = mChannelLayoutWidth - mChannelNumberWidth / 2
            channelNumber = channelNumber.substring(
                0,
                mPaint.breakText(channelNumber, true, maxWidth.toFloat(), null)
            )
            mPaint.color = mEventLayoutTextColor
            if (mMeasuringRect.width() < maxWidth) canvas.drawText(
                channelNumber,
                drawingRect.left + (maxWidth - mMeasuringRect.width()) / 2.toFloat(),
                drawTextTop.toFloat(),
                mPaint
            ) else canvas.drawText(
                channelNumber,
                drawingRect.left.toFloat(),
                drawTextTop.toFloat(),
                mPaint
            )

            // Loading channel image into target for
            drawingRect.top = getTopFrom(position)
            drawingRect.bottom -= (1.5 * mChannelLayoutHeight).toInt()
        } else {
            //drawChannel Number
            var channelNumber = epgData!![position]!!.m_sTvNum.toString()
            mPaint.getTextBounds(channelNumber, 0, channelNumber.length, mMeasuringRect)
            val drawTextTop =
                drawingRect.top + ((drawingRect.bottom - drawingRect.top) / 2 + mMeasuringRect.height() / 2)
            channelNumber = channelNumber.substring(
                0,
                mPaint.breakText(
                    channelNumber,
                    true,
                    drawingRect.right - drawingRect.left.toFloat(),
                    null
                )
            )
            mPaint.color = mEventLayoutTextColor
            mPaint.textSize = mEventLayoutTextSize.toFloat()
            canvas.drawText(
                channelNumber,
                drawingRect.left + (mChannelNumberWidth - mMeasuringRect.width()) / 2.toFloat(),
                drawTextTop.toFloat(),
                mPaint
            )

            // Loading channel image into target for
            drawingRect.left += mChannelNumberWidth + mChannelLayoutPadding
        }

        //draw channel logo image
        if (CommonLists.CHANNEL_IMAGE_CACHE.containsKey(imageURL)) {
            val image: Bitmap = CommonLists.CHANNEL_IMAGE_CACHE.get(imageURL)!!
            drawingRect = getDrawingRectForChannelImage(drawingRect, image)
            canvas.drawBitmap(image, null, drawingRect, null)
        } else {
            if (!CommonLists.CHANNEL_IMAGE_CACHE.containsKey(imageURL)) {
                CommonLists.CHANNEL_IMAGE_TARGET_CACHE[imageURL.toString()] = object : Target {
                    override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                        CommonLists.CHANNEL_IMAGE_CACHE.put(imageURL.toString(), bitmap!!)
                        redraw()
                        CommonLists.CHANNEL_IMAGE_TARGET_CACHE.remove(imageURL)
                    }

                    override fun onBitmapFailed(
                        e: Exception?,
                        errorDrawable: Drawable?
                    ) {
                    }

                    override fun onPrepareLoad(placeHolderDrawable: Drawable?) {}
                }
                if (!imageURL!!.isEmpty()) Utils.getUtils().loadImageInto(
                    context,
                    imageURL,
                    mChannelLayoutWidth - mChannelNumberWidth,
                    mChannelLayoutHeight,
                    CommonLists.CHANNEL_IMAGE_TARGET_CACHE.get(imageURL)
                )
            }
        }
    }

    private fun getDrawingRectForChannelImage(
        drawingRect: Rect,
        image: Bitmap?
    ): Rect {
        drawingRect.left += mChannelLayoutPadding
        drawingRect.top += mChannelLayoutPadding
        drawingRect.right -= mChannelLayoutPadding
        drawingRect.bottom -= mChannelLayoutPadding
        val imageWidth = image!!.width
        val imageHeight = image.height
        val imageRatio = imageHeight / imageWidth.toFloat()
        val rectWidth = drawingRect.right - drawingRect.left
        val rectHeight = drawingRect.bottom - drawingRect.top

        // Keep aspect ratio.
        if (imageWidth > imageHeight) {
            val padding = (rectHeight - rectWidth * imageRatio).toInt() / 2
            drawingRect.top += padding
            drawingRect.bottom -= padding
        } else {
            val padding = (rectWidth - rectHeight / imageRatio).toInt() / 2
            drawingRect.left += padding
            drawingRect.right -= padding
        }
        return drawingRect
    }

    private fun shouldDrawTimeLine(now: Long): Boolean {
        return now >= mTimeLowerBoundary && now < mTimeUpperBoundary
    }

    private fun isEventVisible(start: Long, end: Long): Boolean {
        return (start >= mTimeLowerBoundary && start <= mTimeUpperBoundary
                || end >= mTimeLowerBoundary && end <= mTimeUpperBoundary
                || start <= mTimeLowerBoundary && end >= mTimeUpperBoundary)
    }

    private fun calculatedBaseLine(): Long {
        return Calendar.getInstance()
            .timeInMillis - DAYS_BACK_MILLIS
    }

    private val firstVisibleChannelPosition: Int
        private get() {
            val y = scrollY
            var position =
                (y - mChannelLayoutMargin - mTimeBarHeight) / (mChannelLayoutHeight + mChannelLayoutMargin)
            if (m_bIsEnlarge) {
                position -= 2
            }
            if (position < 0) {
                position = 0
            }
            return position
        }

    // Add one extra row if we don't fill screen with current..
    private val lastVisibleChannelPosition: Int
        private get() {
            val y = scrollY
            val totalChannelCount = epgData!!.size
            val screenHeight = height
            var position = ((y + screenHeight + mTimeBarHeight - mChannelLayoutMargin)
                    / (mChannelLayoutHeight + mChannelLayoutMargin))
            if (position > totalChannelCount - 1) {
                position = totalChannelCount - 1
            }

            // Add one extra row if we don't fill screen with current..
            return if (y + screenHeight > position * mChannelLayoutHeight && position < totalChannelCount - 1) position + 1 else position
        }

    private fun calculateMaxHorizontalScroll() {
        mMaxHorizontalScroll =
            ((DAYS_BACK_MILLIS + DAYS_FORWARD_MILLIS - HOURS_IN_VIEWPORT_MILLIS) / mMillisPerPixel).toInt()
    }

    private fun calculateMaxVerticalScroll() {
        var maxVerticalScroll =
            getTopFrom(epgData!!.size - 1) + mChannelLayoutHeight + mChannelLayoutMargin
        if (m_bIsEnlarge) maxVerticalScroll += 2 * (mChannelLayoutHeight + mChannelLayoutMargin)
        mMaxVerticalScroll =
            if (maxVerticalScroll < height) 0 else maxVerticalScroll - height
    }

    private fun getXFrom(time: Long): Int {
        return (((time - mTimeOffset) / mMillisPerPixel).toInt() + mChannelLayoutMargin
                + mChannelLayoutWidth + mChannelLayoutMargin)
    }

    private fun getTopFrom(position: Int): Int {
        return position * (mChannelLayoutHeight + mChannelLayoutMargin) + mChannelLayoutMargin + mTimeBarHeight
    }

    private fun getTimeFrom(x: Int): Long {
        return x * mMillisPerPixel + mTimeOffset
    }

    private fun calculateMillisPerPixel(): Long {
        return (HOURS_IN_VIEWPORT_MILLIS / (resources.displayMetrics.widthPixels - mChannelLayoutWidth - mChannelLayoutMargin)).toLong()
    }

    private val xPositionStart: Int
        private get() = getXFrom(System.currentTimeMillis() - HOURS_IN_VIEWPORT_MILLIS / 2)

    private fun resetBoundaries() {
        mMillisPerPixel = calculateMillisPerPixel()
        mTimeOffset = calculatedBaseLine()
        mTimeLowerBoundary = getTimeFrom(0)
        mTimeUpperBoundary = getTimeFrom(width)
    }

    private fun calculateChannelsHitArea(): Rect {
        mMeasuringRect.top = mTimeBarHeight
        val visibleChannelsHeight =
            epgData!!.size * (mChannelLayoutHeight + mChannelLayoutMargin)
        mMeasuringRect.bottom =
            if (visibleChannelsHeight < height) visibleChannelsHeight else height
        mMeasuringRect.left = 0
        mMeasuringRect.right = mChannelLayoutWidth
        return mMeasuringRect
    }

    private fun calculateProgramsHitArea(): Rect {
        mMeasuringRect.top = mTimeBarHeight
        val visibleChannelsHeight =
            epgData!!.size * (mChannelLayoutHeight + mChannelLayoutMargin)
        mMeasuringRect.bottom =
            if (visibleChannelsHeight < height) visibleChannelsHeight else height
        mMeasuringRect.left = mChannelLayoutWidth
        mMeasuringRect.right = width
        return mMeasuringRect
    }

    private fun calculateResetButtonHitArea(): Rect {
        mMeasuringRect.left = scrollX + width - mResetButtonSize - mResetButtonMargin
        mMeasuringRect.top = scrollY + height - mResetButtonSize - mResetButtonMargin
        mMeasuringRect.right = mMeasuringRect.left + mResetButtonSize
        mMeasuringRect.bottom = mMeasuringRect.top + mResetButtonSize
        return mMeasuringRect
    }

    private fun getChannelPosition(y: Int): Int {
        var y = y
        y -= mTimeBarHeight
        val channelPosition: Int
        channelPosition = if (m_bIsEnlarge) {
            val currentChannelPosition =
                mChannelLayoutMargin + selectedChannelIndex * (mChannelLayoutHeight + mChannelLayoutMargin)
            if (y < currentChannelPosition) (y + mChannelLayoutMargin) / (mChannelLayoutHeight + mChannelLayoutMargin) else {
                if (y < currentChannelPosition + 3 * (mChannelLayoutHeight + mChannelLayoutMargin)) {
                    selectedChannelIndex
                } else {
                    (y + mChannelLayoutMargin) / (mChannelLayoutHeight + mChannelLayoutMargin) - 2
                }
            }
        } else {
            (y + mChannelLayoutMargin) / (mChannelLayoutHeight + mChannelLayoutMargin)
        }
        return if (epgData!!.size == 0) -1 else channelPosition
    }

    private fun getProgramPosition(channelPosition: Int, time: Long): Int {
        val arrayItemTopic: ArrayItemTopic = epgData!![channelPosition]!!.m_arrItemTopic
        if (arrayItemTopic != null) {
            for (eventPos in 0 until arrayItemTopic.size) {
                val event: ItemTopic = arrayItemTopic.get(eventPos)!!
                if (event.m_dateTopicStart == null && event.m_dateTopicEnd == null) {
                    return eventPos
                }
                if (event.m_dateTopicStart.getTime() <= time && event.m_dateTopicEnd.getTime() >= time) {
                    return eventPos
                }
            }
        }
        return -1
    }

    /**
     * Add click listener to the EPG.
     * @param epgClickListener to add.
     */
    fun setEPGClickListener(epgClickListener: EPGClickListener?) {
        mClickListener = epgClickListener
    }

    /**
     * Add data to EPG. This must be set for EPG to able to draw something.
     * @param epgData pass in any implementation of EPGData.
     */
    fun setEPGData(epgData: ArrayChannelItem?) {
        this.epgData = epgData
    }

    /**
     * This will recalculate boundaries, maximal scroll and scroll to start position which is current time.
     * To be used on device rotation etc since the device height and width will change.
     * @param withAnimation true if scroll to current position should be animated.
     */
    fun recalculateAndRedraw(withAnimation: Boolean) {
        if (epgData != null) {
            resetBoundaries()
            calculateMaxVerticalScroll()
            calculateMaxHorizontalScroll()

//            mScroller.startScroll(0, 0,
//                    getXPositionStart() - 0,
//                    0, withAnimation ? 600 : 0);
            if (epgData!!.size > 0) {
                if (selectedChannelIndex == -1 || selectedChannelIndex > epgData!!.size - 1) selectedChannelIndex =
                    0 else {
                    if (selectedTopicIndex != -1) epgData!![selectedChannelIndex]!!.m_arrItemTopic[selectedTopicIndex]!!.m_bIsSelected =
                        false
                }
                if (selectedChannelIndex > -1 && epgData!![selectedChannelIndex]!!.currentTopicIndex > -1) selectTopicByTime(
                    epgData!![selectedChannelIndex]!!.m_arrItemTopic[epgData!![selectedChannelIndex]!!.currentTopicIndex]!!.m_dateTopicStart
                )
            } else {
                selectedChannelIndex = -1
                selectedTopicIndex = -1
            }
            redraw()
        }
    }

    /**
     * Does a invalidate() and requestLayout() which causes a redraw of screen.
     */
    fun redraw() {
        invalidate()
        requestLayout()
    }

    /**
     * Clears the local image cache for channel images. Can be used when leaving epg and you want to
     * free some memory. Images will be fetched again when loading EPG next time.
     */
    fun clearEPGImageCache() {
        CommonLists.CHANNEL_IMAGE_CACHE.clear()
    }

    fun clearSelection() {
        if (selectedChannelIndex > -1 && selectedChannelIndex < epgData!!.size) {
            if (selectedTopicIndex > -1 && selectedTopicIndex < epgData!![selectedChannelIndex]!!.m_arrItemTopic.size) epgData!![selectedChannelIndex]!!.m_arrItemTopic[selectedTopicIndex]!!.m_bIsSelected =
                false
        }
        selectedChannelIndex = -1
        selectedTopicIndex = -1
    }

    private inner class OnGestureListener : SimpleOnGestureListener() {
        override fun onSingleTapUp(e: MotionEvent): Boolean {

            // This is absolute coordinate on screen not taking scroll into account.
            val x = e.x.toInt()
            val y = e.y.toInt()

            // Adding scroll to clicked coordinate
            val scrollX = scrollX + x
            val scrollY = scrollY + y
            val channelPosition = getChannelPosition(scrollY)
            if (channelPosition != -1 && mClickListener != null) {
                if (calculateResetButtonHitArea().contains(scrollX, scrollY)) {
                    // Reset button clicked
                    mClickListener!!.onResetButtonClicked()
                } else if (calculateChannelsHitArea().contains(x, y)) {
                    if (channelPosition == selectedChannelIndex) {
                        mClickListener!!.onChannelClicked(
                            selectedChannelIndex,
                            epgData!![selectedChannelIndex]
                        )
                    } else setCurrentChannel(channelPosition)
                } else if (calculateProgramsHitArea().contains(x, y)) {
                    // Event area is clicked
                    val selectedTopic = getProgramPosition(
                        channelPosition,
                        getTimeFrom(scrollX - mChannelLayoutWidth)
                    )
                    epgData!![selectedChannelIndex]!!.m_arrItemTopic[selectedTopicIndex]!!.m_bIsSelected =
                        false
                    selectedChannelIndex = channelPosition
                    selectedTopicIndex = selectedTopic
                    epgData!![selectedChannelIndex]!!.m_arrItemTopic[selectedTopicIndex]!!.m_bIsSelected =
                        true
                    redraw()
                    mClickListener!!.onEventClicked(
                        selectedChannelIndex,
                        selectedTopicIndex,
                        epgData!![selectedChannelIndex]!!.m_arrItemTopic[selectedTopicIndex]
                    )
                }
            }
            return true
        }

        override fun onScroll(
            e1: MotionEvent, e2: MotionEvent,
            distanceX: Float, distanceY: Float
        ): Boolean {
            var dx = distanceX.toInt()
            var dy = distanceY.toInt()
            val x = scrollX
            val y = scrollY


            // Avoid over scrolling
            if (x + dx < 0) {
                dx = 0 - x
            }
            if (y + dy < 0) {
                dy = 0 - y
            }
            if (x + dx > mMaxHorizontalScroll) {
                dx = mMaxHorizontalScroll - x
            }
            if (y + dy > mMaxVerticalScroll) {
                dy = mMaxVerticalScroll - y
            }
            scrollBy(dx, dy)
            return true
        }

        override fun onFling(
            e1: MotionEvent, e2: MotionEvent,
            vX: Float, vY: Float
        ): Boolean {
            mScroller.fling(
                scrollX, scrollY, (-vX).toInt(),
                (-vY).toInt(), 0, mMaxHorizontalScroll, 0, mMaxVerticalScroll
            )
            redraw()
            return true
        }

        override fun onDown(e: MotionEvent): Boolean {
            if (!mScroller.isFinished) {
                mScroller.forceFinished(true)
                return true
            }
            return true
        }

        override fun onLongPress(e: MotionEvent) {
            val x = e.x.toInt()
            val y = e.y.toInt()

            // Adding scroll to clicked coordinate
            val scrollX = scrollX + x
            val scrollY = scrollY + y
            val selectedChannel = getChannelPosition(scrollY)
            if (selectedChannel != -1 && mClickListener != null) {
                if (calculateResetButtonHitArea().contains(scrollX, scrollY)) {
                    // Reset button clicked
                    mClickListener!!.onResetButtonClicked()
                } else if (calculateProgramsHitArea().contains(x, y)) {
                    val selectedTopic = getProgramPosition(
                        selectedChannel,
                        getTimeFrom(scrollX - mChannelLayoutWidth)
                    )
                    if (selectedChannel == selectedChannelIndex && selectedTopic == selectedTopicIndex) {
                        mClickListener!!.onLongClicked(
                            selectedChannelIndex,
                            epgData!![selectedChannelIndex]!!.m_arrItemTopic[selectedTopicIndex]
                        )
                    }
                }
            }
        }
    }

    interface EPGClickListener {
        fun onChannelClicked(channelPosition: Int, epgChannel: ChannelItem?)
        fun onEventClicked(
            channelPosition: Int,
            programPosition: Int,
            epgEvent: ItemTopic?
        )

        fun onResetButtonClicked()
        fun onLongClicked(channelPosition: Int, epgEvent: ItemTopic?)
    }

    fun updateEnlargeOption() {
        m_bIsEnlarge = !m_bIsEnlarge
        Utils.getUtils().setBoolean(Constants.ENLARGE, m_bIsEnlarge)
        calculateMaxVerticalScroll()
        invalidate()
    }

    companion object {
        const val DAYS_BACK_MILLIS = 3 * 24 * 60 * 60 * 1000 // 3 days
        const val DAYS_FORWARD_MILLIS = 3 * 24 * 60 * 60 * 1000 // 3 days
        const val HOURS_IN_VIEWPORT_MILLIS = 2 * 60 * 60 * 1000 // 2 hours
        const val TIME_LABEL_SPACING_MILLIS = 30 * 60 * 1000 // 30 minutes
    }

    init {
        setWillNotDraw(false)
        resetBoundaries()
        mDrawingRect = Rect()
        mClipRect = Rect()
        mMeasuringRect = Rect()
        mPaint = Paint(Paint.ANTI_ALIAS_FLAG)
        mGestureDetector =
            GestureDetector(context, OnGestureListener())

        // Adding some friction that makes the epg less flappy.
        mScroller = Scroller(context)
        mScroller.setFriction(0.2f)
        mChannelLayoutMargin = resources.getDimension(R.dimen._2sdp).toInt()
        mChannelLayoutPadding = resources.getDimension(R.dimen._5sdp).toInt()
        mChannelNumberWidth = resources.getDimension(R.dimen._40sdp).toInt()
        mChannelLayoutHeight = resources.getDimension(R.dimen._18sdp).toInt()
        mChannelLayoutWidth =
            resources.getDimension(R.dimen._70sdp).toInt() + mChannelNumberWidth
        mChannelLayoutBackground = resources.getColor(R.color.item_back_color)
        mEventLayoutBackground = resources.getColor(R.color.item_back_color)
        mEventLayoutBackgroundCurrent = resources.getColor(R.color.item_back_selected_color)
        mEventLayoutTextColor = Color.WHITE
        mEventLayoutTextSize = resources.getDimension(R.dimen._10ssp).toInt()
        mTimeBarHeight = resources.getDimension(R.dimen._20ssp).toInt()
        mTimeBarTextSize = resources.getDimension(R.dimen._10ssp).toInt()
        mTimeBarLineWidth = resources.getDimension(R.dimen._30sdp).toInt()
        mTimeBarLineColor = resources.getColor(R.color.timeline_stick)
        mResetButtonSize = resources.getDimension(R.dimen._16sdp).toInt()
        mResetButtonMargin = resources.getDimension(R.dimen._10sdp).toInt()
        val options = BitmapFactory.Options()
        options.outWidth = mResetButtonSize
        options.outHeight = mResetButtonSize
        selectedChannelIndex = -1
        selectedTopicIndex = -1
        m_bIsScrollLocked = false
        m_bIsEnlarge = java.lang.Boolean.parseBoolean(
            Utils.getUtils().getBoolean(
                Constants.ENLARGE
            ).toString()
        )
        setKeyListener()
        timeLineHandler.postDelayed(runnableTimeLineUpdate, 5000)
    }
}