package com.app.couchpotato.base

import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.ImageButton
import android.widget.ProgressBar
import android.widget.TextView
import com.app.couchpotato.R
import com.app.couchpotato.helperutils.Constants
import com.app.couchpotato.helperutils.Utils
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.hls.HlsMediaSource
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.trackselection.TrackSelection
import com.google.android.exoplayer2.ui.DefaultTimeBar
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.*
import com.google.android.exoplayer2.util.Util

open class PlayerBaseActivity : BaseActivity(), View.OnClickListener {
    protected var exo_progress: DefaultTimeBar? = null
    protected lateinit var btnPlay: ImageButton
    protected lateinit var btnReplay: ImageButton
    protected lateinit var btnForward: ImageButton
    protected lateinit var btnPrev: ImageButton
    protected lateinit var btnNext: ImageButton
    protected lateinit var btnClosedCaption: ImageButton
    protected lateinit var btnAudioTrack: ImageButton
    protected lateinit var tvTitle: TextView

    // video player
    protected var playerView: PlayerView? = null
    protected var player: SimpleExoPlayer? = null
    protected var window: Timeline.Window? = null
    protected var mediaDataSourceFactory: DataSource.Factory? =
        null
    protected var trackSelector: DefaultTrackSelector? = null
    protected var shouldAutoPlay = false
    protected var bandwidthMeter: BandwidthMeter? = null
    protected var playWhenReady = false
    protected var currentWindow = 0
    protected var playbackPosition: Long = 0
    protected var streamUrl: String? = null
    protected var progressBar: ProgressBar? = null
    protected var reconnectHandler = Handler()
    protected var reconnectRunnable = Runnable {
        currentWindow = C.INDEX_UNSET
        playbackPosition = C.INDEX_UNSET.toLong()
        initializePlayer()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //video player
        if (savedInstanceState == null) {
            playWhenReady = true
            currentWindow = 0
            playbackPosition = 0
        } else {
            playWhenReady =
                savedInstanceState.getBoolean(KEY_PLAY_WHEN_READY)
            currentWindow = savedInstanceState.getInt(KEY_WINDOW)
            playbackPosition = savedInstanceState.getLong(KEY_POSITION)
        }

        // Load All Demands
        shouldAutoPlay = true
        bandwidthMeter = DefaultBandwidthMeter()
        mediaDataSourceFactory = DefaultDataSourceFactory(
            this,
            Util.getUserAgent(this, "mediaPlayerSample"),
            bandwidthMeter as TransferListener?
        )
        window = Timeline.Window()
    }

    public override fun onStart() {
        super.onStart()
        if (Util.SDK_INT > 23 && streamUrl != null && !streamUrl!!.isEmpty()) {
            initializePlayer()
        }
    }

    public override fun onResume() {
        super.onResume()
        if (Util.SDK_INT < 23 && streamUrl != null && !streamUrl!!.isEmpty()) {
            initializePlayer()
        }
    }

    public override fun onPause() {
        super.onPause()
        reconnectHandler.removeCallbacks(reconnectRunnable)
        if (Util.SDK_INT <= 23) {
            releasePlayer()
        }
    }

    public override fun onStop() {
        super.onStop()
        reconnectHandler.removeCallbacks(reconnectRunnable)
        if (Util.SDK_INT > 23) {
            releasePlayer()
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        updateStartPosition()
        outState.putBoolean(KEY_PLAY_WHEN_READY, playWhenReady)
        outState.putInt(KEY_WINDOW, currentWindow)
        outState.putLong(KEY_POSITION, playbackPosition)
        super.onSaveInstanceState(outState)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btnPlay -> onPlay()
            R.id.btnForward -> onForward()
            R.id.btnReplay -> onReplay()
            R.id.btnNext -> onNext()
            R.id.btnPrev -> onPrev()
            R.id.btnClosedCaption -> {
                onCC()
                onAudioTrack()
            }
            R.id.btnAudioTrack -> onAudioTrack()
        }
    }

    protected open fun initControl() {
        exo_progress = findViewById(R.id.exo_progress)
        btnPlay = findViewById(R.id.btnPlay)
        btnReplay = findViewById(R.id.btnReplay)
        btnForward = findViewById(R.id.btnForward)
        btnPrev = findViewById(R.id.btnPrev)
        btnNext = findViewById(R.id.btnNext)
        btnClosedCaption = findViewById(R.id.btnClosedCaption)
        btnAudioTrack = findViewById(R.id.btnAudioTrack)
        tvTitle = findViewById(R.id.tvTitle)
    }

    open fun setEventListener() {
        btnPlay!!.setOnClickListener(this)
        btnForward!!.setOnClickListener(this)
        btnReplay!!.setOnClickListener(this)
        btnPrev!!.setOnClickListener(this)
        btnNext!!.setOnClickListener(this)
        btnClosedCaption!!.setOnClickListener(this)
        btnAudioTrack!!.setOnClickListener(this)
    }

    protected open fun onPlay() {
        if (player!!.playWhenReady) {
            onPauseVideo()
            if (btnPlay!!.visibility == View.VISIBLE) {
                btnPlay!!.requestFocus()
            }
        } else {
            onPlayVideo()
        }
    }

    protected open fun onForward() {
        val curPos = player!!.currentPosition
        val duration = player!!.duration
        val seekPos = Math.min(curPos + Constants.SEEK_OFFSET, duration)
        player!!.seekTo(seekPos)
    }

    protected open fun onReplay() {
        val curPos = player!!.currentPosition
        val seekPos: Long =
            if (curPos - Constants.SEEK_OFFSET < 0) 0 else curPos - Constants.SEEK_OFFSET.toLong()
        player!!.seekTo(seekPos)
    }

    protected open fun onPrev() {}
    protected open fun onPlayVideo() {
        if (player != null) player!!.playWhenReady = true
        btnPlay!!.setImageResource(R.drawable.player_pause)
    }

    protected open fun onPauseVideo() {
        if (player != null) player!!.playWhenReady = false
        btnPlay!!.setImageResource(R.drawable.player_play)
    }

    protected open fun onNext() {}
    protected open fun onCC() {
        if (playerView!!.subtitleView.visibility == View.VISIBLE) {
            playerView!!.subtitleView.visibility = View.GONE
        } else {
            playerView!!.subtitleView.visibility = View.VISIBLE
        }
    }

    protected open fun onAudioTrack() {}
    protected fun updateStartPosition() {
        if (player == null) return
        playbackPosition = player!!.currentPosition
        currentWindow = player!!.currentWindowIndex
        playWhenReady = player!!.playWhenReady
    }

    protected open fun initializePlayer() {
        if (player == null) {
            progressBar!!.visibility = View.GONE
            val videoTrackSelectionFactory: TrackSelection.Factory =
                AdaptiveTrackSelection.Factory()
            trackSelector = DefaultTrackSelector(videoTrackSelectionFactory)

            // Here
            val allocator = DefaultAllocator(true, C.DEFAULT_BUFFER_SEGMENT_SIZE)
            val loadControl =
                DefaultLoadControl(allocator, 360000, 600000, 2500, 10000, -1, true)
            //***
            player = ExoPlayerFactory.newSimpleInstance(
                this@PlayerBaseActivity,
                DefaultRenderersFactory(this),
                trackSelector,
                loadControl
            )
            playerView!!.setPlayer(player)
            playerView!!.controllerShowTimeoutMs = 3000
            player?.playWhenReady = shouldAutoPlay
            player?.addListener(PlayerEventListener())
        }
        val mediaSource: MediaSource
        mediaSource =
            if (streamUrl!!.endsWith(".m3u8")) HlsMediaSource.Factory(mediaDataSourceFactory)
                .createMediaSource(Uri.parse(Utils.getUtils().makeAvaiableUrl(streamUrl!!))) else ExtractorMediaSource.Factory(
                mediaDataSourceFactory
            )
                .createMediaSource(Uri.parse(Utils.getUtils().makeAvaiableUrl(streamUrl!!)))
        val haveStartPosition = currentWindow != C.INDEX_UNSET
        if (haveStartPosition) {
            player!!.seekTo(currentWindow, playbackPosition)
        }
        player!!.prepare(mediaSource, !haveStartPosition, false)
    }

    protected open fun releasePlayer() {
        if (player != null) {
            updateStartPosition()
            shouldAutoPlay = player!!.playWhenReady
            player!!.release()
            player = null
            trackSelector = null
        }
    }

    protected inner class PlayerEventListener : Player.EventListener {
        override fun onPlayerStateChanged(
            playWhenReady: Boolean,
            playbackState: Int
        ) {
            if (playbackState == Player.STATE_BUFFERING) {
                progressBar!!.visibility = View.VISIBLE
            }
            if (playbackState == Player.STATE_READY) {
                progressBar!!.visibility = View.GONE
            }
            playerView!!.keepScreenOn = !(playbackState == Player.STATE_IDLE || playbackState == Player.STATE_ENDED ||
                    !playWhenReady)
        }

        override fun onPlayerError(e: ExoPlaybackException) {
            Thread(Runnable {
                if (Utils.getUtils().getConnectivityStatus(this@PlayerBaseActivity) != Utils.getUtils().TYPE_NOT_CONNECTED) {
                    runOnUiThread { progressBar!!.visibility = View.GONE }
                } else {
                    player!!.stop()
                    playerView!!.visibility = View.INVISIBLE
                    runOnUiThread { progressBar!!.visibility = View.GONE }
                }
            }).start()
            reconnectHandler.postDelayed(reconnectRunnable,
                Constants.RECONNECT_NEXT_TIME_OUT.toLong()
            )
        }
    }

    companion object {
        private const val KEY_PLAY_WHEN_READY = "play_when_ready"
        private const val KEY_WINDOW = "window"
        private const val KEY_POSITION = "position"
    }
}