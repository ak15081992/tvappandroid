package com.app.couchpotato.base

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.text.format.DateFormat
import android.view.LayoutInflater
import android.widget.ImageView
import android.widget.TextView
import com.app.couchpotato.R
import com.app.couchpotato.commonmodel.ChannelItem
import com.app.couchpotato.commonmodel.CommonLists
import com.app.couchpotato.commonmodel.ItemTopic
import com.app.couchpotato.helperutils.Utils
import com.squareup.picasso.Picasso.LoadedFrom
import com.squareup.picasso.Target
import java.text.SimpleDateFormat
import java.util.*

class ReminderReceiver : BroadcastReceiver() {
    private val reminderDlg: Dialog? = null
    private var ivChannelThumb: ImageView? = null
    private var tvChannelName: TextView? = null
    private var tvChannelNumber: TextView? = null
    private var tvEpgName: TextView? = null
    private var tvEpgDuration: TextView? = null
    private var tvEpgDescription: TextView? = null
    private var tvReminderBtn: TextView? = null
    private var tvCurrentTime: TextView? = null
    private var activityContext: BaseActivity? = null
    private var streamIndex = -1
    fun setActivityContext(activityContext: BaseActivity?) {
        this.activityContext = activityContext
    }

    override fun onReceive(context: Context, intent: Intent) {
        if (intent.action != "ReminderAction" || activityContext == null
        ) return
        if (reminderDlg == null) {
            val inflater =
                activityContext!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            @SuppressLint("InflateParams") val reminderView =
                inflater.inflate(R.layout.reminder_alert_dialog, null, false)
            ivChannelThumb =
                reminderView.findViewById(R.id.ID_CHANNEL_THUMBNAIL)
            tvChannelName = reminderView.findViewById(R.id.ID_CHANNEL_NAME)
            tvChannelNumber = reminderView.findViewById(R.id.ID_CHANNEL_NUMBER)
            tvEpgName = reminderView.findViewById(R.id.ID_TEXT_VIEW_EPG_NAME)
            tvEpgDuration = reminderView.findViewById(R.id.ID_TEXT_VIEW_TIME)
            tvEpgDescription = reminderView.findViewById(R.id.ID_TEXT_VIEW_DESCRIPTION)
            tvReminderBtn = reminderView.findViewById(R.id.ID_BUTTON_REMIND)
            val tvRemoveBtn = reminderView.findViewById<TextView>(R.id.ID_BUTTON_REMOVE)
            tvCurrentTime = reminderView.findViewById(R.id.ID_TEXT_VIEW_CURRENT_TIME)

            /*tvReminderBtn.setOnClickListener(v -> {
                if(activityContext instanceof GuideActivity) {
                    ((GuideActivity)activityContext).filterChannel(activityContext.getString(R.string.all_channels));
                    Intent intent1 = new Intent(activityContext, LivePlayerActivity.class);
                    intent1.putExtra(AppConstants.TAG_CHANNEL_INDEX, streamIndex);
                    activityContext.startActivityForResult(intent1, AppConstants.REQUEST_LIVE_CODE);
                }
                else if(activityContext instanceof LivePlayerActivity) {
                    ((LivePlayerActivity)activityContext).updatedCurrentChannel(streamIndex);
                }
                else {
                    AppConstants.EPG_FILTERED_DATA.clear();
                    AppConstants.EPG_FILTERED_DATA.addAll(AppConstants.EPGDATA);
                    Intent intent1 = new Intent(activityContext, LivePlayerActivity.class);
                    intent1.putExtra(AppConstants.TAG_CHANNEL_INDEX, streamIndex);
                    activityContext.startActivity(intent1);
                }
                AppConstants.REMINDER_TOPIC_ARRAY.remove(0);
                reminderDlg.dismiss();
            });

            tvRemoveBtn.setOnClickListener(v -> {
                AppConstants.REMINDER_TOPIC_ARRAY.remove(0);
                reminderDlg.dismiss();
            });

            reminderDlg = new Dialog(activityContext, R.style.Theme_CustomDialog);
            reminderDlg.setContentView(reminderView);*/
        }
        if (updateCurrentTopicInfo(activityContext!!) && !reminderDlg!!.isShowing) {
            reminderDlg.show()
            tvReminderBtn!!.requestFocus()
        }
    }

    private fun updateCurrentTopicInfo(context: Context): Boolean {
        var tempItem: ChannelItem? = null
        val itemTopic: ItemTopic = CommonLists.REMINDER_TOPIC_ARRAY.get(0)
        streamIndex = 0
        for (item in CommonLists.EPGDATA) {
            if (item?.m_sEPGChannelID == itemTopic.m_sChannelID) {
                tempItem = item
                break
            }
            streamIndex++
        }
        if (tempItem == null) return false
        val channelItem: ChannelItem = tempItem
        if (CommonLists.CHANNEL_IMAGE_CACHE.containsKey(channelItem.m_sStreamIcon)) {
            val image: Bitmap = CommonLists.CHANNEL_IMAGE_CACHE.get(channelItem.m_sStreamIcon)!!
            val drawable: Drawable =
                BitmapDrawable(context.resources, image)
            ivChannelThumb!!.background = drawable
        } else {
            if (!CommonLists.CHANNEL_IMAGE_CACHE.containsKey(channelItem.m_sStreamIcon)) {
                CommonLists.CHANNEL_IMAGE_TARGET_CACHE.put(
                    channelItem.m_sStreamIcon.toString(),
                    object : Target {
                        override fun onBitmapLoaded(
                            bitmap: Bitmap,
                            from: LoadedFrom
                        ) {
                            CommonLists.CHANNEL_IMAGE_CACHE.put(channelItem.m_sStreamIcon.toString(), bitmap)
                            val drawable: Drawable =
                                BitmapDrawable(
                                    context.resources,
                                    bitmap
                                )
                            ivChannelThumb!!.background = drawable
                            CommonLists.CHANNEL_IMAGE_TARGET_CACHE.remove(channelItem.m_sStreamIcon)
                        }

                        override fun onBitmapFailed(
                            e: Exception,
                            errorDrawable: Drawable
                        ) {
                        }

                        override fun onPrepareLoad(placeHolderDrawable: Drawable) {}
                    })
                if (channelItem.m_sStreamIcon != null && !channelItem.m_sStreamIcon!!.isEmpty()) Utils.getUtils().loadImageInto(
                    context,
                    channelItem.m_sStreamIcon,
                    context.resources.getDimension(R.dimen._170sdp).toInt(),
                    context.resources.getDimension(R.dimen._45sdp).toInt(),
                    CommonLists.CHANNEL_IMAGE_TARGET_CACHE[channelItem.m_sStreamIcon.toString()]
                )
            }
        }
        tvChannelName!!.text = channelItem.m_sTvName
        tvChannelNumber!!.text = channelItem.m_sTvNum
        tvEpgName?.setText(itemTopic.m_sTitle)
        val duration: String
        val dayOfTheWeek = DateFormat.format(
            "EEEE",
            itemTopic.m_dateTopicStart
        ) as String // Thursday
        val day = DateFormat.format(
            "dd",
            itemTopic.m_dateTopicStart
        ) as String // 20
        val monthString = DateFormat.format(
            "MMM",
            itemTopic.m_dateTopicStart
        ) as String // Jun
        @SuppressLint("SimpleDateFormat") val sdfs =
            SimpleDateFormat("hh:mm a")
        duration =
            day + " " + monthString + ", " + dayOfTheWeek + " " + sdfs.format(itemTopic.m_dateTopicStart) + " - " + sdfs.format(
                itemTopic.m_dateTopicEnd
            )
        tvEpgDuration!!.text = duration
        tvEpgDescription?.setText(itemTopic.m_sDescription)
        tvCurrentTime?.setText(Utils.getUtils().getShortTime(
            Calendar.getInstance().time.getTime()))
        return true
    }
}