package com.app.couchpotato.base;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.WindowManager;

import androidx.annotation.Nullable;

@SuppressLint("Registered")
public class BaseActivity extends Activity {
    protected ReminderReceiver reminderReceiver;
    protected IntentFilter intentFilter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        reminderReceiver = new ReminderReceiver();
        reminderReceiver.setActivityContext(BaseActivity.this);
        intentFilter = new IntentFilter("ReminderAction");

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(reminderReceiver, intentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();

        unregisterReceiver(reminderReceiver);
    }
}
