package com.app.couchpotato.commonmodel

import com.app.couchpotato.helperutils.Constants
import com.app.couchpotato.helperutils.Utils
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName

data class UserModel(

	@field:SerializedName("user_info")
	val userInfo: UserInfo? = null,

	@field:SerializedName("server_info")
	val serverInfo: ServerInfo? = null
) {

	data class ServerInfo(

		@field:SerializedName("https_port")
		val httpsPort: String? = null,

		@field:SerializedName("timestamp_now")
		val timestampNow: Int? = null,

		@field:SerializedName("port")
		val port: String? = null,

		@field:SerializedName("timezone")
		val timezone: String? = null,

		@field:SerializedName("time_now")
		val timeNow: String? = null,

		@field:SerializedName("rtmp_port")
		val rtmpPort: String? = null,

		@field:SerializedName("url")
		val url: String? = null,

		@field:SerializedName("server_protocol")
		val serverProtocol: String? = null
	)

	data class UserInfo(

		@field:SerializedName("password")
		val password: String? = null,

		@field:SerializedName("max_connections")
		val maxConnections: String? = null,

		@field:SerializedName("active_cons")
		val activeCons: String? = null,

		@field:SerializedName("allowed_output_formats")
		val allowedOutputFormats: List<String?>? = null,

		@field:SerializedName("auth")
		val auth: Int? = null,

		@field:SerializedName("created_at")
		val createdAt: String? = null,

		@field:SerializedName("exp_date")
		val expDate: String? = null,

		@field:SerializedName("message")
		val message: String? = null,

		@field:SerializedName("is_trial")
		val isTrial: String? = null,

		@field:SerializedName("username")
		val username: String? = null,

		@field:SerializedName("status")
		val status: String? = null
	)

	companion object {

		/**
		 * Utils
		 */
		val mUtils= Utils.getUtils()

		var mInstance: UserModel? = null

		fun getUserModel() : UserModel {
			return if (mInstance != null) {
				mInstance as UserModel
			} else {
				if (mUtils.getString(Constants.KEY_USERMODEL).toString()=="null") {
					mInstance =
						UserModel()
					mInstance!!
				} else {
					mInstance = Gson().fromJson(
						mUtils.getString(Constants.KEY_USERMODEL),
						UserModel::class.java)
					return mInstance!!
				}
			}
		}

		fun saveUserModel(userModel: UserModel) {
			mInstance =userModel
			mUtils.setString(Constants.KEY_USERMODEL, Gson().toJson(userModel))
		}
	}
}
