package com.app.couchpotato.commonmodel

import java.util.*

//==============================================================================
class ArrayChannelItem : ArrayList<ChannelItem?>() {
    //------------------------------------------------------------------------------
    fun IsSameWith(a_arrItemChannel: ArrayChannelItem): Boolean {
        var channelItem: ChannelItem?
        if (size != a_arrItemChannel.size) return false
        for (i in this.indices) {
            channelItem = this[i]
            if (!channelItem!!.IsSameWith(a_arrItemChannel[i]!!)) return false
        }
        return true
    } //------------------------------------------------------------------------------
}