package com.app.couchpotato.commonmodel

import java.util.*

//==============================================================================
class ArrayItemTopic : ArrayList<ItemTopic?>() {
    //------------------------------------------------------------------------------
    fun IsSameWith(a_arrItemTopic: ArrayItemTopic): Boolean {
        var itemTopic: ItemTopic?
        if (size != a_arrItemTopic.size) return false
        for (i in this.indices) {
            itemTopic = this[i]
            if (!itemTopic!!.IsSameWith(a_arrItemTopic[i])) return false
        }
        return true
    }

    //------------------------------------------------------------------------------
    fun hasSameTimeRange(topic: ItemTopic): Boolean {
        var itemTopic: ItemTopic?
        var result = false
        for (i in 0 until size) {
            itemTopic = get(i)
            if (topic.m_dateTopicStart.after(itemTopic!!.m_dateTopicStart) && topic.m_dateTopicStart.before(
                    itemTopic.m_dateTopicEnd
                ) || topic.m_dateTopicEnd.after(itemTopic.m_dateTopicStart) && topic.m_dateTopicEnd.before(
                    itemTopic.m_dateTopicEnd
                ) || topic.m_dateTopicStart.before(itemTopic.m_dateTopicStart) && topic.m_dateTopicEnd.after(
                    itemTopic.m_dateTopicEnd
                ) || topic.m_dateTopicStart == itemTopic.m_dateTopicStart || topic.m_dateTopicEnd == itemTopic.m_dateTopicEnd
            ) {
                result = true
                break
            }
        }
        return result
    } //------------------------------------------------------------------------------
}