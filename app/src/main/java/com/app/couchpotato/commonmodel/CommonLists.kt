package com.app.couchpotato.commonmodel

import android.graphics.Bitmap
import com.app.couchpotato.ui.demanddetail.model.ResponseVideoDetail
import com.app.couchpotato.ui.episode.model.EpisodeItem
import com.squareup.picasso.Target

class CommonLists {

    companion object {
        var mCategoryList=ArrayList<CategoryList>()
        val EPG_MAP: HashMap<String, ChannelItem> = HashMap()
        val EPGDATA: ArrayChannelItem = ArrayChannelItem()
        val EPG_CATCH_UP_DATA = ArrayChannelItem()
        val MLBCHANNEL = ArrayChannelItem()
        val EPLCHANNEL = ArrayChannelItem()
        val NBACHANNEL = ArrayChannelItem()
        val NFLCHANNEL = ArrayChannelItem()
        val NHLCHANNEL = ArrayChannelItem()
        val PPVCHANNEL = ArrayChannelItem()
        val FAVORITE_CHANNEL_ARRAY =
            java.util.ArrayList<String>()
        val REMINDER_TOPIC_ARRAY = java.util.ArrayList<ItemTopic>()
        val CHANNEL_IMAGE_CACHE: HashMap<String, Bitmap> = HashMap()
        val CHANNEL_IMAGE_TARGET_CACHE: HashMap<String, Target> =
            HashMap()
        val EPG_FILTERED_DATA = ArrayChannelItem()
        val CHANNEL_CATEGORY_LIST: java.util.ArrayList<LiveCategoryInfo> =
            java.util.ArrayList<LiveCategoryInfo>()

        var MLB_CATEGORY_ID = ""
        var EPL_CATEGORY_ID = ""
        var NBA_CATEGORY_ID = ""
        var NFL_CATEGORY_ID = ""
        var NHL_CATEGORY_ID = ""
        var PPV_CATEGORY_ID = ""
        const val FAVORITE_ARRAY = "FavoriteArray"
        const val REQUEST_LIVE_CODE = 1
        const val TAG_CHANNEL_INDEX = "ChannelIndex"
        val DEMAND_ITEM_ARRAY_LIST: java.util.ArrayList<ResponseVideoDetail.MovieData> =
            java.util.ArrayList<ResponseVideoDetail.MovieData>()
        val EPISODE_ITEM_ARRAY_LIST: java.util.ArrayList<EpisodeItem> =
            java.util.ArrayList<EpisodeItem>()
    }

}