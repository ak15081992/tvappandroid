package com.app.couchpotato.commonmodel

import com.google.gson.annotations.SerializedName

data class CategoryList(

	@field:SerializedName("category_name")
	val categoryName: String? = null,

	@field:SerializedName("category_id")
	val categoryId: String? = null,

	@field:SerializedName("parent_id")
	val parentId: Int? = null
)
