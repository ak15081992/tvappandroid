package com.app.couchpotato.commonmodel

import java.util.*

class ChannelItem {
    var m_sTvNum: String? = null
    var m_sTvName: String? = null
    var m_sStreamType: String? = null
    var m_sStreamID: String? = null
    var m_sStreamIcon: String? = null
    var m_sEPGChannelID: String? = null
    var m_sAdded: String? = null
    var m_sCategory_ID: String? = null
    var m_sCustomSID: String? = null
    var m_sTVArchive: String? = null
    var m_sDirectSource: String? = null
    var m_sTVArchiveDuration: String? = null
    var m_arrItemTopic: ArrayItemTopic

    //------------------------------------------------------------------------------
    constructor() {
        m_arrItemTopic = ArrayItemTopic()
    }

    constructor(channelItem: ChannelItem) {
        m_sTvNum = channelItem.m_sTvNum
        m_sTvName = channelItem.m_sTvName
        m_sStreamType = channelItem.m_sStreamType
        m_sStreamID = channelItem.m_sStreamID
        m_sStreamIcon = channelItem.m_sStreamIcon
        m_sEPGChannelID = channelItem.m_sEPGChannelID
        m_sAdded = channelItem.m_sAdded
        m_sCategory_ID = channelItem.m_sCategory_ID
        m_sCustomSID = channelItem.m_sCustomSID
        m_sTVArchive = channelItem.m_sTVArchive
        m_sDirectSource = channelItem.m_sDirectSource
        m_sTVArchiveDuration = channelItem.m_sTVArchiveDuration
        m_arrItemTopic = ArrayItemTopic()
        for (itemTopic in channelItem.m_arrItemTopic) {
            m_arrItemTopic.add(ItemTopic(itemTopic))
        }
    }

    fun IsSameWith(a_itemChannel: ChannelItem): Boolean {
        if (m_sTvNum != a_itemChannel.m_sTvNum) return false
        if (m_sTvName != a_itemChannel.m_sTvName) return false
        if (m_sStreamType != a_itemChannel.m_sStreamType) return false
        if (m_sStreamID != a_itemChannel.m_sStreamID) return false
        if (m_sStreamIcon != a_itemChannel.m_sStreamIcon) return false
        if (m_sEPGChannelID != a_itemChannel.m_sEPGChannelID) return false
        if (m_sAdded != a_itemChannel.m_sAdded) return false
        if (m_sCategory_ID != a_itemChannel.m_sCategory_ID) return false
        if (m_sCustomSID != a_itemChannel.m_sCustomSID) return false
        if (m_sTVArchive != a_itemChannel.m_sTVArchive) return false
        if (m_sDirectSource != a_itemChannel.m_sDirectSource) return false
        return if (m_sTVArchiveDuration != a_itemChannel.m_sTVArchiveDuration) false else m_arrItemTopic.IsSameWith(
            a_itemChannel.m_arrItemTopic
        )
    }

    val currentTopicIndex: Int
        get() {
            if (m_arrItemTopic.size == 0) return -1
            val currentDate = Calendar.getInstance().time
            var index = 0
            for (topic in m_arrItemTopic) {
                if (topic?.m_dateTopicStart != null && topic.m_dateTopicEnd != null && topic.m_dateTopicStart.before(
                        currentDate
                    ) && topic.m_dateTopicEnd.after(currentDate)
                ) break
                index++
            }
            if (index == m_arrItemTopic.size) index = 0
            return index
        }
}