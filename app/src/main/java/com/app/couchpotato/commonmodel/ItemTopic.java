package com.app.couchpotato.commonmodel;

import java.util.Date;

public class ItemTopic {
    public String m_sChannelNumber;
    private String m_sID;
    private String m_sEpgID;
    public String m_sTitle;
    private String m_sLang;
    private String m_sStart;
    private String m_sEnd;
    public String m_sDescription;
    public String m_sChannelID;
    private String m_sUrl;
    public Date m_dateTopicStart;
    public Date m_dateTopicEnd;
    public boolean  m_bIsSelected;

    //------------------------------------------------------------------------------
    public ItemTopic() {
        m_sChannelNumber = "";
        m_sID           = "";
        m_sEpgID        = "";
        m_sTitle		= "";
        m_sLang         = "";
        m_sStart        = "";
        m_sEnd          = "";
        m_sDescription  = "";
        m_sChannelID =  "";
        m_sUrl          = "";
        m_dateTopicStart	= new Date();
        m_dateTopicEnd		= new Date();
        m_bIsSelected       = false;
    }

    //------------------------------------------------------------------------------
    public ItemTopic(ItemTopic itemTopic) {
        m_sChannelNumber = itemTopic.m_sChannelNumber;
        m_sID           = itemTopic.m_sID;
        m_sEpgID        = itemTopic.m_sEpgID;
        m_sTitle		= itemTopic.m_sTitle;
        m_sLang         = itemTopic.m_sLang;
        m_sStart        = itemTopic.m_sStart;
        m_sEnd          = itemTopic.m_sEnd;
        m_sDescription  = itemTopic.m_sDescription;
        m_sChannelID    = itemTopic.m_sChannelID;
        m_sUrl          = itemTopic.m_sUrl;
        m_dateTopicStart	= itemTopic.m_dateTopicStart == null ? null : new Date(itemTopic.m_dateTopicStart.getTime());
        m_dateTopicEnd		= itemTopic.m_dateTopicEnd == null ? null : new Date(itemTopic.m_dateTopicEnd.getTime());
        m_bIsSelected       = itemTopic.m_bIsSelected;
    }

    //------------------------------------------------------------------------------
    public boolean IsSameWith(ItemTopic a_itemTopic) {
        if (!m_sChannelNumber.equals(a_itemTopic.m_sChannelNumber)) return false;
        if (!m_sID.equals(a_itemTopic.m_sID)) return false;
        if (!m_sEpgID.equals(a_itemTopic.m_sEpgID)) return false;
        if (!m_sTitle.equals(a_itemTopic.m_sTitle)) return false;
        if (!m_sLang.equals(a_itemTopic.m_sLang)) return false;
        if (!m_sStart.equals(a_itemTopic.m_sStart)) return false;
        if (!m_sEnd.equals(a_itemTopic.m_sEnd)) return false;
        if (!m_sDescription.equals(a_itemTopic.m_sDescription)) return false;
        if (!m_sChannelID.equals(a_itemTopic.m_sChannelID)) return false;
        if (!m_sUrl.equals(a_itemTopic.m_sUrl)) return false;
        if (!m_dateTopicStart.equals(a_itemTopic.m_dateTopicStart)) return false;
        return m_dateTopicEnd.equals(a_itemTopic.m_dateTopicEnd);
    }

    //------------------------------------------------------------------------------
}

