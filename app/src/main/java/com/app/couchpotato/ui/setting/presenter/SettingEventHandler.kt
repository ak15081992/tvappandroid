package com.app.couchpotato.ui.setting.presenter

import android.view.View
import android.widget.Toast
import com.app.couchpotato.MyApp
import com.app.couchpotato.customview.loader.LoadingDialog
import com.app.couchpotato.helperutils.Constants
import com.app.couchpotato.helperutils.Utils
import com.app.couchpotato.ui.setting.SettingActivity
import kotlinx.android.synthetic.main.activity_setting.*

class SettingEventHandler(private val mActivity: SettingActivity) {

    /**
     * Utils
     */
    val mUtils = Utils.getUtils()

    /**
     * Assinging LoadingDialog's singleton object into @mProgressLoader
     */
    private val mProgressLoader = LoadingDialog.getLoader()

    /**
     * AppComponent of Dagger
     */
    var mAppComponent = MyApp.get(mActivity).getAppComponents()

    fun goBack() {
        mUtils.finishActivity(
            mActivity,
            false
        )
    }

    fun onHardwareAccelerationClicked(view: View) {
        mActivity.swHardwareAccleration.isChecked=!mActivity.swHardwareAccleration.isChecked
        mUtils.setString(Constants.HARDWARE_ACCELERATION,(mActivity.swHardwareAccleration.isChecked).toString())
    }

    fun onParentalLockClicked(view: View) {
        mActivity.showPassDialog()
    }

    fun onResetTvGuideClicked(view: View) {
        mUtils.setString(Constants.FAVORITE_ARRAY,"null")
        Toast.makeText(mActivity, "Favorite Channels Reset.", Toast.LENGTH_LONG).show()
    }

}