package com.app.couchpotato.ui.tvondemand.model

import android.os.Parcelable
import com.app.couchpotato.helperutils.Constants
import com.app.couchpotato.helperutils.Utils
import com.app.couchpotato.ui.moviesondemand.model.ResponseMovies
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResponseTVSeriesCategoryList(

	@field:SerializedName("category_name")
	val categoryName: String? = null,

	@field:SerializedName("category_id")
	val categoryId: String? = null,

	@field:SerializedName("parent_id")
	val parentId: Int? = null,

	@field:SerializedName("series")
	val series: ArrayList<ResponseTVSeries>? = ArrayList()
):Parcelable {

	companion object {

		/**
		 * Utils
		 */
		val mUtils= Utils.getUtils()

		var mInstance: ArrayList<ResponseTVSeriesCategoryList>? = null

		fun getResponseSERIESCategoryList() : ArrayList<ResponseTVSeriesCategoryList> {
			return if (mInstance != null) {
				mInstance as ArrayList<ResponseTVSeriesCategoryList>
			} else {
				if (mUtils.getString(Constants.KEY_SERIES_ON_DEMAND_CATEGORY_LIST).toString()=="null") {
					mInstance =
						ArrayList()
					mInstance!!
				} else {
					mInstance = Gson().fromJson<ArrayList<ResponseTVSeriesCategoryList>>(
						mUtils.getString(Constants.KEY_SERIES_ON_DEMAND_CATEGORY_LIST),
						ResponseTVSeriesCategoryList::class.java)
					return mInstance!!
				}
			}
		}

		fun saveSERIESCategoryList(mMoviesCategoryList: ArrayList<ResponseTVSeriesCategoryList>) {
			mInstance =mMoviesCategoryList
			mUtils.setString(Constants.KEY_SERIES_ON_DEMAND_CATEGORY_LIST, Gson().toJson(mMoviesCategoryList))
		}
	}

}
