package com.app.couchpotato.ui.season.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.couchpotato.R
import com.app.couchpotato.ui.moviesondemand.MoviesOnDemandActivity
import com.app.couchpotato.ui.season.SeasonActivity
import com.app.couchpotato.ui.season.model.SeasonsItem
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_season.view.*

class SeasonAdapter(
    private val mActivity: SeasonActivity,
    private var mSeriesList: ArrayList<SeasonsItem?>?
) : RecyclerView.Adapter<SeasonAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_season, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        Glide.with(mActivity)
            .load(mSeriesList?.get(position)?.cover)
            .centerCrop()
            .into(holder.itemView.ivCover)
        holder.itemView.tvSeason.text=mActivity.resources.getString(R.string.seasonno,mSeriesList?.get(position)?.seasonNumber.toString())
        holder.itemView.tvEpisode.text=mActivity.resources.getString(R.string.episodecount,mSeriesList?.get(position)?.episodeCount.toString())
    }

    override fun getItemCount(): Int = mSeriesList?.size!!

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view)
}