package com.app.couchpotato.ui.moviesondemand

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.couchpotato.R
import com.app.couchpotato.customview.recycleritemdecorator.LinearRecyclerItemDecorator
import com.app.couchpotato.databinding.ActivityMoviesOnDemandBinding
import com.app.couchpotato.ui.moviesondemand.adapter.MoviesOnDemandRecyclerAdapter
import com.app.couchpotato.ui.moviesondemand.model.ResponseMovieCategory
import com.app.couchpotato.ui.moviesondemand.model.ResponseMovies
import com.app.couchpotato.ui.moviesondemand.presenter.MoviesOnDemandEventHandler
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_movies_on_demand.*

class MoviesOnDemandActivity : AppCompatActivity() {

    /**
     * Binding
     */
    var mBinding: ActivityMoviesOnDemandBinding?=null

    /**
     * Get first new release movie to show on dashboard as banner
     */
    var mLatestMovie: ResponseMovies?=null

    /**
     * Linear Layout Manager
     */
    lateinit var mLinearLayoutManager: LinearLayoutManager

    /**
     * Adapter
     */
    lateinit var mAdapter: MoviesOnDemandRecyclerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding=DataBindingUtil.setContentView(
            this,
            R.layout.activity_movies_on_demand
        )
        mBinding?.mMoviesOnDemandEventHandler= MoviesOnDemandEventHandler(this)

        initUI()
    }

    private fun initUI() {
        Thread(Runnable {
            for (items in ResponseMovieCategory.getResponseMovieCategoryList()) {
                if (items.categoryName.equals("**New Release**")) {
                    mLatestMovie= items.movies?.get(0)!!
                }
            }
            this.runOnUiThread {
                tvLatestMovie.text=mLatestMovie?.name
                ratingBar.rating= mLatestMovie?.rating5based!!
                tvYearOfRelease.text= mBinding?.mMoviesOnDemandEventHandler?.mUtils?.getYear(
                    mLatestMovie?.added?.toLong()!!
                ).toString()
                Glide.with(this)
                    .load(mLatestMovie?.streamIcon)
                    .centerCrop()
                    .into(ivLatestMovie)
            }
        }).start()

        mLinearLayoutManager= LinearLayoutManager(this)
        recyclerviewMovies.layoutManager=mLinearLayoutManager
        recyclerviewMovies.addItemDecoration(
            LinearRecyclerItemDecorator(
                _top = resources.getDimension(R.dimen._1sdp).toInt(),
                _bottom = resources.getDimension(R.dimen._8sdp).toInt(),
                _start = resources.getDimension(R.dimen._1sdp).toInt(),
                _end = resources.getDimension(R.dimen._1sdp).toInt(),
                _extra = resources.getDimension(R.dimen._18sdp).toInt(),
                totalSize = 0,
                type = 0
            )
        )
        mAdapter= MoviesOnDemandRecyclerAdapter(this,ResponseMovieCategory.getResponseMovieCategoryList())
        recyclerviewMovies.adapter=mAdapter
    }
}
