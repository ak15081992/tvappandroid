package com.app.couchpotato.ui.demanddetail.model

import com.google.gson.annotations.SerializedName

data class ResponseVideoDetail(

	@field:SerializedName("movie_data")
	val movieData: MovieData? = null,

	@field:SerializedName("info")
	val info: Info? = null
) {
	data class MovieData(

		@field:SerializedName("category_id")
		val categoryId: String? = null,

		@field:SerializedName("stream_id")
		val streamId: Int? = null,

		@field:SerializedName("added")
		val added: String? = null,

		@field:SerializedName("name")
		val name: String? = null,

		@field:SerializedName("direct_source")
		val directSource: String? = null,

		@field:SerializedName("custom_sid")
		val customSid: String? = null,

		@field:SerializedName("container_extension")
		val containerExtension: String? = null
	)

	data class Info(

		@field:SerializedName("youtube_trailer")
		val youtubeTrailer: String? = null,

		@field:SerializedName("country")
		val country: String? = null,

		@field:SerializedName("o_name")
		val oName: String? = null,

		@field:SerializedName("movie_image")
		val movieImage: String? = null,

		@field:SerializedName("rating")
		val rating: String? = null,

		@field:SerializedName("description")
		val description: String? = null,

		@field:SerializedName("bitrate")
		val bitrate: Int? = null,

		@field:SerializedName("releasedate")
		val releasedate: String? = null,

		@field:SerializedName("video")
		val video: List<Any?>? = null,

		@field:SerializedName("backdrop_path")
		val backdropPath: List<String?>? = null,

		@field:SerializedName("duration")
		val duration: String? = null,

		@field:SerializedName("cast")
		val cast: String? = null,

		@field:SerializedName("plot")
		val plot: String? = null,

		@field:SerializedName("genre")
		val genre: String? = null,

		@field:SerializedName("audio")
		val audio: List<Any?>? = null,

		@field:SerializedName("mpaa_rating")
		val mpaaRating: String? = null,

		@field:SerializedName("kinopoisk_url")
		val kinopoiskUrl: String? = null,

		@field:SerializedName("tmdb_id")
		val tmdbId: String? = null,

		@field:SerializedName("director")
		val director: String? = null,

		@field:SerializedName("duration_secs")
		val durationSecs: Int? = null,

		@field:SerializedName("actors")
		val actors: String? = null,

		@field:SerializedName("name")
		val name: String? = null,

		@field:SerializedName("episode_run_time")
		val episodeRunTime: String? = null,

		@field:SerializedName("rating_count_kinopoisk")
		val ratingCountKinopoisk: Int? = null,

		@field:SerializedName("cover_big")
		val coverBig: String? = null,

		@field:SerializedName("age")
		val age: String? = null
	)
}
