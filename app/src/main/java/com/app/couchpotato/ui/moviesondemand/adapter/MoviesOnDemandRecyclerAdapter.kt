package com.app.couchpotato.ui.moviesondemand.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.couchpotato.R
import com.app.couchpotato.customview.recycleritemdecorator.LinearRecyclerItemDecorator
import com.app.couchpotato.helperutils.Constants
import com.app.couchpotato.ui.demanddetail.DemandDetailActivity
import com.app.couchpotato.ui.moviesondemand.MoviesOnDemandActivity
import com.app.couchpotato.ui.moviesondemand.model.ResponseMovieCategory
import com.app.couchpotato.ui.moviesondemand.model.ResponseMovies
import kotlinx.android.synthetic.main.item_on_demand.view.*

class MoviesOnDemandRecyclerAdapter(
    private val mActivity: MoviesOnDemandActivity,
    private var mMoviesList: ArrayList<ResponseMovieCategory>
) : RecyclerView.Adapter<MoviesOnDemandRecyclerAdapter.MyViewHolder>() {

    /**
     * Linear Layout Manager
     */
    lateinit var mLinearlayoutManager: LinearLayoutManager

    /**
     * Adapter
     */
    lateinit var mAdapter:MovieAdapter

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_on_demand, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.itemView.tvCategory.text= mMoviesList[position].categoryName
        mLinearlayoutManager= LinearLayoutManager(mActivity,LinearLayoutManager.HORIZONTAL,false)
        holder.itemView.recyclerviewMovie.layoutManager=mLinearlayoutManager
        holder.itemView.recyclerviewMovie.addItemDecoration(
            LinearRecyclerItemDecorator(
                mActivity.resources.getDimension(R.dimen._1sdp).toInt(),
                mActivity.resources.getDimension(R.dimen._1sdp).toInt(),
                mActivity.resources.getDimension(R.dimen._1sdp).toInt(),
                mActivity.resources.getDimension(R.dimen._14sdp).toInt(),
                mActivity.resources.getDimension(R.dimen._18sdp).toInt(),
                10,
                1
            )
        )
        mAdapter= MovieAdapter(mActivity,mMoviesList[position].movies,object: MovieAdapter.MovieClickListener{
            override fun onMovieClicked(movie: ResponseMovies?) {
                mActivity.startActivity(
                    Intent(mActivity,DemandDetailActivity::class.java)
                        .putExtra(Constants.KEY_VIDEO_ID,movie?.streamId.toString())
                        .putExtra(Constants.KEY_VIDEOINDEX,mMoviesList[position].movies?.indexOf(movie))
                )
            }
        })
        holder.itemView.recyclerviewMovie.adapter=mAdapter
    }

    override fun getItemCount(): Int = mMoviesList.size

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view)
}