package com.app.couchpotato.ui.splash

import android.content.Intent
import android.media.MediaPlayer.OnCompletionListener
import android.media.MediaPlayer.OnPreparedListener
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.app.couchpotato.R
import com.app.couchpotato.databinding.ActivitySplashBinding
import com.app.couchpotato.helperutils.Utils
import com.app.couchpotato.ui.login.LoginActivity
import com.app.couchpotato.ui.splash.presenter.SplashEventHandler
import kotlinx.android.synthetic.main.activity_splash.*

class SplashActivity : AppCompatActivity() {

    /**
     * Binding
     */
    var mBinding: ActivitySplashBinding?=null

    /**
     * Current Position
     */
    var mCurrentPosition=0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding=DataBindingUtil.setContentView(
            this,
            R.layout.activity_splash
        )
        mBinding?.mSplashEventHandler = SplashEventHandler(this)
    }

    override fun onStart() {
        super.onStart()
        initPlayer()
    }

    private fun initPlayer() {
        videoView.setVideoURI(Uri.parse("android.resource://" + packageName + "/" + R.raw.splash))
        videoView.setOnPreparedListener(
            OnPreparedListener {
                videoView.start()
            })

        videoView.setOnCompletionListener(
            OnCompletionListener {
                Utils.getUtils().fireActivityIntent(
                    this,
                    Intent(this,LoginActivity::class.java),
                    isFinish = true,
                    isForward = true
                )
            })
    }

    override fun onPause() {
        super.onPause()
        mCurrentPosition=videoView.currentPosition
        videoView.pause()
    }

    override fun onStop() {
        super.onStop()
        videoView.stopPlayback()
    }
}
