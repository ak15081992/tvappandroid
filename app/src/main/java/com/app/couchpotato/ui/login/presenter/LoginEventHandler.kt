package com.app.couchpotato.ui.login.presenter

import android.annotation.SuppressLint
import android.content.Intent
import android.view.View
import com.app.couchpotato.MyApp
import com.app.couchpotato.R
import com.app.couchpotato.commonmodel.*
import com.app.couchpotato.customview.loader.LoadingDialog
import com.app.couchpotato.helperutils.ChannelComparator
import com.app.couchpotato.helperutils.Constants
import com.app.couchpotato.helperutils.TopicComparator
import com.app.couchpotato.helperutils.Utils
import com.app.couchpotato.ui.dashboard.DashboardActivity
import com.app.couchpotato.ui.login.LoginActivity
import com.app.couchpotato.ui.moviesondemand.model.ResponseMovieCategory
import com.app.couchpotato.ui.moviesondemand.model.ResponseMovies
import com.tapadoo.alerter.Alerter
import kotlinx.android.synthetic.main.activity_login.*
import okhttp3.ResponseBody
import org.json.JSONArray
import org.json.JSONException
import org.w3c.dom.Element
import org.w3c.dom.Node
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayInputStream
import java.io.InputStream
import java.nio.charset.StandardCharsets
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import javax.xml.parsers.DocumentBuilder
import javax.xml.parsers.DocumentBuilderFactory
import kotlin.collections.ArrayList

class LoginEventHandler(private val mActivity: LoginActivity) {

    /**
     * Utils
     */
    val mUtils = Utils.getUtils()

    /**
     * Assinging LoadingDialog's singleton object into @mProgressLoader
     */
    private val mProgressLoader = LoadingDialog.getLoader()

    /**
     * AppComponent of Dagger
     */
    var mAppComponent = MyApp.get(mActivity).getAppComponents()

    init {
        if (UserModel.getUserModel().userInfo.toString() != "null") {
            mActivity.llMainContainer.visibility = View.GONE
            loginAPIImpl(
                Alerter.create(mActivity)
                ,UserModel.getUserModel().userInfo?.username.toString(),
                UserModel.getUserModel().userInfo?.password.toString()
            )
        }
    }

    fun onLoginClicked(view: View) {
        val mAlerter = Alerter.create(mActivity)
        if (isValidation(mAlerter)) {
            loginAPIImpl(
                mAlerter,
                mActivity.etUsername.text.toString(),
                mActivity.etPassword.text.toString()
            )
        }
    }

    /**
     * Login API Implementation to get basic details
     */
    private fun loginAPIImpl(mAlerter: Alerter, username: String, password: String) {
        if (mUtils.isOnline(mActivity)) {
            mProgressLoader.showLoader(mActivity)
            mAppComponent.getApiHelper().login(
                username = username,
                password = password
            ).enqueue(object : Callback<UserModel> {
                override fun onFailure(call: Call<UserModel>, t: Throwable) {
                    mProgressLoader.dismissLoader()
                    mUtils.showAlerter(
                        mAlerter,
                        R.color.colorRedError,
                        R.drawable.ic_error_white_24dp,
                        t.localizedMessage
                    )
                }

                override fun onResponse(
                    call: Call<UserModel>,
                    response: Response<UserModel>
                ) {
                    try {
                        if (response.code() == 200) {
                            if (response.body()?.userInfo?.auth.toString() == "0") {
                                mProgressLoader.dismissLoader()
                                mUtils.showAlerter(
                                    mAlerter,
                                    R.color.colorRedError,
                                    R.drawable.ic_error_white_24dp,
                                    mActivity.resources.getString(R.string.invalidusernamepassword)
                                )
                                return
                            }
                            if (response.body()?.userInfo?.status.toString() == "Expired") {
                                mProgressLoader.dismissLoader()
                                mUtils.showAlerter(
                                    mAlerter,
                                    R.color.colorRedError,
                                    R.drawable.ic_error_white_24dp,
                                    mActivity.resources.getString(R.string.expired)
                                )
                                return
                            }
                            UserModel.saveUserModel(response.body()!!)
                            /**
                             * Get Live Categories
                             */
                            getLiveCategoryAPIImpl(mAlerter, username, password)
                        } else {
                            mProgressLoader.dismissLoader()
                            mUtils.showAlerter(
                                mAlerter,
                                R.color.colorRedError,
                                R.drawable.ic_error_white_24dp,
                                response.message()
                            )
                        }
                    } catch (e: Exception) {
                        mProgressLoader.dismissLoader()
                        e.printStackTrace()
                        mUtils.showAlerter(
                            mAlerter,
                            R.color.colorRedError,
                            R.drawable.ic_error_white_24dp,
                            e.printStackTrace().toString()
                        )
                    }
                }
            })
        } else {
            mUtils.showAlerter(
                mAlerter,
                R.color.colorRedError,
                R.drawable.ic_error_white_24dp,
                mActivity.resources.getString(R.string.networkerror)
            )
        }
    }

    private fun getLiveCategoryAPIImpl(mAlerter: Alerter, username: String, password: String) {
        mAppComponent.getApiHelper().getLiveCategories(
            username = username,
            password = password
        ).enqueue(object : Callback<ArrayList<CategoryList>> {
            override fun onFailure(call: Call<ArrayList<CategoryList>>, t: Throwable) {
                mProgressLoader.dismissLoader()
                mUtils.showAlerter(
                    mAlerter,
                    R.color.colorRedError,
                    R.drawable.ic_error_white_24dp,
                    t.localizedMessage
                )
            }

            override fun onResponse(
                call: Call<ArrayList<CategoryList>>,
                response: Response<ArrayList<CategoryList>>
            ) {
                try {
                    if (response.code() == 200) {
                        CommonLists.mCategoryList = response.body()!!
                        getAllEPGAPIImpl()
                        //getChannelData(mAlerter)
                    } else {
                        mProgressLoader.dismissLoader()
                        mUtils.showAlerter(
                            mAlerter,
                            R.color.colorRedError,
                            R.drawable.ic_error_white_24dp,
                            response.message()
                        )
                    }
                } catch (e: Exception) {
                    mProgressLoader.dismissLoader()
                    e.printStackTrace()
                    mUtils.showAlerter(
                        mAlerter,
                        R.color.colorRedError,
                        R.drawable.ic_error_white_24dp,
                        e.printStackTrace().toString()
                    )
                }
            }

        })
    }

    fun getAllEPGAPIImpl() {
        val mAlerter = Alerter.create(mActivity)
        if (mUtils.isOnline(mActivity)) {
            mProgressLoader.setMessage(mActivity.resources.getString(R.string.loadingstreamdata))
            mAppComponent.getApiHelper().getAllEPGDate(
                username = UserModel.getUserModel().userInfo?.username.toString(),
                password = UserModel.getUserModel().userInfo?.password.toString()
            ).enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    mProgressLoader.dismissLoader()
                    mUtils.showAlerter(
                        mAlerter,
                        R.color.colorRedError,
                        R.drawable.ic_error_white_24dp,
                        t.localizedMessage
                    )
                }

                override fun onResponse(
                    call: Call<ResponseBody>,
                    response: Response<ResponseBody>
                ) {
                    if (response.code() == 200)
                    {
                        val resultStr = response.body()?.string()
                        Thread(Runnable {
                            val dbFactory =
                                DocumentBuilderFactory.newInstance()
                            val dBuilder: DocumentBuilder
                            try {
                                dBuilder = dbFactory.newDocumentBuilder()
                                val `is`: InputStream
                                `is` =
                                    ByteArrayInputStream(resultStr!!.toByteArray(StandardCharsets.UTF_8))
                                val doc = dBuilder.parse(`is`)
                                val element = doc.documentElement
                                element.normalize()
                                var nList =
                                    doc.getElementsByTagName("channel")
                                for (i in 0 until nList.length) {
                                    val node = nList.item(i)
                                    if (node.nodeType == Node.ELEMENT_NODE) {
                                        val element2 =
                                            node as Element
                                        val channelItem = ChannelItem()
                                        channelItem.m_sEPGChannelID = element2.getAttribute("id")
                                        if (!element2.hasChildNodes()) continue
                                        var childList =
                                            element2.getElementsByTagName("display-name")
                                        if (childList != null && childList.length > 0) channelItem.m_sTvName =
                                            childList.item(0).textContent
                                        childList = element2.getElementsByTagName("icon")
                                        if (childList != null && childList.length > 0) {
                                            channelItem.m_sStreamIcon =
                                                (element2.getElementsByTagName("icon")
                                                    .item(0) as Element).getAttribute("src")
                                        }
                                        CommonLists.EPG_MAP.put(
                                            channelItem.m_sEPGChannelID!!,
                                            channelItem
                                        )
                                    }
                                }
                                nList = doc.getElementsByTagName("programme")
                                for (i in 0 until nList.length) {
                                    val node = nList.item(i)
                                    if (node.nodeType == Node.ELEMENT_NODE) {
                                        val element2 =
                                            node as Element
                                        val itemTopic = ItemTopic()
                                        itemTopic.m_sChannelID = element2.getAttribute("channel")
                                        itemTopic.m_sTitle =
                                            element2.getElementsByTagName("title").item(0)
                                                .textContent
                                        itemTopic.m_sDescription =
                                            element2.getElementsByTagName("desc").item(0)
                                                .textContent
                                        @SuppressLint("SimpleDateFormat") val format =
                                            SimpleDateFormat("yyyyMMddHHmmss Z")
                                        try {
                                            itemTopic.m_dateTopicStart = format.parse(
                                                mUtils.UTCStringToLocalString(
                                                    element2.getAttribute(
                                                        "start"
                                                    )
                                                ).toString()
                                            )
                                            itemTopic.m_dateTopicEnd = format.parse(
                                                mUtils.UTCStringToLocalString(
                                                    element2.getAttribute(
                                                        "stop"
                                                    )
                                                ).toString()
                                            )
                                        } catch (e: ParseException) {
                                            e.printStackTrace()
                                        }
                                        val channelItem: ChannelItem? =
                                            CommonLists.EPG_MAP[itemTopic.m_sChannelID]
                                        if (channelItem != null && !channelItem.m_arrItemTopic.hasSameTimeRange(
                                                itemTopic
                                            ) && !itemTopic.m_dateTopicEnd.equals(itemTopic.m_dateTopicStart) && itemTopic.m_dateTopicEnd.after(
                                                itemTopic.m_dateTopicStart
                                            )
                                        ) {
                                            channelItem.m_arrItemTopic.add(itemTopic)
                                        }
                                    }
                                }
                                mActivity.runOnUiThread(
                                    Runnable {
                                        getChannelData(mAlerter)
                                    })
                            } catch (e: Exception) {
                                mActivity.runOnUiThread(Runnable {
                                    mProgressLoader.dismissLoader()
                                    mUtils.showAlerter(
                                        mAlerter,
                                        R.color.colorRedError,
                                        R.drawable.ic_error_white_24dp,
                                        e.printStackTrace().toString()
                                    )
                                })
                                e.printStackTrace()
                            }
                        }).start()
                    } else {
                        mProgressLoader.dismissLoader()
                        mUtils.showAlerter(
                            mAlerter,
                            R.color.colorRedError,
                            R.drawable.ic_error_white_24dp,
                            response.message()
                        )
                    }
                }
            })
        } else {
            mUtils.showAlerter(
                mAlerter,
                R.color.colorRedError,
                R.drawable.ic_error_white_24dp,
                mActivity.resources.getString(R.string.networkerror)
            )
        }
    }

    private fun getChannelData(
        mAlerter: Alerter
    ) {
        mProgressLoader.setMessage(mActivity.resources.getString(R.string.loadingchanneldata))
        mAppComponent.getApiHelper().getChannelData(
            username = UserModel.getUserModel().userInfo?.username.toString(),
            password = UserModel.getUserModel().userInfo?.password.toString()
        ).enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                mProgressLoader.dismissLoader()
                mUtils.showAlerter(
                    mAlerter,
                    R.color.colorRedError,
                    R.drawable.ic_error_white_24dp,
                    mActivity.resources.getString(R.string.somethingwentwrong)
                )
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.code() == 200) {
                    val resultStr = response.body()?.string()
                    Thread(Runnable {
                        try {
                            val jsonArray = JSONArray(resultStr)
                            for (i in 0 until jsonArray.length()) {
                                val jsonObject = jsonArray.getJSONObject(i)
                                var epgChannelID =
                                    if (jsonObject.has(Constants.ITEM_EPG_CHANNEL_ID)) jsonObject.getString(
                                        Constants.ITEM_EPG_CHANNEL_ID
                                    ) else ""
                                if (epgChannelID == null || epgChannelID.isEmpty() || epgChannelID.equals(
                                        "null",
                                        ignoreCase = true
                                    )
                                ) {
                                    epgChannelID = ""
                                }
                                var channelItem: ChannelItem
                                channelItem = (if (CommonLists.EPG_MAP.get(epgChannelID) == null) {
                                    ChannelItem()
                                } else if (CommonLists.EPG_MAP.get(epgChannelID) != null &&
                                    CommonLists.EPG_MAP.get(epgChannelID)?.m_sTvName != null
                                ) {
                                    ChannelItem(
                                        CommonLists.EPG_MAP.get(
                                            epgChannelID
                                        )!!
                                    )
                                } else {
                                    CommonLists.EPG_MAP.get(epgChannelID)
                                })!!
                                CommonLists.EPGDATA.add(channelItem)
                                assert(channelItem != null)
                                channelItem.m_sTvName =
                                    if (jsonObject.has(Constants.ITEM_NAME)) jsonObject.getString(
                                        Constants.ITEM_NAME
                                    ) else ""
                                channelItem.m_sEPGChannelID =
                                    if (jsonObject.has(Constants.ITEM_EPG_CHANNEL_ID)) jsonObject.getString(
                                        Constants.ITEM_EPG_CHANNEL_ID
                                    ) else ""
                                channelItem.m_sTvNum =
                                    if (jsonObject.has(Constants.ITEM_STREAM_NUM)) jsonObject.getString(
                                        Constants.ITEM_STREAM_NUM
                                    ) else ""
                                channelItem.m_sStreamType =
                                    if (jsonObject.has(Constants.ITEM_STREAM_TYPE)) jsonObject.getString(
                                        Constants.ITEM_STREAM_TYPE
                                    ) else ""
                                channelItem.m_sStreamID =
                                    if (jsonObject.has(Constants.ITEM_STREAM_ID)) jsonObject.getString(
                                        Constants.ITEM_STREAM_ID
                                    ) else ""
                                channelItem.m_sStreamIcon =
                                    if (jsonObject.has(Constants.ITEM_STREAM_ICON)) jsonObject.getString(
                                        Constants.ITEM_STREAM_ICON
                                    ) else ""
                                channelItem.m_sAdded =
                                    if (jsonObject.has(Constants.ITEM_ADDED)) jsonObject.getString(
                                        Constants.ITEM_ADDED
                                    ) else ""
                                channelItem.m_sCategory_ID =
                                    if (jsonObject.has(Constants.ITEM_CATEGORY_ID)) jsonObject.getString(
                                        Constants.ITEM_CATEGORY_ID
                                    ) else ""
                                channelItem.m_sCustomSID =
                                    if (jsonObject.has(Constants.ITEM_CUSTOM_SID)) jsonObject.getString(
                                        Constants.ITEM_CUSTOM_SID
                                    ) else ""
                                channelItem.m_sTVArchive =
                                    if (jsonObject.has(Constants.ITEM_TV_ARCHIVE)) jsonObject.getString(
                                        Constants.ITEM_TV_ARCHIVE
                                    ) else ""
                                channelItem.m_sDirectSource =
                                    if (jsonObject.has(Constants.ITEM_DIRECT_SOURCE)) jsonObject.getString(
                                        Constants.ITEM_DIRECT_SOURCE
                                    ) else ""
                                channelItem.m_sTVArchiveDuration =
                                    if (jsonObject.has(Constants.ITEM_TV_ARCHIVE_DURATION)) jsonObject.getString(
                                        Constants.ITEM_TV_ARCHIVE_DURATION
                                    ) else ""
                                if (channelItem.m_sTVArchive.equals("1")) CommonLists.EPG_CATCH_UP_DATA.add(
                                    channelItem
                                )
                            }
                            var bIsContains: Boolean
                            var i = 0
                            while (i < CommonLists.EPGDATA.size) {
                                bIsContains = false
                                if (CommonLists.EPGDATA.get(i)?.m_sCategory_ID.equals(CommonLists.MLB_CATEGORY_ID)) {
                                    CommonLists.MLBCHANNEL.add(CommonLists.EPGDATA.get(i))
                                    bIsContains = true
                                } else if (CommonLists.EPGDATA.get(i)?.m_sCategory_ID.equals(
                                        CommonLists.EPL_CATEGORY_ID
                                    )
                                ) {
                                    CommonLists.EPLCHANNEL.add(CommonLists.EPGDATA.get(i))
                                    bIsContains = true
                                } else if (CommonLists.EPGDATA.get(i)?.m_sCategory_ID.equals(
                                        CommonLists.NBA_CATEGORY_ID
                                    )
                                ) {
                                    CommonLists.NBACHANNEL.add(CommonLists.EPGDATA.get(i))
                                    bIsContains = true
                                } else if (CommonLists.EPGDATA.get(i)?.m_sCategory_ID.equals(
                                        CommonLists.NFL_CATEGORY_ID
                                    )
                                ) {
                                    CommonLists.NFLCHANNEL.add(CommonLists.EPGDATA.get(i))
                                    bIsContains = true
                                } else if (CommonLists.EPGDATA.get(i)?.m_sCategory_ID.equals(
                                        CommonLists.NHL_CATEGORY_ID
                                    )
                                ) {
                                    CommonLists.NHLCHANNEL.add(CommonLists.EPGDATA.get(i))
                                    bIsContains = true
                                } else if (CommonLists.EPGDATA.get(i)?.m_sCategory_ID.equals(
                                        CommonLists.PPV_CATEGORY_ID
                                    )
                                ) {
                                    CommonLists.PPVCHANNEL.add(CommonLists.EPGDATA.get(i))
                                    bIsContains = true
                                }
                                if (bIsContains) {
                                    if (CommonLists.EPGDATA.get(i)?.m_arrItemTopic?.size != 0) {
                                        Collections.sort<ItemTopic>(
                                            CommonLists.EPGDATA.get(
                                                i
                                            )?.m_arrItemTopic!!,
                                            TopicComparator()
                                        )
                                        val arrayItemTopic: ArrayItemTopic =
                                            CommonLists.EPGDATA.get(i)?.m_arrItemTopic!!
                                        var index = 1
                                        while (index < arrayItemTopic.size) {
                                            if (!arrayItemTopic.get(index - 1)?.m_dateTopicEnd?.equals(
                                                    arrayItemTopic.get(index)?.m_dateTopicStart
                                                )!! && arrayItemTopic.get(index - 1)?.m_dateTopicEnd?.before(
                                                    arrayItemTopic.get(index)?.m_dateTopicStart
                                                )!!
                                            ) {
                                                val itemTopic = ItemTopic()
                                                itemTopic.m_sTitle = "No Program"
                                                itemTopic.m_dateTopicStart =
                                                    Date(arrayItemTopic.get(index - 1)?.m_dateTopicEnd?.getTime()!!)
                                                itemTopic.m_dateTopicEnd =
                                                    Date(arrayItemTopic.get(index)?.m_dateTopicStart?.getTime()!!)
                                                itemTopic.m_sChannelNumber =
                                                    CommonLists.EPGDATA.get(i)?.m_sTvNum
                                                arrayItemTopic.add(index, itemTopic)
                                                index++
                                            }
                                            index++
                                        }
                                        val startDate = Date(
                                            Calendar.getInstance().time.time - 3 * 24 * 60 * 60 * 1000
                                        )
                                        if (arrayItemTopic.get(0)?.m_dateTopicStart?.after(startDate)!!) {
                                            val itemTopic = ItemTopic()
                                            itemTopic.m_sTitle = "No Program"
                                            itemTopic.m_dateTopicStart =
                                                Date(startDate.time)
                                            itemTopic.m_dateTopicEnd =
                                                Date(arrayItemTopic.get(0)!!.m_dateTopicStart.getTime())
                                            itemTopic.m_sChannelNumber =
                                                CommonLists.EPGDATA.get(i)?.m_sTvNum
                                            arrayItemTopic.add(0, itemTopic)
                                        }
                                        val endDate = Date(
                                            Calendar.getInstance().time.time + 3 * 24 * 60 * 60 * 1000
                                        )
                                        if (arrayItemTopic.get(arrayItemTopic.size - 1)?.m_dateTopicEnd?.before(
                                                endDate
                                            )!!
                                        ) {
                                            val itemTopic = ItemTopic()
                                            itemTopic.m_sTitle = "No Program"
                                            itemTopic.m_dateTopicStart =
                                                Date(arrayItemTopic.get(arrayItemTopic.size - 1)?.m_dateTopicEnd!!.getTime())
                                            itemTopic.m_dateTopicEnd =
                                                Date(endDate.time)
                                            itemTopic.m_sChannelNumber =
                                                CommonLists.EPGDATA.get(i)?.m_sTvNum
                                            arrayItemTopic.add(itemTopic)
                                        }
                                    }
                                    CommonLists.EPGDATA.removeAt(i)
                                    i--
                                } else {
                                    if (CommonLists.EPGDATA.get(i)?.m_arrItemTopic?.size == 0) {
                                        val itemTopic = ItemTopic()
                                        itemTopic.m_sTitle = "No Program"
                                        itemTopic.m_dateTopicStart = null
                                        itemTopic.m_dateTopicEnd = null
                                        itemTopic.m_sChannelNumber =
                                            CommonLists.EPGDATA.get(i)?.m_sTvNum
                                        CommonLists.EPGDATA.get(i)?.m_arrItemTopic!!.add(itemTopic)
                                    } else {
                                        Collections.sort<ItemTopic>(
                                            CommonLists.EPGDATA.get(
                                                i
                                            )?.m_arrItemTopic!!,
                                            TopicComparator()
                                        )
                                        val arrayItemTopic: ArrayItemTopic =
                                            CommonLists.EPGDATA.get(i)?.m_arrItemTopic!!
                                        var index = 1
                                        while (index < arrayItemTopic.size) {
                                            if (!arrayItemTopic.get(index - 1)?.m_dateTopicEnd?.equals(
                                                    arrayItemTopic.get(index)?.m_dateTopicStart
                                                )!! && arrayItemTopic.get(index - 1)?.m_dateTopicEnd?.before(
                                                    arrayItemTopic.get(index)?.m_dateTopicStart
                                                )!!
                                            ) {
                                                val itemTopic = ItemTopic()
                                                itemTopic.m_sTitle = "No Program"
                                                itemTopic.m_dateTopicStart =
                                                    Date(arrayItemTopic.get(index - 1)!!.m_dateTopicEnd.getTime())
                                                itemTopic.m_dateTopicEnd =
                                                    Date(arrayItemTopic.get(index)!!.m_dateTopicStart.getTime())
                                                itemTopic.m_sChannelNumber =
                                                    CommonLists.EPGDATA.get(i)!!.m_sTvNum
                                                arrayItemTopic.add(index, itemTopic)
                                                index++
                                            }
                                            index++
                                        }
                                        val startDate = Date(
                                            Calendar.getInstance().time.time - 3 * 24 * 60 * 60 * 1000
                                        )
                                        if (arrayItemTopic.get(0)!!.m_dateTopicStart != null && arrayItemTopic.get(
                                                0
                                            )!!.m_dateTopicStart.after(startDate)
                                        ) {
                                            val itemTopic = ItemTopic()
                                            itemTopic.m_sTitle = "No Program"
                                            itemTopic.m_dateTopicStart =
                                                Date(startDate.time)
                                            itemTopic.m_dateTopicEnd =
                                                Date(arrayItemTopic.get(0)!!.m_dateTopicStart.getTime())
                                            itemTopic.m_sChannelNumber =
                                                CommonLists.EPGDATA.get(i)!!.m_sTvNum
                                            arrayItemTopic.add(0, itemTopic)
                                        }
                                        val endDate = Date(
                                            Calendar.getInstance().time.time + 3 * 24 * 60 * 60 * 1000
                                        )
                                        if (arrayItemTopic.get(0)!!.m_dateTopicEnd != null && arrayItemTopic.get(
                                                arrayItemTopic.size - 1
                                            )!!.m_dateTopicEnd.before(endDate)
                                        ) {
                                            val itemTopic = ItemTopic()
                                            itemTopic.m_sTitle = "No Program"
                                            itemTopic.m_dateTopicStart =
                                                Date(arrayItemTopic.get(arrayItemTopic.size - 1)!!.m_dateTopicEnd.getTime())
                                            itemTopic.m_dateTopicEnd =
                                                Date(endDate.time)
                                            itemTopic.m_sChannelNumber =
                                                CommonLists.EPGDATA.get(i)!!.m_sTvNum
                                            arrayItemTopic.add(itemTopic)
                                        }
                                    }
                                }
                                i++
                            }
                            bIsContains = false
                            for (groupItem in CommonLists.MLBCHANNEL) {
                                if (!bIsContains && CommonLists.EPGDATA.contains(groupItem)) bIsContains =
                                    true else if (bIsContains) CommonLists.EPGDATA.remove(groupItem)
                            }
                            if (!bIsContains && CommonLists.MLBCHANNEL.size > 0) {
                                CommonLists.MLBCHANNEL.get(0)!!.m_arrItemTopic.clear()
                                if (CommonLists.MLBCHANNEL.get(0)!!.m_arrItemTopic.size == 0) {
                                    val itemTopic = ItemTopic()
                                    itemTopic.m_sTitle = "MLB CHANNELS"
                                    itemTopic.m_dateTopicStart = null
                                    itemTopic.m_dateTopicEnd = null
                                    itemTopic.m_sChannelNumber =
                                        CommonLists.MLBCHANNEL.get(0)!!.m_sTvNum
                                    CommonLists.MLBCHANNEL.get(0)!!.m_arrItemTopic.add(itemTopic)
                                }
                            }
                            bIsContains = false
                            for (groupItem in CommonLists.NBACHANNEL) {
                                if (!bIsContains && CommonLists.EPGDATA.contains(groupItem)) bIsContains =
                                    true else if (bIsContains) CommonLists.EPGDATA.remove(groupItem)
                            }
                            if (!bIsContains && CommonLists.NBACHANNEL.size > 0) {
                                CommonLists.NBACHANNEL.get(0)!!.m_arrItemTopic.clear()
                                if (CommonLists.NBACHANNEL.get(0)!!.m_arrItemTopic.size == 0) {
                                    val itemTopic = ItemTopic()
                                    itemTopic.m_sTitle = "NBA CHANNELS"
                                    itemTopic.m_dateTopicStart = null
                                    itemTopic.m_dateTopicEnd = null
                                    itemTopic.m_sChannelNumber =
                                        CommonLists.NBACHANNEL.get(0)!!.m_sTvNum
                                    CommonLists.NBACHANNEL.get(0)!!.m_arrItemTopic.add(itemTopic)
                                }
                            }
                            bIsContains = false
                            for (groupItem in CommonLists.NFLCHANNEL) {
                                if (!bIsContains && CommonLists.EPGDATA.contains(groupItem)) bIsContains =
                                    true else if (bIsContains) CommonLists.EPGDATA.remove(groupItem)
                            }
                            if (!bIsContains && CommonLists.NFLCHANNEL.size > 0) {
                                CommonLists.NFLCHANNEL.get(0)!!.m_arrItemTopic.clear()
                                if (CommonLists.NFLCHANNEL.get(0)!!.m_arrItemTopic.size == 0) {
                                    val itemTopic = ItemTopic()
                                    itemTopic.m_sTitle = "NFL CHANNELS"
                                    itemTopic.m_dateTopicStart = null
                                    itemTopic.m_dateTopicEnd = null
                                    itemTopic.m_sChannelNumber =
                                        CommonLists.NFLCHANNEL.get(0)!!.m_sTvNum
                                    CommonLists.NFLCHANNEL.get(0)!!.m_arrItemTopic.add(itemTopic)
                                }
                            }
                            bIsContains = false
                            for (groupItem in CommonLists.NHLCHANNEL) {
                                if (!bIsContains && CommonLists.EPGDATA.contains(groupItem)) bIsContains =
                                    true else if (bIsContains) CommonLists.EPGDATA.remove(groupItem)
                            }
                            if (!bIsContains && CommonLists.NHLCHANNEL.size > 0) {
                                CommonLists.NHLCHANNEL.get(0)!!.m_arrItemTopic.clear()
                                if (CommonLists.NHLCHANNEL.get(0)!!.m_arrItemTopic.size == 0) {
                                    val itemTopic = ItemTopic()
                                    itemTopic.m_sTitle = "NHL CHANNELS"
                                    itemTopic.m_dateTopicStart = null
                                    itemTopic.m_dateTopicEnd = null
                                    itemTopic.m_sChannelNumber =
                                        CommonLists.NHLCHANNEL.get(0)!!.m_sTvNum
                                    CommonLists.NHLCHANNEL.get(0)!!.m_arrItemTopic.add(itemTopic)
                                }
                            }
                            bIsContains = false
                            for (groupItem in CommonLists.EPLCHANNEL) {
                                if (!bIsContains && CommonLists.EPGDATA.contains(groupItem)) bIsContains =
                                    true else if (bIsContains) CommonLists.EPGDATA.remove(groupItem)
                            }
                            if (!bIsContains && CommonLists.EPLCHANNEL.size > 0) {
                                CommonLists.EPLCHANNEL.get(0)!!.m_arrItemTopic.clear()
                                if (CommonLists.EPLCHANNEL.get(0)!!.m_arrItemTopic.size == 0) {
                                    val itemTopic = ItemTopic()
                                    itemTopic.m_sTitle = "EPL CHANNELS"
                                    itemTopic.m_dateTopicStart = null
                                    itemTopic.m_dateTopicEnd = null
                                    itemTopic.m_sChannelNumber =
                                        CommonLists.EPLCHANNEL.get(0)!!.m_sTvNum
                                    CommonLists.EPLCHANNEL.get(0)!!.m_arrItemTopic.add(itemTopic)
                                }
                            }
                            bIsContains = false
                            for (groupItem in CommonLists.PPVCHANNEL) {
                                if (!bIsContains && CommonLists.EPGDATA.contains(groupItem)) bIsContains =
                                    true else if (bIsContains) CommonLists.EPGDATA.remove(groupItem)
                            }
                            if (!bIsContains && CommonLists.PPVCHANNEL.size > 0) {
                                CommonLists.PPVCHANNEL.get(0)!!.m_arrItemTopic.clear()
                                if (CommonLists.PPVCHANNEL.get(0)!!.m_arrItemTopic.size == 0) {
                                    val itemTopic = ItemTopic()
                                    itemTopic.m_sTitle = "PPV CHANNELS"
                                    itemTopic.m_dateTopicStart = null
                                    itemTopic.m_dateTopicEnd = null
                                    itemTopic.m_sChannelNumber =
                                        CommonLists.PPVCHANNEL.get(0)!!.m_sTvNum
                                    CommonLists.PPVCHANNEL.get(0)!!.m_arrItemTopic.add(itemTopic)
                                }
                            }
                            Collections.sort<ChannelItem>(
                                CommonLists.EPGDATA,
                                ChannelComparator()
                            )
                            if (CommonLists.MLBCHANNEL.size > 0) CommonLists.EPGDATA.add(
                                0,
                                CommonLists.MLBCHANNEL.get(0)
                            )
                            if (CommonLists.NBACHANNEL.size > 0) CommonLists.EPGDATA.add(
                                0,
                                CommonLists.NBACHANNEL.get(0)
                            )
                            if (CommonLists.NFLCHANNEL.size > 0) CommonLists.EPGDATA.add(
                                0,
                                CommonLists.NFLCHANNEL.get(0)
                            )
                            if (CommonLists.NHLCHANNEL.size > 0) CommonLists.EPGDATA.add(
                                0,
                                CommonLists.NHLCHANNEL.get(0)
                            )
                            if (CommonLists.EPLCHANNEL.size > 0) CommonLists.EPGDATA.add(
                                0,
                                CommonLists.EPLCHANNEL.get(0)
                            )
                            if (CommonLists.PPVCHANNEL.size > 0) CommonLists.EPGDATA.add(
                                0,
                                CommonLists.PPVCHANNEL.get(0)
                            )
                            mActivity.runOnUiThread(Runnable {
                                getMoviesOnDemandCategoryList(mAlerter)
                            })
                        } catch (e: JSONException) {
                            mActivity.runOnUiThread {
                                mProgressLoader.dismissLoader()
                                mUtils.showAlerter(
                                    mAlerter,
                                    R.color.colorRedError,
                                    R.drawable.ic_error_white_24dp,
                                    mActivity.resources.getString(R.string.somethingwentwrong)
                                )
                            }
                        }
                    }).start()
                } else {
                    mProgressLoader.dismissLoader()
                    mUtils.showAlerter(
                        mAlerter,
                        R.color.colorRedError,
                        R.drawable.ic_error_white_24dp,
                        response.message()
                    )
                }
            }

        })
    }

    /**
     * Get List of Movies on Demand
     */
    private fun getMoviesOnDemandCategoryList(mAlerter: Alerter) {
        mProgressLoader.setMessage(mActivity.resources.getString(R.string.loadingmoviesdata))
        mAppComponent.getApiHelper().getMoviesOnDemandCategoryList(
            username = UserModel.getUserModel().userInfo?.username.toString(),
            password = UserModel.getUserModel().userInfo?.password.toString()
        ).enqueue(object : Callback<ArrayList<ResponseMovieCategory>> {
            override fun onFailure(call: Call<ArrayList<ResponseMovieCategory>>, t: Throwable) {
                mProgressLoader.dismissLoader()
                mUtils.showAlerter(
                    mAlerter,
                    R.color.colorRedError,
                    R.drawable.ic_error_white_24dp,
                    mActivity.resources.getString(R.string.somethingwentwrong)
                )
            }

            override fun onResponse(
                call: Call<ArrayList<ResponseMovieCategory>>,
                response: Response<ArrayList<ResponseMovieCategory>>
            ) {
                if (response.code() == 200) {
                    try {
                        ResponseMovieCategory.saveMoveCategoryList(response.body()!!)
                        getMoviesList(mAlerter)
                    } catch (e: java.lang.Exception) {
                        mProgressLoader.dismissLoader()
                        e.printStackTrace()
                        mUtils.showAlerter(
                            mAlerter,
                            R.color.colorRedError,
                            R.drawable.ic_error_white_24dp,
                            mActivity.resources.getString(R.string.somethingwentwrong)
                        )
                    }
                } else {
                    mProgressLoader.dismissLoader()
                    mUtils.showAlerter(
                        mAlerter,
                        R.color.colorRedError,
                        R.drawable.ic_error_white_24dp,
                        response.message()
                    )
                }
            }

        })
    }

    private fun getMoviesList(mAlerter: Alerter) {
        mAppComponent.getApiHelper().getMoviesList(
            username = UserModel.getUserModel().userInfo?.username.toString(),
            password = UserModel.getUserModel().userInfo?.password.toString()
        ).enqueue(object : Callback<ArrayList<ResponseMovies>> {
            override fun onFailure(call: Call<ArrayList<ResponseMovies>>, t: Throwable) {
                mProgressLoader.dismissLoader()
                mUtils.showAlerter(
                    mAlerter,
                    R.color.colorRedError,
                    R.drawable.ic_error_white_24dp,
                    mActivity.resources.getString(R.string.somethingwentwrong)
                )
            }

            override fun onResponse(
                call: Call<ArrayList<ResponseMovies>>,
                response: Response<ArrayList<ResponseMovies>>
            ) {
                if (response.code() == 200) {
                    try {
                        Thread(Runnable {
                            ResponseMovies.saveMoveCategoryList(response.body()!!)
                            for (movieCategory in ResponseMovieCategory.getResponseMovieCategoryList()) {
                                for (items in response.body()!!) {
                                    if (items.categoryId == movieCategory.categoryId) {
                                        movieCategory.movies?.add(items)
                                    }
                                }
                            }
                            mActivity.runOnUiThread {
                                mProgressLoader.dismissLoader()
                                mUtils.fireActivityIntent(
                                    mActivity,
                                    Intent(mActivity, DashboardActivity::class.java),
                                    isFinish = false,
                                    isForward = true
                                )
                            }
                        }).start()
                    } catch (e: java.lang.Exception) {
                        mProgressLoader.dismissLoader()
                        e.printStackTrace()
                        mUtils.showAlerter(
                            mAlerter,
                            R.color.colorRedError,
                            R.drawable.ic_error_white_24dp,
                            mActivity.resources.getString(R.string.somethingwentwrong)
                        )
                    }
                } else {
                    mProgressLoader.dismissLoader()
                    mUtils.showAlerter(
                        mAlerter,
                        R.color.colorRedError,
                        R.drawable.ic_error_white_24dp,
                        response.message()
                    )
                }
            }

        })
    }

    private fun isValidation(mAlerter: Alerter): Boolean {
        if (mActivity.etUsername.text.toString().trim().isEmpty()) {
            mUtils.showAlerter(
                mAlerter,
                R.color.colorRedError,
                R.drawable.ic_error_white_24dp,
                mActivity.resources.getString(R.string.error_emptyusername)
            )
            mActivity.etUsername.requestFocus()
            return false
        }
        if (mActivity.etPassword.text.toString().trim().isEmpty()) {
            mUtils.showAlerter(
                mAlerter,
                R.color.colorRedError,
                R.drawable.ic_error_white_24dp,
                mActivity.resources.getString(R.string.error_emptypassword)
            )
            mActivity.etPassword.requestFocus()
            return false
        }
        return true
    }
}