package com.app.couchpotato.ui.episode.model;

import android.widget.ImageView;

public class SeasonItem{
    public ImageView itrans;
    public String air_date;
    public String episode_count;
    public String id;
    public String name;
    public String overview;
    public String season_number;
    public String cover;
    public String cover_big;
    public SeasonItem(){

    }
}
