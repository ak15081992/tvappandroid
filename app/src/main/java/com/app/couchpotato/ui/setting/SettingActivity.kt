package com.app.couchpotato.ui.setting

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.app.couchpotato.R
import com.app.couchpotato.commonmodel.UserModel
import com.app.couchpotato.databinding.ActivitySettingBinding
import com.app.couchpotato.helperutils.Constants
import com.app.couchpotato.ui.setting.presenter.SettingEventHandler
import kotlinx.android.synthetic.main.activity_setting.*
import java.lang.Boolean

class SettingActivity : AppCompatActivity() {

    /**
     * Binding
     */
    var mBinding: ActivitySettingBinding?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding=DataBindingUtil.setContentView(
            this,
            R.layout.activity_setting
        )
        mBinding?.mSettingEventHandler= SettingEventHandler(this)
        mBinding?.mUserModel= UserModel.getUserModel()

        initUI()
    }

    private fun initUI() {
        swHardwareAccleration.isChecked= (if (mBinding?.mSettingEventHandler?.mUtils?.getString(Constants.HARDWARE_ACCELERATION)=="null") {
            false
        } else {
            mBinding?.mSettingEventHandler?.mUtils?.getString(Constants.HARDWARE_ACCELERATION)?.toBoolean()
        })!!
        swParentalLock.isChecked=if (
            mBinding?.mSettingEventHandler?.mUtils?.getString(Constants.PARENTAL_LOCK)!="null") {
            mBinding?.mSettingEventHandler?.mUtils?.getString(Constants.PARENTAL_LOCK)?.toBoolean()!!
        } else {
            false
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        mBinding?.mSettingEventHandler?.goBack()
    }

    fun showPassDialog() {
        val li = LayoutInflater.from(this)
        @SuppressLint("InflateParams") val promptsView: View =
            li.inflate(R.layout.dialog_password, null)
        val alertDialogBuilder = AlertDialog.Builder(
            this
        )
        alertDialogBuilder.setView(promptsView)
        val userInput = promptsView
            .findViewById<EditText>(R.id.etPassword)
        alertDialogBuilder
            .setCancelable(false)
            .setPositiveButton(
                "OK"
            ) { dialog: DialogInterface?, id: Int ->
                var flag = false
                if (UserModel.getUserModel().userInfo?.username.toString() != "null") {
                    if (userInput.text.toString() == UserModel.getUserModel().userInfo?.password) flag = true
                }
                if (!flag) {
                    Toast.makeText(
                        baseContext,
                        "Wrong Password, Input again",
                        Toast.LENGTH_SHORT
                    ).show()
                    return@setPositiveButton
                }
                flag = !Boolean.valueOf(
                    if (mBinding?.mSettingEventHandler?.mUtils?.getString(Constants.PARENTAL_LOCK)!="null") {
                        mBinding?.mSettingEventHandler?.mUtils?.getString(Constants.PARENTAL_LOCK)
                    } else {
                        "false"
                    }
                )
                mBinding?.mSettingEventHandler?.mUtils?.setString(
                    Constants.PARENTAL_LOCK,
                    if (flag) "true" else "false"
                )
                swParentalLock.isChecked=
                    mBinding?.mSettingEventHandler?.mUtils?.getString(Constants.PARENTAL_LOCK)?.toBoolean()!!
            }
            .setNegativeButton(
                "Cancel"
            ) { dialog: DialogInterface, id: Int -> dialog.cancel() }

        // create alert dialog
        val alertDialog = alertDialogBuilder.create()
        // show it
        alertDialog.show()
    }
}
