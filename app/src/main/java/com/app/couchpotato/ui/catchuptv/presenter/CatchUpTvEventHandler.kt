package com.app.couchpotato.ui.catchuptv.presenter

import com.app.couchpotato.MyApp
import com.app.couchpotato.customview.loader.LoadingDialog
import com.app.couchpotato.helperutils.Utils
import com.app.couchpotato.ui.catchuptv.CatchUpTvActivity

class CatchUpTvEventHandler(private val mActivity: CatchUpTvActivity) {
    /**
     * Utils
     */
    val mUtils= Utils.getUtils()

    /**
     * Assinging LoadingDialog's singleton object into @mProgressLoader
     */
    private val mProgressLoader = LoadingDialog.getLoader()

    /**
     * AppComponent of Dagger
     */
    var mAppComponent= MyApp.get(mActivity).getAppComponents()
}