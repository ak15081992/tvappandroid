package com.app.couchpotato.ui.vlcmediaplayer

import android.annotation.SuppressLint
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.*
import android.widget.*
import android.widget.SeekBar.OnSeekBarChangeListener
import com.app.couchpotato.R
import com.app.couchpotato.base.PlayerBaseActivity
import com.app.couchpotato.commonmodel.CommonLists
import com.app.couchpotato.commonmodel.UserModel
import com.app.couchpotato.dagger.module.ModuleApiInterface
import com.app.couchpotato.helperutils.Utils
import org.videolan.libvlc.IVLCVout
import org.videolan.libvlc.LibVLC
import org.videolan.libvlc.Media
import org.videolan.libvlc.MediaPlayer
import java.util.*
import kotlin.math.roundToInt

class VlcMediaPlayerActivity : PlayerBaseActivity(), View.OnClickListener,
    IVLCVout.Callback, IVLCVout.OnNewVideoLayoutListener, MediaPlayer.EventListener,
    OnSeekBarChangeListener {
    private var mVideoSurface: SurfaceView? = null
    private var mVideoSurfaceFrame: FrameLayout? = null
    private lateinit var mLibVLC: LibVLC
    private lateinit var mMediaPlayer: MediaPlayer
    private var vlcSeekbar: SeekBar? = null
    private var tvDuration: TextView? = null
    private var llController: LinearLayout? = null
    private val mHandler = Handler()
    private var mOnLayoutChangeListener: View.OnLayoutChangeListener? = null
    private var mVideoHeight = 0
    private var mVideoWidth = 0
    private var mVideoVisibleHeight = 0
    private var mVideoVisibleWidth = 0
    private var mVideoSarNum = 0
    private var mVideoSarDen = 0
    var playPosition = 0
    var videoTitle: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vlc_media_player)
        streamUrl = ""
        if (getIntent().hasExtra("streamUrl")) streamUrl = getIntent().getStringExtra("streamUrl")
        videoTitle = ""
        if (getIntent().hasExtra("videoName")) videoTitle = getIntent().getStringExtra("videoName")
        if (getIntent().hasExtra("videoIndex")) {
            playPosition = getIntent().getIntExtra("videoIndex", 0)
        }
        if (savedInstanceState == null) {
            playWhenReady = true
            playbackPosition = 0
        } else {
            playWhenReady =
                savedInstanceState.getBoolean(KEY_PLAY_WHEN_READY)
            playbackPosition = savedInstanceState.getLong(KEY_POSITION)
        }
        initControls()
        setEventListener()
        btnPlay?.requestFocus()
        Utils.getUtils().disableSSLCertificateChecking()
    }

    private fun reloadVideo() {
        releasePlayer()
        playWhenReady = true
        playbackPosition = 0
        val userName: String = UserModel.getUserModel().userInfo?.username.toString()
        val password: String = UserModel.getUserModel().userInfo?.password.toString()
        if (CommonLists.EPISODE_ITEM_ARRAY_LIST.size > 0 && playPosition > -1 && playPosition < CommonLists.EPISODE_ITEM_ARRAY_LIST.size) {
            streamUrl =
                ModuleApiInterface.BASE_URL + "series/" + userName + "/" + password + "/" +
                        CommonLists.EPISODE_ITEM_ARRAY_LIST.get(playPosition).id + "." + CommonLists.EPISODE_ITEM_ARRAY_LIST[playPosition].container_extension
            videoTitle = CommonLists.EPISODE_ITEM_ARRAY_LIST.get(playPosition).name
        } else if (CommonLists.DEMAND_ITEM_ARRAY_LIST.size > 0 && playPosition > -1 && playPosition < CommonLists.DEMAND_ITEM_ARRAY_LIST.size) {
            streamUrl =
                ModuleApiInterface.BASE_URL + "movie/" + userName + "/" + password + "/" + CommonLists.DEMAND_ITEM_ARRAY_LIST[playPosition].streamId + "." + CommonLists.DEMAND_ITEM_ARRAY_LIST.get(playPosition).containerExtension
            videoTitle = CommonLists.DEMAND_ITEM_ARRAY_LIST.get(playPosition).name
        }
        initializePlayer()
    }

    private fun initControls() {
        mVideoSurface = findViewById(R.id.video_surface)
        mVideoSurfaceFrame = findViewById(R.id.video_surface_frame)
        vlcSeekbar = findViewById(R.id.vlc_seekbar)
        tvDuration = findViewById(R.id.tvDuration)
        llController = findViewById(R.id.llController)
        btnPlay = findViewById(R.id.btnPlay)
        btnReplay = findViewById(R.id.btnReplay)
        btnForward = findViewById(R.id.btnForward)
        btnPrev = findViewById(R.id.btnPrev)
        btnNext = findViewById(R.id.btnNext)
        btnClosedCaption = findViewById(R.id.btnClosedCaption)
        btnAudioTrack = findViewById(R.id.btnAudioTrack)
        tvTitle = findViewById(R.id.tvTitle)
        tvTitle.setText(videoTitle)
        progressBar = findViewById(R.id.progress_bar)
        vlcSeekbar!!.setOnSeekBarChangeListener(this)
    }

    override fun setEventListener() {
        btnPlay.setOnClickListener(this)
        btnForward.setOnClickListener(this)
        btnReplay.setOnClickListener(this)
        btnPrev.setOnClickListener(this)
        btnNext.setOnClickListener(this)
        btnClosedCaption.setOnClickListener(this)
        btnAudioTrack.setOnClickListener(this)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun initializePlayer() {
        mVideoSurfaceFrame!!.setOnTouchListener { v: View?, event: MotionEvent ->
            if (event.action == MotionEvent.ACTION_DOWN) {
                if (btnPlay.isEnabled()) {
                    onPlay()
                }
            }
            false
        }
        tvTitle.text = videoTitle
        val args = ArrayList<String>()
        args.add("--vout=android-display")
        args.add("-vvv")
        mLibVLC = LibVLC(this, args)
        mMediaPlayer = MediaPlayer(mLibVLC)
        val vlcVout: IVLCVout = mMediaPlayer.vlcVout
        vlcVout.setVideoView(mVideoSurface)
        vlcVout.attachViews(this)
        mMediaPlayer.vlcVout.addCallback(this)
        mMediaPlayer.setEventListener(this)
        Log.e("streamUrl", streamUrl)
        val media = Media(mLibVLC, Uri.parse(Utils.getUtils().makeAvaiableUrl(streamUrl.toString())))
        mMediaPlayer.media = media
        media.release()
        if (playWhenReady) mMediaPlayer.play()
        if (mOnLayoutChangeListener == null) {
            mOnLayoutChangeListener = object : View.OnLayoutChangeListener {
                private val mRunnable =
                    Runnable { updateVideoSurfaces() }

                override fun onLayoutChange(
                    v: View, left: Int, top: Int, right: Int,
                    bottom: Int, oldLeft: Int, oldTop: Int, oldRight: Int, oldBottom: Int
                ) {
                    if (left != oldLeft || top != oldTop || right != oldRight || bottom != oldBottom) {
                        mHandler.removeCallbacks(mRunnable)
                        mHandler.post(mRunnable)
                    }
                }
            }
        }
        mVideoSurfaceFrame!!.addOnLayoutChangeListener(mOnLayoutChangeListener)
    }

    override fun releasePlayer() {
        updateStartPosition()
        if (mOnLayoutChangeListener != null) {
            mVideoSurfaceFrame!!.removeOnLayoutChangeListener(mOnLayoutChangeListener)
            mOnLayoutChangeListener = null
        }
        mMediaPlayer.stop()
        mMediaPlayer.release()
        mLibVLC.release()
        mMediaPlayer.getVLCVout().detachViews()
        mMediaPlayer.getVLCVout().removeCallback(this)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btnPlay -> onPlay()
            R.id.btnForward -> onForward()
            R.id.btnReplay -> onReplay()
            R.id.btnNext -> onNext()
            R.id.btnPrev -> onPrev()
            R.id.btnClosedCaption -> {
                onCC()
                onAudioTrack()
            }
            R.id.btnAudioTrack -> onAudioTrack()
        }
    }

    protected override fun onPlay() {
        if (mMediaPlayer.isPlaying()) {
            onPauseVideo()
        } else {
            onPlayVideo()
        }
    }

    override fun onForward() {
        val curPos: Long = mMediaPlayer.getTime()
        val duration: Long = mMediaPlayer.getLength()
        val seekPos =
            Math.min(curPos + SEEK_OFFSET, duration)
        mMediaPlayer.setTime(seekPos)
        onPositionChanged()
    }

    protected override fun onReplay() {
        val curPos: Long = mMediaPlayer.getTime()
        val seekPos =
            if (curPos - SEEK_OFFSET < 0) 0 else curPos - SEEK_OFFSET
        mMediaPlayer.setTime(seekPos)
        onPositionChanged()
    }

    protected override fun onPrev() {
        playPosition--
        reloadVideo()
    }

    protected override fun onNext() {
        playPosition++
        reloadVideo()
    }

    override fun onCC() {
        if (mMediaPlayer != null && mMediaPlayer.getSpuTracksCount() > 1) {
            val trackDescriptions: Array<MediaPlayer.TrackDescription> =
                mMediaPlayer.getSpuTracks()
            val popupMenu =
                PopupMenu(this, btnClosedCaption)
            for (trackDescription in trackDescriptions) {
                popupMenu.menu.add(trackDescription.name)
            }
            popupMenu.setOnMenuItemClickListener { item: MenuItem ->
                var index = 0
                for (trackDescription in trackDescriptions) {
                    if (trackDescription.name.equals(item.title.toString(),true)) {
                        break
                    }
                    index++
                }
                mMediaPlayer.setSpuTrack(trackDescriptions[index].id)
                true
            }
            popupMenu.show()
        }
    }

    protected override fun onAudioTrack() {
        if (mMediaPlayer != null && mMediaPlayer.audioTracksCount > 1) {
            val trackDescriptions: Array<MediaPlayer.TrackDescription> =
                mMediaPlayer.audioTracks
            val popupMenu =
                PopupMenu(this, btnAudioTrack)
            for (trackDescription in trackDescriptions) {
                popupMenu.menu.add(trackDescription.name)
            }
            popupMenu.setOnMenuItemClickListener { item: MenuItem ->
                var index = 0
                for (trackDescription in trackDescriptions) {
                    if (trackDescription.name.equals(item.title.toString(),true)) {
                        break
                    }
                    index++
                }
                mMediaPlayer.setAudioTrack(trackDescriptions[index].id)
                true
            }
            popupMenu.show()
        }
    }

    protected override fun onPlayVideo() {
        if (mMediaPlayer != null) mMediaPlayer.play()
    }

    protected override fun onPauseVideo() {
        if (mMediaPlayer != null) mMediaPlayer.pause()
    }

    private fun changeMediaPlayerLayout(displayW: Int, displayH: Int) {
        /* Change the video placement using the MediaPlayer API */
        val vtrack: Media.VideoTrack = mMediaPlayer.getCurrentVideoTrack() ?: return
        val videoSwapped =
            (vtrack.orientation === Media.VideoTrack.Orientation.LeftBottom
                    || vtrack.orientation === Media.VideoTrack.Orientation.RightTop)
        var videoW: Int = vtrack.width
        var videoH: Int = vtrack.height
        if (videoSwapped) {
            val swap = videoW
            videoW = videoH
            videoH = swap
        }
        if (vtrack.sarNum !== vtrack.sarDen) videoW = videoW * vtrack.sarNum / vtrack.sarDen
        val ar = videoW / videoH.toFloat()
        val dar = displayW / displayH.toFloat()
        val scale: Float
        scale =
            if (dar >= ar) displayW / videoW.toFloat() /* horizontal */ else displayH / videoH.toFloat() /* vertical */
        mMediaPlayer.setScale(scale)
        mMediaPlayer.setAspectRatio(null)
    }

    private fun updateVideoSurfaces() {
        val sw: Int = getWindow().getDecorView().getWidth()
        val sh: Int = getWindow().getDecorView().getHeight()

        // sanity check
        if (sw * sh == 0) {
            Log.e(VIDEO_PLAYER_TAG, "Invalid surface size")
            return
        }
        mMediaPlayer.getVLCVout().setWindowSize(sw, sh)
        var lp = mVideoSurface!!.layoutParams
        if (mVideoWidth * mVideoHeight == 0) {
            /* Case of OpenGL vouts: handles the placement of the video using MediaPlayer API */
            lp.width = ViewGroup.LayoutParams.MATCH_PARENT
            lp.height = ViewGroup.LayoutParams.MATCH_PARENT
            mVideoSurface!!.layoutParams = lp
            lp = mVideoSurfaceFrame!!.layoutParams
            lp.width = ViewGroup.LayoutParams.MATCH_PARENT
            lp.height = ViewGroup.LayoutParams.MATCH_PARENT
            mVideoSurfaceFrame!!.layoutParams = lp
            changeMediaPlayerLayout(sw, sh)
            return
        }
        if (lp.width == lp.height && lp.width == ViewGroup.LayoutParams.MATCH_PARENT) {
            mMediaPlayer.aspectRatio = null
            mMediaPlayer.scale = 0F
        }
        var dw = sw.toDouble()
        var dh = sh.toDouble()
        if (sw < sh) {
            dw = sh.toDouble()
            dh = sw.toDouble()
        }

        // compute the aspect ratio
        val ar: Double
        val vw: Double
        if (mVideoSarDen == mVideoSarNum) {
            /* No indication about the density, assuming 1:1 */
//            vw = mVideoVisibleWidth;
            ar = mVideoVisibleWidth.toDouble() / mVideoVisibleHeight.toDouble()
        } else {
            /* Use the specified aspect ratio */
            vw = mVideoVisibleWidth * mVideoSarNum.toDouble() / mVideoSarDen
            ar = vw / mVideoVisibleHeight
        }

        // compute the display aspect ratio
        val dar = dw / dh
        if (dar >= ar) dw = dh * ar /* horizontal */ else dh = dw / ar /* vertical */

        // set display size
        lp.width = dw.toInt() //(int) Math.ceil(dw * mVideoWidth / mVideoVisibleWidth);
        lp.height = dh.toInt() //(int) Math.ceil(dh * mVideoHeight / mVideoVisibleHeight);
        mVideoSurface!!.layoutParams = lp

        // set frame size (crop if necessary)
        /*lp = mVideoSurfaceFrame.getLayoutParams();
        lp.width = (int) Math.floor(dw);
        lp.height = (int) Math.floor(dh);
        mVideoSurfaceFrame.setLayoutParams(lp);*/mVideoSurface!!.invalidate()
    }

    override fun onNewVideoLayout(
        vlcVout: IVLCVout?,
        width: Int,
        height: Int,
        visibleWidth: Int,
        visibleHeight: Int,
        sarNum: Int,
        sarDen: Int
    ) {
        mVideoWidth = width
        mVideoHeight = height
        mVideoVisibleWidth = visibleWidth
        mVideoVisibleHeight = visibleHeight
        mVideoSarNum = sarNum
        mVideoSarDen = sarDen
        updateVideoSurfaces()
    }

    override fun onSurfacesCreated(vlcVout: IVLCVout?) {}
    override fun onSurfacesDestroyed(vlcVout: IVLCVout?) {}
    private fun showController() {
        llController!!.visibility = View.VISIBLE
        btnPlay.requestFocus()
    }

    private fun hideController() {
        llController!!.visibility = View.GONE
    }

    override fun onEvent(event: MediaPlayer.Event) {
        when (event.type) {
            MediaPlayer.Event.Buffering ->                 //Log.e("aaa", "Buffering");
                onBuffering()
            MediaPlayer.Event.EncounteredError ->                 //Log.e("aaa", "EncounteredError");
                onEncounteredError()
            MediaPlayer.Event.EndReached ->                 //Log.e("aaa", "EndReached");
                onEndReached()
            MediaPlayer.Event.ESAdded ->                 //Log.e("aaa", "ESAdded");
                onESAdded()
            MediaPlayer.Event.ESDeleted ->                 //Log.e("aaa", "ESDeleted");
                onESDeleted()
            MediaPlayer.Event.MediaChanged ->                 //Log.e("aaa", "MediaChanged");
                onMediaChanged()
            MediaPlayer.Event.Opening ->                 //Log.e("aaa", "Opening");
                onOpening()
            MediaPlayer.Event.PausableChanged ->                 //Log.e("aaa", "PausableChanged");
                onPausableChanged()
            MediaPlayer.Event.Paused ->                 //Log.e("aaa", "Paused");
                onPaused()
            MediaPlayer.Event.Playing ->                 //Log.e("aaa", "Playing");
                onPlaying()
            MediaPlayer.Event.PositionChanged ->                 //Log.e("aaa", "PositionChanged");
                onPositionChanged()
            MediaPlayer.Event.Stopped ->                 //Log.e("aaa", "Stopped");
                onStopped()
            MediaPlayer.Event.TimeChanged ->                 //Log.e("aaa", "TimeChanged");
                onTimeChanged()
            MediaPlayer.Event.Vout ->                 //Log.e("aaa", "Vout");
                onVout()
        }
    }

    private fun onBuffering() {
        progressBar?.setVisibility(View.VISIBLE)
    }

    private fun onEncounteredError() {
        progressBar?.setVisibility(View.GONE)
        Toast.makeText(this, "There is no found real media.", Toast.LENGTH_SHORT)
            .show()
    }

    private fun onEndReached() {
        if (mMediaPlayer != null && mMediaPlayer.time != -1L && mMediaPlayer.position != -1F) {
            val curTime: Long = mMediaPlayer.getLength()
            val totalTime: Long = mMediaPlayer.getLength()
            val minutes = (curTime / (60 * 1000)).toInt()
            val seconds = (curTime / 1000 % 60).toInt()
            val endMinutes = (totalTime / (60 * 1000)).toInt()
            val endSeconds = (totalTime / 1000 % 60).toInt()
            @SuppressLint("DefaultLocale") val duration = String.format(
                "%02d:%02d / %02d:%02d",
                minutes,
                seconds,
                endMinutes,
                endSeconds
            )
            vlcSeekbar!!.progress = 100
            tvDuration!!.text = duration
        }
        onNext()
    }

    private fun onESAdded() {}
    private fun onESDeleted() {}
    private fun onMediaChanged() {}
    private fun onOpening() {}
    private fun onPausableChanged() {}
    private fun onPaused() {
        btnPlay.setImageResource(R.drawable.player_play)
        showController()
    }

    private fun onPlaying() {
        btnPlay.setImageResource(R.drawable.player_pause)
        mHandler.postDelayed({ hideController() }, 1000)
    }

    private fun onPositionChanged() {
        progressBar?.setVisibility(View.GONE)
        if (mMediaPlayer != null && mMediaPlayer.time != -1L && mMediaPlayer.position != -1F) {
            val curTime: Long = mMediaPlayer.getTime()
            val totalTime: Long = mMediaPlayer.getLength()
            val minutes = (curTime / (60 * 1000)).toInt()
            val seconds = (curTime / 1000 % 60).toInt()
            val endHours = (totalTime / (60 * 60 * 1000)).toInt()
            val endMinutes = ((totalTime - endHours * 60 * 60 * 1000) / (60 * 1000)).toInt()
            val endSeconds = (totalTime / 1000 % 60).toInt()
            @SuppressLint("DefaultLocale") val duration = String.format(
                "%02d:%02d / %02d:%02d:%02d",
                minutes,
                seconds,
                endHours,
                endMinutes,
                endSeconds
            )
            vlcSeekbar!!.progress = ((mMediaPlayer.getPosition() * 100).roundToInt())
            tvDuration!!.text = duration
        }
    }

    private fun onStopped() {
        mMediaPlayer.stop()
        btnPlay?.setImageResource(R.drawable.player_play)
    }

    private fun onTimeChanged() {
        progressBar?.setVisibility(View.GONE)
    }

    private fun onVout() {}
    override fun onProgressChanged(
        seekBar: SeekBar,
        progress: Int,
        fromUser: Boolean
    ) {
        if (fromUser && mMediaPlayer != null) {
            mMediaPlayer.setTime(mMediaPlayer.getLength() * progress / 100)
        }
        assert(mMediaPlayer != null)
        if (!mMediaPlayer.isPlaying()) {
            onPauseVideo()
        }
        mHandler.post { progressBar?.setVisibility(View.GONE) }
    }

    override fun onStartTrackingTouch(seekBar: SeekBar) {}
    override fun onStopTrackingTouch(seekBar: SeekBar) {}
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (event.action != KeyEvent.ACTION_DOWN) return true
        Log.e("Keycode", keyCode.toString())
        return when (keyCode) {
            KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE, KeyEvent.KEYCODE_MEDIA_PLAY -> {
                onPlay()
                true
            }
            KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                showController()
                true
            }
            KeyEvent.KEYCODE_MEDIA_FAST_FORWARD -> {
                onForward()
                true
            }
            KeyEvent.KEYCODE_MEDIA_REWIND -> {
                onReplay()
                true
            }
            KeyEvent.KEYCODE_MEDIA_PAUSE -> {
                onPaused()
                true
            }
            KeyEvent.KEYCODE_MEDIA_STOP -> {
                onStopped()
                true
            }
            KeyEvent.KEYCODE_MEDIA_NEXT -> true
            KeyEvent.KEYCODE_MEDIA_PREVIOUS -> true
            else -> super.onKeyDown(keyCode, event)
        }
    }

    companion object {
        private const val VIDEO_PLAYER_TAG = "VideoPlayerActivity"
        private const val KEY_PLAY_WHEN_READY = "play_when_ready"
        private const val KEY_POSITION = "position"
        private const val SEEK_OFFSET = 10000
    }
}