package com.app.couchpotato.ui.episode

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.GridLayoutManager
import com.app.couchpotato.R
import com.app.couchpotato.commonmodel.CommonLists
import com.app.couchpotato.commonmodel.UserModel
import com.app.couchpotato.dagger.module.ModuleApiInterface
import com.app.couchpotato.databinding.ActivityEpisodeBinding
import com.app.couchpotato.helperlisteners.RecyclerItemClickListener
import com.app.couchpotato.helperutils.Constants
import com.app.couchpotato.ui.episode.adapter.EpisodeRecyclerAdapter
import com.app.couchpotato.ui.episode.model.EpisodeItem
import com.app.couchpotato.ui.episode.presenter.EpisodeEventHandler
import com.app.couchpotato.ui.vlcmediaplayer.VlcMediaPlayerActivity
import kotlinx.android.synthetic.main.activity_episode.*

class EpisodeActivity : AppCompatActivity() {

    /**
     * Binding
     */
    var mBinding: ActivityEpisodeBinding?=null

    var list: ArrayList<EpisodeItem> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding=DataBindingUtil.setContentView(
            this,
            R.layout.activity_episode
        )
        mBinding?.mEpisodeEventHandler= EpisodeEventHandler(this)

        initUI()

        mBinding?.mEpisodeEventHandler?.getSeriesInfoResponseBodyAPIImpl()
    }

    private fun initUI() {
        recyclerviewEpisode.layoutManager= GridLayoutManager(this,5)
        recyclerviewEpisode.addOnItemTouchListener(
            RecyclerItemClickListener(
                this,
                object: RecyclerItemClickListener.OnItemClickListener{
                    override fun onItemClick(view: View, position: Int) {
                        val streamUrl: String =
                            ModuleApiInterface.BASE_URL+ "series/" + UserModel.getUserModel().userInfo?.username + "/" +
                                    UserModel.getUserModel().userInfo?.password+ "/" +
                                    list[position].id + "." + list[position].container_extension
                        CommonLists.DEMAND_ITEM_ARRAY_LIST.clear()
                        CommonLists.EPISODE_ITEM_ARRAY_LIST.clear()
                        CommonLists.EPISODE_ITEM_ARRAY_LIST.addAll(list)
                        val intent =
                            Intent(this@EpisodeActivity, VlcMediaPlayerActivity::class.java)
                        intent.putExtra("streamUrl", streamUrl)
                        intent.putExtra("videoName", list[position].title)
                        intent.putExtra("videoIndex", position)
                        startActivity(intent)
                    }
                }
            )
        )
    }


    fun updateUI() {
        tvSeriesName.text="Season "+intent?.getStringExtra(Constants.KEY_SEASON_NO)
        tvSeriesName.visibility=View.VISIBLE
        recyclerviewEpisode.adapter=EpisodeRecyclerAdapter(this,list)
    }
}
