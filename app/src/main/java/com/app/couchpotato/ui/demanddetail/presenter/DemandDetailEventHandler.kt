package com.app.couchpotato.ui.demanddetail.presenter

import androidx.core.content.ContextCompat
import com.app.couchpotato.MyApp
import com.app.couchpotato.R
import com.app.couchpotato.commonmodel.UserModel
import com.app.couchpotato.customview.loader.LoadingDialog
import com.app.couchpotato.helperutils.Constants
import com.app.couchpotato.helperutils.Utils
import com.app.couchpotato.ui.demanddetail.DemandDetailActivity
import com.app.couchpotato.ui.demanddetail.model.ResponseVideoDetail
import com.app.couchpotato.ui.moviesondemand.model.ResponseMovies
import com.tapadoo.alerter.Alerter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DemandDetailEventHandler(private val mActivity: DemandDetailActivity) {

    /**
     * Utils
     */
    val mUtils = Utils.getUtils()

    /**
     * Assinging LoadingDialog's singleton object into @mProgressLoader
     */
    private val mProgressLoader = LoadingDialog.getLoader()

    /**
     * AppComponent of Dagger
     */
    var mAppComponent = MyApp.get(mActivity).getAppComponents()

    fun getDemandDetailsAPIHandler() {
        val mAlerter=Alerter.create(mActivity)
        if (mUtils.isOnline(mActivity)) {
            mProgressLoader.showLoader(mActivity)
            mAppComponent.getApiHelper().getVideoInfo(
                username = UserModel.getUserModel().userInfo?.username.toString(),
                password = UserModel.getUserModel().userInfo?.password.toString(),
                videoId = mActivity.intent.getStringExtra(Constants.KEY_VIDEO_ID)
            ).enqueue(object: Callback<ResponseVideoDetail>{
                override fun onFailure(call: Call<ResponseVideoDetail>, t: Throwable) {
                    mProgressLoader.dismissLoader()
                    mUtils.showAlerter(
                        mAlerter,
                        R.color.colorRedError,
                        R.drawable.ic_error_white_24dp,
                        t.localizedMessage
                    )
                }

                override fun onResponse(
                    call: Call<ResponseVideoDetail>,
                    response: Response<ResponseVideoDetail>
                ) {
                    mProgressLoader.dismissLoader()
                    try {
                        if (response.code() == 200) {
                            mActivity.updateUI(response.body())
                        } else {
                            mUtils.showAlerter(
                                mAlerter,
                                R.color.colorRedError,
                                R.drawable.ic_error_white_24dp,
                                response.message()
                            )
                        }
                    } catch (e:Exception) {
                        e.printStackTrace()
                        mUtils.showAlerter(
                            mAlerter,
                            R.color.colorRedError,
                            R.drawable.ic_error_white_24dp,
                            mActivity.resources.getString(R.string.somethingwentwrong)
                        )
                    }
                }

            })
        } else {
            mUtils.showAlerter(
                mAlerter,
                R.color.colorRedError,
                R.drawable.ic_error_white_24dp,
                mActivity.resources.getString(R.string.networkerror)
            )
        }
    }
}