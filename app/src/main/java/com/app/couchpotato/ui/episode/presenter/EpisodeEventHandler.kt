package com.app.couchpotato.ui.episode.presenter

import com.app.couchpotato.MyApp
import com.app.couchpotato.R
import com.app.couchpotato.commonmodel.UserModel
import com.app.couchpotato.customview.loader.LoadingDialog
import com.app.couchpotato.helperutils.Constants
import com.app.couchpotato.helperutils.Utils
import com.app.couchpotato.ui.episode.EpisodeActivity
import com.app.couchpotato.ui.episode.model.EpisodeItem
import com.tapadoo.alerter.Alerter
import okhttp3.ResponseBody
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class EpisodeEventHandler(private val mActivity: EpisodeActivity) {

    /**
     * Utils
     */
    val mUtils = Utils.getUtils()

    /**
     * Assinging LoadingDialog's singleton object into @mProgressLoader
     */
    private val mProgressLoader = LoadingDialog.getLoader()

    /**
     * AppComponent of Dagger
     */
    var mAppComponent = MyApp.get(mActivity).getAppComponents()

    fun getSeriesInfoResponseBodyAPIImpl() {
        val mAlerter=Alerter.create(mActivity)
        if (mUtils.isOnline(mActivity)) {
            mProgressLoader.showLoader(mActivity)
            mAppComponent.getApiHelper().getSeasonListResponseBody(
                username = UserModel.getUserModel().userInfo?.username.toString(),
                password = UserModel.getUserModel().userInfo?.password.toString(),
                seriesId = mActivity.intent?.getStringExtra(Constants.KEY_SERIES_ID)
            ).enqueue(object: Callback<ResponseBody>{
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    mProgressLoader.dismissLoader()
                    mUtils.showAlerter(
                        mAlerter,
                        R.color.colorRedError,
                        R.drawable.ic_error_white_24dp,
                        t.localizedMessage!!
                    )
                }

                override fun onResponse(
                    call: Call<ResponseBody>,
                    response: Response<ResponseBody>
                ) {
                    try {
                        if (response.code()==200) {
                            val result=response.body()?.string()
                            val jsonObj = JSONObject(result!!)
                            val jObjEpisode: JSONObject = jsonObj.getJSONObject("episodes")
                            val EpisodeArray: JSONArray
                            EpisodeArray = jObjEpisode.getJSONArray(mActivity.intent?.getStringExtra(Constants.KEY_SEASON_NO)!!)
                            for (i in 0 until EpisodeArray.length()) {
                                val jb = EpisodeArray.getJSONObject(i)
                                val item = EpisodeItem()
                                item.id = jb.optString("id", "")
                                item.title = jb.optString("title", "")
                                item.container_extension = jb.optString("container_extension", "")
                                item.movie_image =
                                    jb.getJSONObject("info").optString("movie_image", "")
                                if (item.movie_image.contains("streamztv.uk.to")) item.movie_image =
                                    item.movie_image.replace("streamztv.uk.to", "163.172.102.165")
                                mActivity.list.add(item)
                            }
                            mActivity.updateUI()
                            mProgressLoader.dismissLoader()

                        } else {
                            mProgressLoader.dismissLoader()
                            mUtils.showAlerter(
                                mAlerter,
                                R.color.colorRedError,
                                R.drawable.ic_error_white_24dp,
                                mActivity.resources.getString(R.string.somethingwentwrong)
                            )
                        }
                    } catch (e:Exception) {
                        mProgressLoader.dismissLoader()
                        e.printStackTrace()
                        mUtils.showAlerter(
                            mAlerter,
                            R.color.colorRedError,
                            R.drawable.ic_error_white_24dp,
                            mActivity.resources.getString(R.string.somethingwentwrong)
                        )
                    }
                }

            })
        } else {
            mUtils.showAlerter(
                mAlerter,
                R.color.colorRedError,
                R.drawable.ic_error_white_24dp,
                mActivity.resources.getString(R.string.networkerror)
            )
        }
    }
}