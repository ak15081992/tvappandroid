package com.app.couchpotato.ui.tvondemand.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.couchpotato.R
import com.app.couchpotato.customview.recycleritemdecorator.LinearRecyclerItemDecorator
import com.app.couchpotato.helperutils.Constants
import com.app.couchpotato.ui.season.SeasonActivity
import com.app.couchpotato.ui.tvondemand.TvOnDemandActivity
import com.app.couchpotato.ui.tvondemand.model.ResponseTVSeries
import com.app.couchpotato.ui.tvondemand.model.ResponseTVSeriesCategoryList
import kotlinx.android.synthetic.main.item_on_demand.view.*


class TvSeriesAdapter(
    private val mActivity: TvOnDemandActivity,
    private var mSeriesList: ArrayList<ResponseTVSeriesCategoryList>
) : RecyclerView.Adapter<TvSeriesAdapter.MyViewHolder>() {

    /**
     * Linear Layout Manager
     */
    lateinit var mLinearlayoutManager: LinearLayoutManager

    /**
     * Adapter
     */
    lateinit var mAdapter: SeriesAdapter

    interface SeriesClickListener {
        fun onSeriesClicked(series: ResponseTVSeries?)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_on_demand, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.itemView.tvCategory.text= mSeriesList[position].categoryName
        mLinearlayoutManager= LinearLayoutManager(mActivity,LinearLayoutManager.HORIZONTAL,false)
        holder.itemView.recyclerviewMovie.layoutManager=mLinearlayoutManager
        holder.itemView.recyclerviewMovie.addItemDecoration(
            LinearRecyclerItemDecorator(
                mActivity.resources.getDimension(R.dimen._1sdp).toInt(),
                mActivity.resources.getDimension(R.dimen._1sdp).toInt(),
                mActivity.resources.getDimension(R.dimen._1sdp).toInt(),
                mActivity.resources.getDimension(R.dimen._14sdp).toInt(),
                mActivity.resources.getDimension(R.dimen._18sdp).toInt(),
                10,
                1
            )
        )
        mAdapter= SeriesAdapter(mActivity,mSeriesList[position].series,object: SeriesAdapter.SeriesClickListener{
            override fun onSeriesClicked(mSeries: ResponseTVSeries?) {
                mActivity.startActivity(
                    Intent(mActivity,SeasonActivity::class.java)
                        .putExtra(Constants.KEY_TV_SERIES,mSeries)
                )
            }
        })
        holder.itemView.recyclerviewMovie.adapter=mAdapter
    }

    override fun getItemCount(): Int = mSeriesList.size

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view)
}