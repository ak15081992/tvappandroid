package com.app.couchpotato.ui.videosondemand

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.app.couchpotato.R
import com.app.couchpotato.databinding.ActivityVideosOnDemandBinding
import com.app.couchpotato.ui.videosondemand.presenter.VideosOnDemandEventHandler
import kotlinx.android.synthetic.main.activity_videos_on_demand.*

class VideosOnDemandActivity : Activity() {

    /**
     * Binding
     */
    var mBinding: ActivityVideosOnDemandBinding?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding=DataBindingUtil.setContentView(
            this,
            R.layout.activity_videos_on_demand
        )
        mBinding?.mVideosOnDemandEventHandler= VideosOnDemandEventHandler(this)

        initUi()
    }

    private fun initUi() {
        llMovieOnDemand.requestFocus()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
    }
}
