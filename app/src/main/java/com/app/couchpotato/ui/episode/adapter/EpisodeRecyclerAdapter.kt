package com.app.couchpotato.ui.episode.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.couchpotato.R
import com.app.couchpotato.ui.episode.EpisodeActivity
import com.app.couchpotato.ui.episode.model.EpisodeItem
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_episode.view.*

class EpisodeRecyclerAdapter(
    private val mActivity: EpisodeActivity,
    private var mSeasonList: ArrayList<EpisodeItem>
) : RecyclerView.Adapter<EpisodeRecyclerAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_episode, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        Glide.with(mActivity)
            .load(mSeasonList?.get(position)?.movie_image)
            .centerCrop()
            .into(holder.itemView.ivCover)
        holder.itemView.tvEpisode.text = "Episode "+position.plus(1).toString()+"-"+mSeasonList?.get(position).title
    }

    override fun getItemCount(): Int = mSeasonList?.size!!

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view)
}