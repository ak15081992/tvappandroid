package com.app.couchpotato.ui.tvguide

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.app.couchpotato.R
import com.app.couchpotato.databinding.ActivityTvGuideBinding
import com.app.couchpotato.ui.tvguide.presenter.TVGuideEventHandler

class TvGuideActivity : AppCompatActivity() {

    /**
     * Binding
     */
    var mBinding: ActivityTvGuideBinding?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding=DataBindingUtil.setContentView(
            this,
            R.layout.activity_tv_guide
        )
        mBinding?.mTvGuideEventHandler= TVGuideEventHandler(this)
    }
}
