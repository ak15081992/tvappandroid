package com.app.couchpotato.ui.guide

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.text.format.DateFormat
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.widget.*
import com.app.couchpotato.R
import com.app.couchpotato.adapter.MainMenuAdapter
import com.app.couchpotato.base.PlayerBaseActivity
import com.app.couchpotato.commonmodel.ChannelItem
import com.app.couchpotato.commonmodel.CommonLists
import com.app.couchpotato.commonmodel.CommonLists.Companion.EPGDATA
import com.app.couchpotato.commonmodel.ItemTopic
import com.app.couchpotato.commonmodel.UserModel
import com.app.couchpotato.customview.EPGView
import com.app.couchpotato.customview.EPGView.EPGClickListener
import com.app.couchpotato.helperutils.Constants
import com.app.couchpotato.helperutils.Utils
import java.text.SimpleDateFormat
import java.util.*

class GuideActivity : PlayerBaseActivity() {
    enum class GROUP_CHANNEL_KIND {
        MLB_CHANNEL, EPL_CHANNEL, NBA_CHANNEL, NFL_CHANNEL, NHL_CHANNEL, PPV_CHANNEL
    }

    private var m_tvChannelNumber: TextView? = null
    private var m_tvTopicName: TextView? = null
    private var m_tvTopicDateTime: TextView? = null
    private var m_tvTopicDescription: TextView? = null
    private var m_tvCurrentTime: TextView? = null
    private var m_tvCurrentDate: TextView? = null
    private var m_tvExpirationDate: TextView? = null

    // Category List
    private var m_currentCategory: String? = null
    private var m_bEnableKeyEvent = false
    private val m_timeHandler = Handler()
    private val m_resetTimelineHandler = Handler()
    private var m_epgView: EPGView? = null
    private val runnableUpdateTime: Runnable = object : Runnable {
        override fun run() {
            setTimeInfo()
            m_timeHandler.postDelayed(
                this,
                Constants.UPDATE_TIME_DELAY.toLong()
            )
        }
    }
    private val runnableResetTimeLine =
        Runnable { if (m_epgView != null) m_epgView!!.recalculateAndRedraw(false) }
    var m_rlBottomMenu: RelativeLayout? = null
    private var channelNumber = ""
    private val handler = Handler()
    private val runnable = Runnable {
        var index = 0
        var category = getString(R.string.all_channels)
        for (item in EPGDATA) {
            if (item!!.m_sTvNum == channelNumber) {
                category = item!!.m_sCategory_ID.toString()
                break
            }
        }
        filterChannel(category)
        for (item in CommonLists.EPG_FILTERED_DATA) {
            if (item!!.m_sTvNum == channelNumber) {
                if (m_epgView != null) m_epgView!!.setCurrentChannel(index)
                streamUrl = Utils.getUtils().makeStreamURL(
                    this,
                    CommonLists.EPG_FILTERED_DATA.get(index)?.m_sStreamID.toString()
                )
                initializePlayer()
                break
            }
            index++
        }
        channelNumber = ""
        updateSelectedTopicInfo()
    }
    private var bIsStartFromBeginning = false

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_guide)
        Utils.getUtils().disableSSLCertificateChecking()
        initControl()
        if (!UserModel.getUserModel().userInfo?.expDate.equals("null") && !UserModel.getUserModel().userInfo?.expDate?.isEmpty()!!) {
            m_tvExpirationDate?.setText(UserModel.getUserModel().userInfo?.expDate)
        } else m_tvExpirationDate!!.text = "Unlimited"
        m_timeHandler.postDelayed(runnableUpdateTime, Constants.UPDATE_TIME_DELAY.toLong())
        CommonLists.EPG_FILTERED_DATA.addAll(EPGDATA)
        if (CommonLists.FAVORITE_CHANNEL_ARRAY.size > 0) filterChannel(resources.getString(R.string.favorite_category)) else {
            for (categoryItem in CommonLists.CHANNEL_CATEGORY_LIST) {
                if (categoryItem.m_sCategroyName.toLowerCase().equals("us entertainment")) {
                    filterChannel(categoryItem.m_sCategroyID)
                }
            }
        }
        m_epgView?.setEPGData(CommonLists.EPG_FILTERED_DATA)
        m_epgView!!.recalculateAndRedraw(false)
        m_epgView!!.setEPGClickListener(object : EPGClickListener {
            override fun onChannelClicked(
                channelPosition: Int,
                channelItem: ChannelItem?
            ) {
                /*if (CommonLists.MLBCHANNEL.contains(channelItem)) {
                    showGroupChannelMenu(GROUP_CHANNEL_KIND.MLB_CHANNEL)
                } else if (CommonLists.NBACHANNEL.contains(channelItem)) {
                    showGroupChannelMenu(GROUP_CHANNEL_KIND.NBA_CHANNEL)
                } else if (CommonLists.NFLCHANNEL.contains(channelItem)) {
                    showGroupChannelMenu(GROUP_CHANNEL_KIND.NFL_CHANNEL)
                } else if (CommonLists.NHLCHANNEL.contains(channelItem)) {
                    showGroupChannelMenu(GROUP_CHANNEL_KIND.NHL_CHANNEL)
                } else if (CommonLists.PPVCHANNEL.contains(channelItem)) {
                    showGroupChannelMenu(GROUP_CHANNEL_KIND.PPV_CHANNEL)
                } else if (CommonLists.EPLCHANNEL.contains(channelItem)) {
                    showGroupChannelMenu(GROUP_CHANNEL_KIND.EPL_CHANNEL)
                } else StartActivityFullScreen()*/
            }

            override fun onEventClicked(
                channelPosition: Int,
                programPosition: Int,
                epgEvent: ItemTopic?
            ) {
                updateSelectedTopicInfo()
            }

            override fun onResetButtonClicked() {}
            override fun onLongClicked(channelPosition: Int, epgEvent: ItemTopic?) {
                //showContextMenu()
            }
        })
        val selectedChannelIndex = m_epgView!!.selectedChannelIndex
        if (selectedChannelIndex > -1) {
            val selectedChannel: ChannelItem? =
                CommonLists.EPG_FILTERED_DATA.get(selectedChannelIndex)
            m_tvChannelNumber!!.text = selectedChannel?.m_sTvNum
            updateSelectedTopicInfo()
            streamUrl = Utils.getUtils().makeStreamURL(
                this,
                CommonLists.EPG_FILTERED_DATA[m_epgView!!.selectedChannelIndex]?.m_sStreamID.toString()
            )
        }
        super.setEventListener()
        SetKeyListener()
        bIsStartFromBeginning = false
    }

    override fun onStart() {
        super.onStart()
        /*if (Utils.getSharePreferenceValue(this, CommonLists.HARD_ACCELARATION, "false")
                .contains("true")
        ) {
            getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED,
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED
            )
        }*/
    }

    public override fun onDestroy() {
        super.onDestroy()
        val valueStr = StringBuilder()
        for (channelName in CommonLists.FAVORITE_CHANNEL_ARRAY) {
            valueStr.append(channelName).append(",")
        }
        Utils.getUtils().setString(CommonLists.FAVORITE_ARRAY, valueStr.toString())
        m_epgView!!.clearSelection()
        CommonLists.EPG_FILTERED_DATA.clear()
    }

    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent
    ) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == CommonLists.REQUEST_LIVE_CODE) {
                val selectedChannelIndex = data.getIntExtra(CommonLists.TAG_CHANNEL_INDEX, 0)
                m_epgView!!.setCurrentChannel(selectedChannelIndex)
                val selectedChannel: ChannelItem =
                    CommonLists.EPG_FILTERED_DATA.get(selectedChannelIndex)!!
                m_currentCategory = selectedChannel.m_sCategory_ID
                m_tvChannelNumber!!.text = selectedChannel.m_sTvNum
                updateSelectedTopicInfo()
                streamUrl = Utils.getUtils().makeStreamURL(this,
                    selectedChannel.m_sStreamID.toString()
                )
                initializePlayer()
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun showCategoryDialog() {
        val inflater =
            getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        @SuppressLint("InflateParams") val menuView: View =
            inflater.inflate(R.layout.main_menu_dialog, null, false)
        val categoryDlg = Dialog(this, R.style.Theme_CustomDialog)
        categoryDlg.setContentView(menuView)
        val lvMenu =
            menuView.findViewById<ListView>(R.id.ID_MENU)
        val arrayCategory = ArrayList<String>()
        arrayCategory.add(getString(R.string.favorite_category))
        arrayCategory.add(getString(R.string.all_channels))
        var selectedIndex = 0
        var adultChannelIndex = 0
        if (m_currentCategory != null && m_currentCategory == getString(R.string.all_channels)) selectedIndex =
            1
        for (i in 0 until CommonLists.CHANNEL_CATEGORY_LIST.size) {
            if (CommonLists.CHANNEL_CATEGORY_LIST.get(i).m_sCategroyID.equals(Utils.getUtils().AdultLiveCategoryNumber) && Utils.getUtils().setString(
                    Constants.PARENTAL_LOCK,
                    "false"
                ).equals("true")
            ) {
                adultChannelIndex = i
                continue
            }
            arrayCategory.add(CommonLists.CHANNEL_CATEGORY_LIST.get(i).m_sCategroyName.toUpperCase())
            if (m_currentCategory != null && !m_currentCategory!!.isEmpty() && m_currentCategory.equals(
                    CommonLists.CHANNEL_CATEGORY_LIST.get(i).m_sCategroyID,
                    ignoreCase = true
                )
            ) {
                selectedIndex = if (Utils.getUtils().setString(
                        Constants.PARENTAL_LOCK,
                        "false"
                    ).equals("true") && i > adultChannelIndex
                ) {
                    i + 1
                } else {
                    i + 2
                }
            }
        }
        if (m_currentCategory != null && selectedIndex > 0) {
            val selectedCategory = arrayCategory[selectedIndex]
            arrayCategory.removeAt(selectedIndex)
            arrayCategory.add(0, selectedCategory)
            selectedIndex = 0
        }
        val reportAdapter = MainMenuAdapter(this, arrayCategory)
        lvMenu.adapter = reportAdapter
        lvMenu.setSelection(selectedIndex)
        lvMenu.onItemClickListener =
            AdapterView.OnItemClickListener { parent: AdapterView<*>?, view: View?, position: Int, id: Long ->
                var categoryID = ""
                if (arrayCategory[position] == resources.getString(R.string.favorite_category)) categoryID =
                    resources.getString(R.string.favorite_category) else if (arrayCategory[position] == resources.getString(
                        R.string.all_channels
                    )
                ) categoryID = resources.getString(R.string.all_channels) else {
                    for (info in CommonLists.CHANNEL_CATEGORY_LIST) {
                        if (info.m_sCategroyName.toLowerCase().equals(arrayCategory[position].toLowerCase())) {
                            categoryID = info.m_sCategroyID
                            break
                        }
                    }
                }
                filterChannel(categoryID)
                categoryDlg.dismiss()
            }
        categoryDlg.show()
    }

    fun filterChannel(category: String?) {
        if (m_epgView == null) return
        /*if (category == CommonLists.MLB_CATEGORY_ID) {
            showGroupChannelMenu(GROUP_CHANNEL_KIND.MLB_CHANNEL)
        } else if (category == CommonLists.EPL_CATEGORY_ID) {
            showGroupChannelMenu(GROUP_CHANNEL_KIND.EPL_CHANNEL)
        } else if (category == CommonLists.NBA_CATEGORY_ID) {
            showGroupChannelMenu(GROUP_CHANNEL_KIND.NBA_CHANNEL)
        } else if (category == CommonLists.NFL_CATEGORY_ID) {
            showGroupChannelMenu(GROUP_CHANNEL_KIND.NFL_CHANNEL)
        } else if (category == CommonLists.NHL_CATEGORY_ID) {
            showGroupChannelMenu(GROUP_CHANNEL_KIND.NHL_CHANNEL)
        } else if (category == CommonLists.PPV_CATEGORY_ID) {
            showGroupChannelMenu(GROUP_CHANNEL_KIND.PPV_CHANNEL)
        }*/
        m_epgView!!.clearSelection()
        m_currentCategory = category
        CommonLists.EPG_FILTERED_DATA.clear()
        for (item in CommonLists.EPGDATA) {
            if (m_currentCategory == getString(R.string.favorite_category)) {
                if (CommonLists.FAVORITE_CHANNEL_ARRAY.contains(item?.m_sStreamID)) {
                    if (item?.m_sCategory_ID == Utils.getUtils().AdultLiveCategoryNumber && Utils.getUtils().setString(
                            Constants.PARENTAL_LOCK,
                            "false"
                        ).equals("true")
                    ) {
                        continue
                    }
                    CommonLists.EPG_FILTERED_DATA.add(item)
                }
            } else {
                if (item?.m_sCategory_ID != null && item?.m_sCategory_ID.equals(
                        m_currentCategory,
                        ignoreCase = true
                    ) || m_currentCategory == getString(R.string.all_channels)
                ) {
                    if (item?.m_sCategory_ID == Utils.getUtils().AdultLiveCategoryNumber && Utils.getUtils().setString(
                            Constants.PARENTAL_LOCK,
                            "false"
                        ).equals("true")
                    ) {
                        continue
                    }
                    CommonLists.EPG_FILTERED_DATA.add(item)
                }
            }
        }
        m_epgView!!.recalculateAndRedraw(false)
    }

    /*private fun showGroupChannelMenu(kind: GROUP_CHANNEL_KIND) {
        val inflater =
            getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        @SuppressLint("InflateParams") val groupView: View =
            inflater.inflate(R.layout.group_channel_dialog, null, false)
        val groupChannelDlg =
            Dialog(this, R.style.Theme_CustomDialog)
        groupChannelDlg.setContentView(groupView)
        val iv =
            groupView.findViewById<ImageView>(R.id.ID_GROUP_CHANNEL_IMG)
        val lvMenu =
            groupView.findViewById<ListView>(R.id.ID_LIST_VIEW_SUB_CHANNEL)
        val channelAdapter: ChannelAdapter
        val arrayChannelItem: ArrayChannelItem
        when (kind) {
            GROUP_CHANNEL_KIND.MLB_CHANNEL -> {
                arrayChannelItem = CommonLists.MLBCHANNEL
                iv.setImageResource(R.drawable.mlb)
            }
            GROUP_CHANNEL_KIND.NBA_CHANNEL -> {
                arrayChannelItem = CommonLists.NBACHANNEL
                iv.setImageResource(R.drawable.nba)
            }
            GROUP_CHANNEL_KIND.NFL_CHANNEL -> {
                arrayChannelItem = CommonLists.NFLCHANNEL
                iv.setImageResource(R.drawable.sunday_ticket)
            }
            GROUP_CHANNEL_KIND.NHL_CHANNEL -> {
                arrayChannelItem = CommonLists.NHLCHANNEL
                iv.setImageResource(R.drawable.nhl)
            }
            GROUP_CHANNEL_KIND.EPL_CHANNEL -> {
                arrayChannelItem = CommonLists.EPLCHANNEL
                iv.setImageResource(R.drawable.epl)
            }
            else -> {
                arrayChannelItem = CommonLists.PPVCHANNEL
                iv.setImageResource(R.drawable.ppv)
            }
        }
        channelAdapter = ChannelAdapter(this@TvGuideActivity, arrayChannelItem, "GROUP_CHANNEL")
        lvMenu.adapter = channelAdapter
        lvMenu.onItemClickListener =
            AdapterView.OnItemClickListener { parent: AdapterView<*>?, view: View?, position: Int, id: Long ->
                groupChannelDlg.dismiss()
                if (m_epgView == null) return@setOnItemClickListener
                streamUrl = Utils.makeStreamURL(
                    this@TvGuideActivity,
                    arrayChannelItem.get(position).m_sStreamID
                )
                val intent = Intent(this@TvGuideActivity, LivePlayerActivity::class.java)
                intent.putExtra(CommonLists.TAG_CHANNEL_INDEX, m_epgView!!.selectedChannelIndex)
                intent.putExtra(
                    CommonLists.TAG_CHANNEL_STREAM_URL,
                    Utils.makeStreamURL(
                        this@TvGuideActivity,
                        arrayChannelItem.get(position).m_sStreamID
                    )
                )
                startActivityForResult(intent, CommonLists.REQUEST_LIVE_CODE)
            }
        groupChannelDlg.show()
    }*/

    /*private fun sortChannels() {
        m_epgView!!.clearSelection()
        val bIsOrderByNumber: Boolean = java.lang.Boolean.valueOf(
            Utils.getSharePreferenceValue(
                this@TvGuideActivity,
                Constants.ORDER_BY_NUMBER,
                true.toString()
            )
        )
        Utils.getUtils().setString(
            Constants.ORDER_BY_NUMBER,
            bIsOrderByNumber.toString()
        )
        Collections.sort(
            CommonLists.EPG_FILTERED_DATA,
            ChannelComparator()
        )
        m_epgView!!.recalculateAndRedraw(false)
    }*/

    /*private fun showContextMenu() {
        val inflater =
            getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        @SuppressLint("InflateParams") val menuView: View =
            inflater.inflate(R.layout.context_menu_dialog, null, false)
        val categoryDlg = Dialog(this, R.style.Theme_CustomDialog)
        categoryDlg.setContentView(menuView)
        val lvMenu =
            menuView.findViewById<ListView>(R.id.ID_CONTEXT_MENU)
        val contextList = ArrayList<String>()
        var isReminded = -1
        var bHasRemindOption = false
        var selectedChannelItem: ChannelItem? = null
        if (m_epgView!!.selectedChannelIndex > -1) {
            selectedChannelItem =
                CommonLists.EPG_FILTERED_DATA.get(m_epgView!!.selectedChannelIndex)
            if (CommonLists.FAVORITE_CHANNEL_ARRAY.contains(selectedChannelItem.m_sStreamID)) contextList.add(
                getString(R.string.remove_from_favorite)
            ) else contextList.add(getString(R.string.add_to_favorite))
            val itemTopic: ItemTopic? =
                selectedChannelItem.m_arrItemTopic[m_epgView!!.selectedTopicIndex]
            for (i in 0 until CommonLists.REMINDER_TOPIC_ARRAY.size()) {
                val reminderItem: ItemTopic = CommonLists.REMINDER_TOPIC_ARRAY.get(i)
                if (reminderItem.IsSameWith(itemTopic)) {
                    isReminded = i
                    break
                }
            }
            if (isReminded != -1) {
                contextList.add(getString(R.string.remove_from_reminder))
                bHasRemindOption = true
            } else {
                if (itemTopic.m_dateTopicStart != null && itemTopic.m_dateTopicStart.after(Utils.CurrentTime())) {
                    contextList.add(getString(R.string.add_to_reminder))
                    bHasRemindOption = true
                }
            }
        }
        val channelItem = selectedChannelItem
        val bHasRemind = bHasRemindOption
        var isWatchFromBegin = false
        if (channelItem != null && channelItem.m_sTVArchive == "1") {
            val itemTopic: ItemTopic? = channelItem.m_arrItemTopic[m_epgView!!.selectedTopicIndex]
            if (itemTopic.m_dateTopicStart != null && itemTopic.m_dateTopicStart.before(Utils.CurrentTime())) {
                contextList.add(getString(R.string.watch_from_begining))
                isWatchFromBegin = true
            }
        }
        val bIsWatchFromBegin = isWatchFromBegin
        contextList.add(getString(R.string.categories))
        contextList.add(getString(R.string.enlarge))
        contextList.add(getString(R.string.multiview))
        if (java.lang.Boolean.parseBoolean(
                Utils.getSharePreferenceValue(
                    this@TvGuideActivity,
                    CommonLists.ORDER_BY_NUMBER,
                    true.toString()
                )
            )
        ) {
            contextList.add(getString(R.string.order_by_channel_name))
        } else {
            contextList.add(getString(R.string.order_by_number))
        }
        val reportAdapter = MainMenuAdapter(this, contextList)
        lvMenu.adapter = reportAdapter
        val bReminded = isReminded
        lvMenu.onItemClickListener =
            AdapterView.OnItemClickListener { parent: AdapterView<*>?, view: View?, position: Int, id: Long ->
                if (channelItem == null) {
                    when (position) {
                        0 -> showCategoryDialog()
                        1 -> m_epgView!!.updateEnlargeOption()
                        2 -> {
                        }
                        3 -> showMultiViewActivity()
                        4 -> sortChannels()
                    }
                } else {
                    if (bHasRemind) {
                        when (position) {
                            0 -> if (CommonLists.FAVORITE_CHANNEL_ARRAY.contains(channelItem.m_sStreamID)) CommonLists.FAVORITE_CHANNEL_ARRAY.remove(
                                channelItem.m_sStreamID
                            ) else CommonLists.FAVORITE_CHANNEL_ARRAY.add(channelItem.m_sStreamID)
                            1 -> if (bReminded != -1) CommonLists.REMINDER_TOPIC_ARRAY.remove(
                                bReminded
                            ) else CommonLists.REMINDER_TOPIC_ARRAY.add(
                                channelItem.m_arrItemTopic[m_epgView!!.selectedTopicIndex]
                            )
                            else -> {
                            }
                        }
                        if (bIsWatchFromBegin) {
                            when (position) {
                                2 -> {
                                    bIsStartFromBeginning = true
                                    StartActivityFullScreen()
                                }
                                3 -> showCategoryDialog()
                                4 -> m_epgView!!.updateEnlargeOption()
                                5 -> showMultiViewActivity()
                                6 -> sortChannels()
                                else -> {
                                }
                            }
                        } else {
                            when (position) {
                                2 -> showCategoryDialog()
                                3 -> m_epgView!!.updateEnlargeOption()
                                4 -> showMultiViewActivity()
                                5 -> sortChannels()
                                else -> {
                                }
                            }
                        }
                    } else {
                        if (position == 0) {
                            if (CommonLists.FAVORITE_CHANNEL_ARRAY.contains(channelItem.m_sStreamID)) CommonLists.FAVORITE_CHANNEL_ARRAY.remove(
                                channelItem.m_sStreamID
                            ) else CommonLists.FAVORITE_CHANNEL_ARRAY.add(channelItem.m_sStreamID)
                        } else {
                            if (bIsWatchFromBegin) {
                                when (position) {
                                    1 -> {
                                        bIsStartFromBeginning = true
                                        StartActivityFullScreen()
                                    }
                                    2 -> showCategoryDialog()
                                    3 -> m_epgView!!.updateEnlargeOption()
                                    4 -> showMultiViewActivity()
                                    5 -> sortChannels()
                                    else -> {
                                    }
                                }
                            } else {
                                when (position) {
                                    1 -> showCategoryDialog()
                                    2 -> m_epgView!!.updateEnlargeOption()
                                    3 -> showMultiViewActivity()
                                    4 -> sortChannels()
                                    else -> {
                                    }
                                }
                            }
                        }
                    }
                }
                categoryDlg.dismiss()
            }
        categoryDlg.show()
    }*/

    /*private fun showMultiViewActivity() {
        val intent = Intent(this@TvGuideActivity, MultiViewActivity::class.java)
        intent.putExtra("Category", m_currentCategory)
        startActivity(intent)
    }*/

    override fun initControl() {
        super.initControl()
        m_tvChannelNumber = findViewById(R.id.ID_CURRENT_CHANNEL_NUMBER)
        m_tvTopicName = findViewById(R.id.ID_CURRENT_TOPIC_NAME)
        m_tvTopicDateTime = findViewById(R.id.ID_CURRENT_TOPIC_DATE_TIME)
        m_tvTopicDescription = findViewById(R.id.ID_CURRENT_TOPIC_DESCRIPTION)
        m_tvCurrentTime = findViewById(R.id.ID_CURRENT_TIME)
        m_tvCurrentDate = findViewById(R.id.ID_CURRENT_DATE)
        m_tvExpirationDate = findViewById(R.id.ID_EXPIRATION_DATE)
        m_epgView = findViewById(R.id.epg)
        m_rlBottomMenu = findViewById(R.id.ID_RL_BOTTOM_MENU)
        progressBar = findViewById(R.id.pbProgress)
        playerView = findViewById(R.id.ID_HEAD_VIDEO_VIEW)
        m_bEnableKeyEvent = true
    }

    @SuppressLint("SimpleDateFormat")
    private fun setTimeInfo() {
        var dateFormat: SimpleDateFormat
        val calendar = Calendar.getInstance()
        dateFormat = SimpleDateFormat("hh:mm a")
        val mStrTime = dateFormat.format(calendar.time)
        dateFormat = SimpleDateFormat("dd/MM")
        var mStrDate = dateFormat.format(calendar.time)
        val nWeek = calendar[Calendar.DAY_OF_WEEK]
        if (nWeek == 1) {
            mStrDate = "Sun $mStrDate"
        } else if (nWeek == 2) {
            mStrDate = "Mon $mStrDate"
        } else if (nWeek == 3) {
            mStrDate = "Tue $mStrDate"
        } else if (nWeek == 4) {
            mStrDate = "Wed $mStrDate"
        } else if (nWeek == 5) {
            mStrDate = "Thu $mStrDate"
        } else if (nWeek == 6) {
            mStrDate = "Fri $mStrDate"
        } else if (nWeek == 7) {
            mStrDate = "Sat $mStrDate"
        }
        m_tvCurrentTime!!.text = mStrTime
        m_tvCurrentDate!!.text = mStrDate
    }

    private fun SetKeyListener() {
        // Key Press
        val mListenerKey= object: View.OnKeyListener{
            override fun onKey(p0: View?, p1: Int, keyEvent: KeyEvent?): Boolean {
                if (!m_bEnableKeyEvent) {
                    return true
                }

                val dateSelectedTopicStart = Date()
                dateSelectedTopicStart.time = 0
                when(keyEvent?.keyCode) {
                    KeyEvent.KEYCODE_BACK -> {
                        if (keyEvent.action == KeyEvent.ACTION_DOWN && keyEvent.eventTime - keyEvent.downTime > 1000) {
                            m_resetTimelineHandler.post(runnableResetTimeLine)
                            return true
                        }
                        if (keyEvent.action == KeyEvent.ACTION_UP && keyEvent.eventTime - keyEvent.downTime < 1000) {
                            if (m_epgView!!.selectedChannelIndex > -1) CommonLists.EPG_FILTERED_DATA.get(
                                m_epgView!!.selectedChannelIndex
                            )?.m_arrItemTopic!!.get(m_epgView!!.selectedTopicIndex)?.m_bIsSelected =
                                false
                            finish()
                        }
                        return true
                    }
                    KeyEvent.KEYCODE_ENTER, KeyEvent.KEYCODE_DPAD_CENTER -> {
                        if (keyEvent.isLongPress && keyEvent.action == KeyEvent.ACTION_DOWN) {
                            //showContextMenu()
                            return true
                        }
                        if (keyEvent.isLongPress || keyEvent.action != KeyEvent.ACTION_UP) return true
                        if (m_epgView!!.selectedChannelIndex < 0) return true
                        /*val channelItem: ChannelItem =
                            CommonLists.EPG_FILTERED_DATA.get(m_epgView!!.selectedChannelIndex)!!
                        if (CommonLists.MLBCHANNEL.contains(channelItem)) {
                            showGroupChannelMenu(GROUP_CHANNEL_KIND.MLB_CHANNEL)
                        } else if (CommonLists.NBACHANNEL.contains(channelItem)) {
                            showGroupChannelMenu(GROUP_CHANNEL_KIND.NBA_CHANNEL)
                        } else if (CommonLists.NFLCHANNEL.contains(channelItem)) {
                            showGroupChannelMenu(GROUP_CHANNEL_KIND.NFL_CHANNEL)
                        } else if (CommonLists.NHLCHANNEL.contains(channelItem)) {
                            showGroupChannelMenu(GROUP_CHANNEL_KIND.NHL_CHANNEL)
                        } else if (CommonLists.PPVCHANNEL.contains(channelItem)) {
                            showGroupChannelMenu(GROUP_CHANNEL_KIND.PPV_CHANNEL)
                        } else if (CommonLists.EPLCHANNEL.contains(channelItem)) {
                            showGroupChannelMenu(GROUP_CHANNEL_KIND.EPL_CHANNEL)
                        } else StartActivityFullScreen()
                        return true*/
                    }
                    KeyEvent.KEYCODE_DPAD_UP, KeyEvent.KEYCODE_DPAD_DOWN, KeyEvent.KEYCODE_DPAD_LEFT, KeyEvent.KEYCODE_DPAD_RIGHT, KeyEvent.KEYCODE_MEDIA_NEXT, KeyEvent.KEYCODE_MEDIA_PREVIOUS -> {
                        if (keyEvent.action != KeyEvent.ACTION_DOWN) return true
                        if (CommonLists.EPG_FILTERED_DATA.size == 0) return true
                        m_epgView!!.processKeyEvent(keyEvent.keyCode, keyEvent)
                        updateSelectedTopicInfo()
                        return true
                    }
                    KeyEvent.KEYCODE_MENU -> {
                        if (keyEvent.action != KeyEvent.ACTION_DOWN) return true
                        //showContextMenu()
                    }
                    KeyEvent.KEYCODE_0, KeyEvent.KEYCODE_1, KeyEvent.KEYCODE_2, KeyEvent.KEYCODE_3, KeyEvent.KEYCODE_4, KeyEvent.KEYCODE_5, KeyEvent.KEYCODE_6, KeyEvent.KEYCODE_7, KeyEvent.KEYCODE_8, KeyEvent.KEYCODE_9 -> {
                        if (keyEvent.action != KeyEvent.ACTION_UP) return true
                        handler.removeCallbacks(runnable)
                        channelNumber += (keyEvent.keyCode - KeyEvent.KEYCODE_0).toString()
                        m_tvChannelNumber!!.text = channelNumber
                        handler.postDelayed(runnable, 1000)
                        return true
                    }
                    else -> {
                    }
                }


                return false
            }

        }
        // Set Focus to Content View, if else, Key Event won't be accepted at once
        findViewById<View>(R.id.activity_guide).rootView.clearFocus()
        findViewById<View>(R.id.activity_guide).isFocusable = true
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        findViewById<View>(R.id.activity_guide).setOnKeyListener(mListenerKey)
    }

    //------------------------------------------------------------------------------
    private fun StartActivityFullScreen() {
        if (m_epgView == null) return
        val selectedIndex = m_epgView!!.selectedChannelIndex
        if (selectedIndex > -1) {
            val channelItem: ChannelItem =
                CommonLists.EPG_FILTERED_DATA.get(m_epgView!!.selectedChannelIndex)!!
            streamUrl = Utils.getUtils().makeStreamURL(this, channelItem.m_sStreamID.toString())
            /*val intent = Intent(this, LivePlayerActivity::class.java)
            intent.putExtra(CommonLists.TAG_CHANNEL_INDEX, selectedIndex)
            if (bIsStartFromBeginning) intent.putExtra(
                CommonLists.TAG_WATCH_FROM_BEGINNING,
                m_epgView!!.selectedTopicIndex
            ) else intent.putExtra(CommonLists.TAG_WATCH_FROM_BEGINNING, -1)
            startActivityForResult(intent, CommonLists.REQUEST_LIVE_CODE)
            bIsStartFromBeginning = false*/
        }
    }

    //------------------------------------------------------------------------------
    private fun updateSelectedTopicInfo() {
        if (m_epgView == null || CommonLists.EPG_FILTERED_DATA.size == 0 || m_epgView!!.selectedChannelIndex < 0) return
        val selectedChannel: ChannelItem? =
            CommonLists.EPG_FILTERED_DATA.get(m_epgView!!.selectedChannelIndex)
        if (m_epgView!!.selectedTopicIndex < 0) return
        m_tvChannelNumber!!.text = selectedChannel?.m_sTvNum
        val selectedTopic: ItemTopic? =
            selectedChannel?.m_arrItemTopic?.get(m_epgView!!.selectedTopicIndex)
        m_tvTopicName!!.setText(selectedTopic?.m_sTitle)
        if (selectedTopic!!.m_dateTopicStart != null && selectedTopic.m_dateTopicEnd != null) {
            val dayOfTheWeek = DateFormat.format(
                "EEEE",
                selectedTopic!!.m_dateTopicStart
            ) as String // Thursday
            val day = DateFormat.format(
                "dd",
                selectedTopic!!.m_dateTopicStart
            ) as String // 20
            val monthString = DateFormat.format(
                "MMM",
                selectedTopic.m_dateTopicStart
            ) as String // Jun
            @SuppressLint("SimpleDateFormat") val sdfs =
                SimpleDateFormat("hh:mm a")
            val duration =
                day + " " + monthString + ", " + dayOfTheWeek + " " + sdfs.format(selectedTopic.m_dateTopicStart) + " - " + sdfs.format(
                    selectedTopic!!.m_dateTopicEnd
                )
            m_tvTopicDateTime!!.text = duration
        } else m_tvTopicDateTime!!.text = ""
        m_tvTopicDescription?.setText(selectedTopic?.m_sDescription)
    }

    inner class ChannelComparator : Comparator<ChannelItem> {
        override fun compare(left: ChannelItem, right: ChannelItem): Int {
            return if (java.lang.Boolean.parseBoolean(
                    Utils.getUtils().setString(
                        Constants.ORDER_BY_NUMBER,
                        true.toString()
                    ).toString()
                )
            ) {
                if (left.m_sTvNum!!.isEmpty()) -1 else if (right.m_sTvNum!!.isEmpty()) 1 else Integer.compare(
                    left.m_sTvNum!!.toInt(),
                    right.m_sTvNum!!.toInt()
                )
            } else {
                left.m_sTvName!!.compareTo(right.m_sTvName!!)
            }
        }
    }
}