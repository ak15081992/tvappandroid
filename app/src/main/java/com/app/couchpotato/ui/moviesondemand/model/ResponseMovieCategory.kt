package com.app.couchpotato.ui.moviesondemand.model

import android.os.Parcelable
import com.app.couchpotato.helperutils.Constants
import com.app.couchpotato.helperutils.Utils
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResponseMovieCategory(

	@field:SerializedName("category_name")
	val categoryName: String? = null,

	@field:SerializedName("category_id")
	val categoryId: String? = null,

	@field:SerializedName("parent_id")
	val parentId: Int? = null,

	@field:SerializedName("movies")
	val movies: ArrayList<ResponseMovies>? = ArrayList()

): Parcelable {
	companion object {

		/**
		 * Utils
		 */
		val mUtils= Utils.getUtils()

		var mInstance: ArrayList<ResponseMovieCategory>? = null

		fun getResponseMovieCategoryList() : ArrayList<ResponseMovieCategory> {
			return if (mInstance != null) {
				mInstance as ArrayList<ResponseMovieCategory>
			} else {
				if (mUtils.getString(Constants.KEY_MOVIE_ON_DEMAND_CATEGORY_LIST).toString()=="null") {
					mInstance =
						ArrayList()
					mInstance!!
				} else {
					mInstance = Gson().fromJson<ArrayList<ResponseMovieCategory>>(
						mUtils.getString(Constants.KEY_MOVIE_ON_DEMAND_CATEGORY_LIST),
						ResponseMovieCategory::class.java)
					return mInstance!!
				}
			}
		}

		fun saveMoveCategoryList(mMoviesCategoryList: ArrayList<ResponseMovieCategory>) {
			mInstance =mMoviesCategoryList
			mUtils.setString(Constants.KEY_MOVIE_ON_DEMAND_CATEGORY_LIST, Gson().toJson(mMoviesCategoryList))
		}
	}
}
