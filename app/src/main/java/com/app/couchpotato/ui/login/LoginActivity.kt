package com.app.couchpotato.ui.login

import android.app.Activity
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.app.couchpotato.MyApp
import com.app.couchpotato.R
import com.app.couchpotato.databinding.ActivityLoginBinding
import com.app.couchpotato.ui.login.presenter.LoginEventHandler
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    /**
     * Binding
     */
    var mBinding: ActivityLoginBinding?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding=DataBindingUtil.setContentView(
            this,
            R.layout.activity_login
        )
        mBinding?.mLoginEventHandler= LoginEventHandler(this)

        initUI()
    }

    private fun initUI() {
        etUsername.typeface=MyApp.fontMontserratMedium
        etPassword.typeface=MyApp.fontMontserratMedium
        etUsername.requestFocus()
    }
}
