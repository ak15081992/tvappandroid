package com.app.couchpotato.ui.demanddetail

import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.app.couchpotato.R
import com.app.couchpotato.commonmodel.UserModel
import com.app.couchpotato.dagger.module.ModuleApiInterface
import com.app.couchpotato.databinding.ActivityDemandDetailBinding
import com.app.couchpotato.helperutils.Constants
import com.app.couchpotato.ui.demanddetail.model.ResponseVideoDetail
import com.app.couchpotato.ui.demanddetail.presenter.DemandDetailEventHandler
import com.app.couchpotato.ui.vlcmediaplayer.VlcMediaPlayerActivity
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_demand_detail.*

class DemandDetailActivity : AppCompatActivity() {

    lateinit var mBinding: ActivityDemandDetailBinding

    /**
     * Response Demand Detail
     */
    var mResponseDemandDetail: Int?=null

    lateinit var streamUrl: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding=DataBindingUtil.setContentView(
            this,
            R.layout.activity_demand_detail
        )
        mBinding.mDemandDetailEventHandler= DemandDetailEventHandler(this)

        setKeyListener()
    }

    override fun onStart() {
        super.onStart()
        if (mResponseDemandDetail==null) {
            mBinding.mDemandDetailEventHandler?.getDemandDetailsAPIHandler()
        }
    }

    fun setKeyListener() {
        /*val mListenKey=View.OnKeyListener { view, keyCode, keyEvent ->
            when (keyEvent.keyCode) {
                KeyEvent.KEYCODE_BACK -> {
                    if (keyEvent.action != KeyEvent.ACTION_UP) true
                    finish()
                }

            }

            false
        }*/
        val m_listenerKey =
            View.OnKeyListener { v: View?, keyCode: Int, keyEvent: KeyEvent ->
                when (keyEvent.keyCode) {
                    KeyEvent.KEYCODE_BACK -> {
                        if (keyEvent.action != KeyEvent.ACTION_UP) true
                        finish()
                    }
                    KeyEvent.KEYCODE_ENTER, KeyEvent.KEYCODE_DPAD_CENTER -> {
                        if (keyEvent.action != KeyEvent.ACTION_UP) true
                        if (streamUrl.isEmpty()) {
                            Toast.makeText(
                                baseContext,
                                "Can not get stream url",
                                Toast.LENGTH_SHORT
                            ).show()
                            true
                        }
                        val intent =
                            Intent(this@DemandDetailActivity, VlcMediaPlayerActivity::class.java)
                        intent.putExtra("streamUrl", streamUrl)
                        intent.putExtra("videoName", txt_name.text.toString())
                        intent.putExtra("videoIndex", intent.getStringExtra(Constants.KEY_VIDEOINDEX))
                        startActivity(intent)
                    }
                    KeyEvent.KEYCODE_MENU -> if (keyEvent.action != KeyEvent.ACTION_DOWN) true
                    else -> {
                    }
                }
                false
            }

        // Set Focus to Content View, if else, Key Event won't be accepted at once
        activity_demanddetail.rootView.clearFocus()
        activity_demanddetail.isFocusable = true
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        activity_demanddetail.setOnKeyListener(m_listenerKey)
    }

    fun updateUI(body: ResponseVideoDetail?) {
        streamUrl= ModuleApiInterface.BASE_URL + "movie/" + UserModel.getUserModel().userInfo?.username + "/" + UserModel.getUserModel().userInfo?.password + "/" + body?.movieData?.streamId + "." + body?.movieData?.containerExtension
        txt_name.text=body?.info?.name
        txt_description.text=body?.info?.description
        txt_rdate.text=body?.info?.releasedate
        txt_country.text=body?.info?.country
        Glide.with(this).load(body?.info?.coverBig).into(img_sview)
        Glide.with(this).load(body?.info?.backdropPath?.get(0)).centerCrop().into(img_back)
    }
}
