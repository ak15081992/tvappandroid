package com.app.couchpotato.ui.dashboard

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.app.couchpotato.R
import com.app.couchpotato.commonmodel.UserModel
import com.app.couchpotato.databinding.ActivityDashboardBinding
import com.app.couchpotato.ui.dashboard.presenter.DashboardEventHandler
import com.app.couchpotato.ui.moviesondemand.model.ResponseMovieCategory
import com.app.couchpotato.ui.moviesondemand.model.ResponseMovies
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_dashboard.*

class DashboardActivity : AppCompatActivity() {

    /**
     * Binding
     */
    var mBinding: ActivityDashboardBinding?=null

    /**
     * Get first new release movie to show on dashboard as banner
     */
    var mLatestMovie: ResponseMovies?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding=DataBindingUtil.setContentView(
            this,
            R.layout.activity_dashboard
        )
        mBinding?.mDashboardActivity= DashboardEventHandler(this)

      //  initUI()
    }

    private fun initUI() {
        llLiveTV.requestFocus()
        if (UserModel.getUserModel().userInfo?.expDate.toString().isEmpty()) {
            tvExpirationDate.text=resources.getString(R.string.unlimited)
        } else {
            tvExpirationDate.text=mBinding?.mDashboardActivity?.mUtils?.convertTime(UserModel.getUserModel().userInfo?.expDate?.toLong()!!)
        }
        for (items in ResponseMovieCategory.getResponseMovieCategoryList()) {
            if (items.categoryName.equals("**New Release**")) {
                mLatestMovie= items.movies?.get(0)!!
            }
        }
        Glide.with(this)
            .load(mLatestMovie?.streamIcon)
            .centerCrop()
            .into(ivLatestMovie)
    }

}
