package com.app.couchpotato.ui.moviesondemand.presenter

import android.view.View
import com.app.couchpotato.MyApp
import com.app.couchpotato.customview.loader.LoadingDialog
import com.app.couchpotato.helperutils.Utils
import com.app.couchpotato.ui.moviesondemand.MoviesOnDemandActivity

class MoviesOnDemandEventHandler(private val mActivity: MoviesOnDemandActivity) {
    /**
     * Utils
     */
    val mUtils= Utils.getUtils()

    /**
     * Assinging LoadingDialog's singleton object into @mProgressLoader
     */
    private val mProgressLoader = LoadingDialog.getLoader()

    /**
     * AppComponent of Dagger
     */
    var mAppComponent= MyApp.get(mActivity).getAppComponents()

    fun onWatchNowClicked(view: View) {

    }
}