package com.app.couchpotato.ui.moviesondemand.model

import android.os.Parcelable
import com.app.couchpotato.helperutils.Constants
import com.app.couchpotato.helperutils.Utils
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResponseMovies(

	@field:SerializedName("stream_icon")
	val streamIcon: String? = null,

	@field:SerializedName("category_id")
	val categoryId: String? = null,

	@field:SerializedName("stream_id")
	val streamId: Int? = null,

	@field:SerializedName("added")
	val added: String? = null,

	@field:SerializedName("num")
	val num: Int? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("stream_type")
	val streamType: String? = null,

	@field:SerializedName("rating")
	val rating: String? = null,

	@field:SerializedName("rating_5based")
	val rating5based: Float? = null,

	@field:SerializedName("direct_source")
	val directSource: String? = null,

	@field:SerializedName("custom_sid")
	val customSid: String? = null,

	@field:SerializedName("container_extension")
	val containerExtension: String? = null
): Parcelable {
	companion object {

		/**
		 * Utils
		 */
		val mUtils= Utils.getUtils()

		var mInstance: ArrayList<ResponseMovies>? = null

		fun getResponseMovieCategoryList() : ArrayList<ResponseMovies> {
			return if (mInstance != null) {
				mInstance as ArrayList<ResponseMovies>
			} else {
				if (mUtils.getString(Constants.KEY_MOVIE_ON_DEMAND_CATEGORY_LIST).toString()=="null") {
					mInstance =
						ArrayList()
					mInstance!!
				} else {
					mInstance = Gson().fromJson<ArrayList<ResponseMovies>>(
						mUtils.getString(Constants.KEY_MOVIE_ON_DEMAND_CATEGORY_LIST),
						ResponseMovies::class.java)
					return mInstance!!
				}
			}
		}

		fun saveMoveCategoryList(mMoviesList: ArrayList<ResponseMovies>) {
			mInstance =mMoviesList
			mUtils.setString(Constants.KEY_MOVIE_ON_DEMAND_CATEGORY_LIST, Gson().toJson(mMoviesList))
		}
	}
}
