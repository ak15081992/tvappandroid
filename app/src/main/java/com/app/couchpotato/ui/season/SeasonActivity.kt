package com.app.couchpotato.ui.season

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.couchpotato.R
import com.app.couchpotato.customview.recycleritemdecorator.LinearRecyclerItemDecorator
import com.app.couchpotato.databinding.ActivitySeasonBinding
import com.app.couchpotato.helperlisteners.RecyclerItemClickListener
import com.app.couchpotato.helperutils.Constants
import com.app.couchpotato.ui.episode.EpisodeActivity
import com.app.couchpotato.ui.season.adapter.SeasonAdapter
import com.app.couchpotato.ui.season.model.ResponseSeasonList
import com.app.couchpotato.ui.season.presenter.SeasonEventHandler
import com.app.couchpotato.ui.tvondemand.model.ResponseTVSeries
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_season.*

class SeasonActivity : AppCompatActivity() {

    /**
     * Binding
     */
    var mBinding: ActivitySeasonBinding?=null

    /**
     * Tv Series
     */
    var mTVSeries: ResponseTVSeries?=null

    /**
     * Response Seasons List
     */
    var mResponseSeasonList: ResponseSeasonList?=null

    /**
     * Adapter
     */
    var mAdapter: SeasonAdapter?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding=DataBindingUtil.setContentView(
            this,
            R.layout.activity_season
        )
        mBinding?.mSeasonEventHandler=SeasonEventHandler(this)

        mTVSeries=intent.getParcelableExtra(Constants.KEY_TV_SERIES)

        initUI()
    }

    private fun initUI() {
        Glide.with(this)
            .load(mTVSeries?.backdropPath?.get(0))
            .into(ivCover)
        recyclerviewSeasons.layoutManager=GridLayoutManager(this,5)
        recyclerviewSeasons.addOnItemTouchListener(
            RecyclerItemClickListener(
                this,
                object: RecyclerItemClickListener.OnItemClickListener{
                    override fun onItemClick(view: View, position: Int) {
                        startActivity(
                            Intent(this@SeasonActivity,EpisodeActivity::class.java)
                                .putExtra(Constants.KEY_SERIES_ID,mTVSeries?.seriesId.toString())
                                .putExtra(Constants.KEY_SEASON_NO,mResponseSeasonList?.seasons?.get(position)?.seasonNumber.toString())
                        )
                    }
                }
            )
        )
    }

    override fun onStart() {
        super.onStart()
        if (mResponseSeasonList==null) {
            mBinding?.mSeasonEventHandler?.getSeasonListAPIImpl()
        }
    }

    fun updateUI() {
        tvSeriesName.text=mTVSeries?.name
        tvSeriesName.visibility=View.VISIBLE
        mAdapter= SeasonAdapter(this,mResponseSeasonList?.seasons)
        recyclerviewSeasons.adapter=mAdapter
    }
}
