package com.app.couchpotato.ui.catchuptv

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.app.couchpotato.R
import com.app.couchpotato.databinding.ActivityCatchUpTvBinding
import com.app.couchpotato.ui.catchuptv.presenter.CatchUpTvEventHandler

class CatchUpTvActivity : AppCompatActivity() {

    /**
     * Binding
     */
    var mBinding: ActivityCatchUpTvBinding?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding=DataBindingUtil.setContentView(
            this,
            R.layout.activity_catch_up_tv
        )
        mBinding?.mCatchUpTvEventHandler= CatchUpTvEventHandler(this)
    }
}
