package com.app.couchpotato.ui.season.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResponseSeasonList(

	@field:SerializedName("seasons")
	val seasons: ArrayList<SeasonsItem?>? = null,

	@field:SerializedName("episodes")
	val episodes: Episodes? = null,

	@field:SerializedName("info")
	val info: Info? = null
): Parcelable

@Parcelize
data class Tags(

	@field:SerializedName("BPS-eng")
	val bPSEng: String? = null,

	@field:SerializedName("_STATISTICS_WRITING_APP-eng")
	val sTATISTICSWRITINGAPPEng: String? = null,

	@field:SerializedName("_STATISTICS_TAGS-eng")
	val sTATISTICSTAGSEng: String? = null,

	@field:SerializedName("language")
	val language: String? = null,

	@field:SerializedName("DURATION-eng")
	val dURATIONEng: String? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("NUMBER_OF_FRAMES-eng")
	val nUMBEROFFRAMESEng: String? = null,

	@field:SerializedName("_STATISTICS_WRITING_DATE_UTC-eng")
	val sTATISTICSWRITINGDATEUTCEng: String? = null,

	@field:SerializedName("NUMBER_OF_BYTES-eng")
	val nUMBEROFBYTESEng: String? = null
): Parcelable


@Parcelize
data class Episode(

	@field:SerializedName("added")
	val added: String? = null,

	@field:SerializedName("season")
	val season: Int? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("episode_num")
	val episodeNum: Int? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("direct_source")
	val directSource: String? = null,

	@field:SerializedName("custom_sid")
	val customSid: String? = null,

	@field:SerializedName("container_extension")
	val containerExtension: String? = null,

	@field:SerializedName("info")
	val info: Info? = null
): Parcelable

@Parcelize
data class Episodes(

	@field:SerializedName("1")
	val jsonMember1: List<Episode?>? = null

): Parcelable

@Parcelize
data class Audio(

	@field:SerializedName("time_base")
	val timeBase: String? = null,

	@field:SerializedName("loro_surmixlev")
	val loroSurmixlev: String? = null,

	@field:SerializedName("r_frame_rate")
	val rFrameRate: String? = null,

	@field:SerializedName("ltrt_surmixlev")
	val ltrtSurmixlev: String? = null,

	@field:SerializedName("start_pts")
	val startPts: Int? = null,

	@field:SerializedName("index")
	val index: Int? = null,

	@field:SerializedName("ltrt_cmixlev")
	val ltrtCmixlev: String? = null,

	@field:SerializedName("codec_name")
	val codecName: String? = null,

	@field:SerializedName("tags")
	val tags: Tags? = null,

	@field:SerializedName("loro_cmixlev")
	val loroCmixlev: String? = null,

	@field:SerializedName("start_time")
	val startTime: String? = null,

	@field:SerializedName("disposition")
	val disposition: Disposition? = null,

	@field:SerializedName("codec_tag")
	val codecTag: String? = null,

	@field:SerializedName("sample_rate")
	val sampleRate: String? = null,

	@field:SerializedName("channels")
	val channels: Int? = null,

	@field:SerializedName("sample_fmt")
	val sampleFmt: String? = null,

	@field:SerializedName("dmix_mode")
	val dmixMode: String? = null,

	@field:SerializedName("codec_time_base")
	val codecTimeBase: String? = null,

	@field:SerializedName("codec_tag_string")
	val codecTagString: String? = null,

	@field:SerializedName("bits_per_sample")
	val bitsPerSample: Int? = null,

	@field:SerializedName("avg_frame_rate")
	val avgFrameRate: String? = null,

	@field:SerializedName("codec_type")
	val codecType: String? = null,

	@field:SerializedName("codec_long_name")
	val codecLongName: String? = null
): Parcelable

@Parcelize
data class Info(

	@field:SerializedName("youtube_trailer")
	val youtubeTrailer: String? = null,

	@field:SerializedName("releaseDate")
	val releaseDate: String? = null,

	@field:SerializedName("director")
	val director: String? = null,

	@field:SerializedName("rating")
	val rating: Float? = null,

	@field:SerializedName("cover")
	val cover: String? = null,

	@field:SerializedName("backdrop_path")
	val backdropPath: List<String?>? = null,

	@field:SerializedName("cast")
	val cast: String? = null,

	@field:SerializedName("category_id")
	val categoryId: String? = null,

	@field:SerializedName("plot")
	val plot: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("genre")
	val genre: String? = null,

	@field:SerializedName("episode_run_time")
	val episodeRunTime: String? = null,

	@field:SerializedName("last_modified")
	val lastModified: String? = null,

	@field:SerializedName("rating_5based")
	val rating5based: Float? = null,

	@field:SerializedName("duration")
	val duration: String? = null,

	@field:SerializedName("tmdb_id")
	val tmdbId: Int? = null,

	@field:SerializedName("movie_image")
	val movieImage: String? = null,

	@field:SerializedName("season")
	val season: String? = null,

	@field:SerializedName("bitrate")
	val bitrate: Int? = null,

	@field:SerializedName("releasedate")
	val releasedate: String? = null,

	@field:SerializedName("video")
	val video: Video? = null,

	@field:SerializedName("audio")
	val audio: Audio? = null,

	@field:SerializedName("duration_secs")
	val durationSecs: Int? = null
): Parcelable

@Parcelize
data class SeasonsItem(

	@field:SerializedName("cover")
	val cover: String? = null,

	@field:SerializedName("air_date")
	val airDate: String? = null,

	@field:SerializedName("overview")
	val overview: String? = null,

	@field:SerializedName("episode_count")
	val episodeCount: Int? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("season_number")
	val seasonNumber: Int? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("cover_big")
	val coverBig: String? = null
): Parcelable

@Parcelize
data class Video(

	@field:SerializedName("pix_fmt")
	val pixFmt: String? = null,

	@field:SerializedName("r_frame_rate")
	val rFrameRate: String? = null,

	@field:SerializedName("start_pts")
	val startPts: Int? = null,

	@field:SerializedName("sample_aspect_ratio")
	val sampleAspectRatio: String? = null,

	@field:SerializedName("field_order")
	val fieldOrder: String? = null,

	@field:SerializedName("is_avc")
	val isAvc: String? = null,

	@field:SerializedName("codec_tag_string")
	val codecTagString: String? = null,

	@field:SerializedName("avg_frame_rate")
	val avgFrameRate: String? = null,

	@field:SerializedName("codec_long_name")
	val codecLongName: String? = null,

	@field:SerializedName("height")
	val height: Int? = null,

	@field:SerializedName("nal_length_size")
	val nalLengthSize: String? = null,

	@field:SerializedName("chroma_location")
	val chromaLocation: String? = null,

	@field:SerializedName("time_base")
	val timeBase: String? = null,

	@field:SerializedName("coded_height")
	val codedHeight: Int? = null,

	@field:SerializedName("level")
	val level: Int? = null,

	@field:SerializedName("profile")
	val profile: String? = null,

	@field:SerializedName("bits_per_raw_sample")
	val bitsPerRawSample: String? = null,

	@field:SerializedName("index")
	val index: Int? = null,

	@field:SerializedName("codec_name")
	val codecName: String? = null,

	@field:SerializedName("tags")
	val tags: Tags? = null,

	@field:SerializedName("start_time")
	val startTime: String? = null,

	@field:SerializedName("disposition")
	val disposition: Disposition? = null,

	@field:SerializedName("codec_tag")
	val codecTag: String? = null,

	@field:SerializedName("has_b_frames")
	val hasBFrames: Int? = null,

	@field:SerializedName("refs")
	val refs: Int? = null,

	@field:SerializedName("codec_time_base")
	val codecTimeBase: String? = null,

	@field:SerializedName("width")
	val width: Int? = null,

	@field:SerializedName("display_aspect_ratio")
	val displayAspectRatio: String? = null,

	@field:SerializedName("coded_width")
	val codedWidth: Int? = null,

	@field:SerializedName("codec_type")
	val codecType: String? = null
): Parcelable

@Parcelize
data class Disposition(

	@field:SerializedName("dub")
	val dub: Int? = null,

	@field:SerializedName("karaoke")
	val karaoke: Int? = null,

	@field:SerializedName("default")
	val jsonMemberDefault: Int? = null,

	@field:SerializedName("original")
	val original: Int? = null,

	@field:SerializedName("visual_impaired")
	val visualImpaired: Int? = null,

	@field:SerializedName("forced")
	val forced: Int? = null,

	@field:SerializedName("attached_pic")
	val attachedPic: Int? = null,

	@field:SerializedName("timed_thumbnails")
	val timedThumbnails: Int? = null,

	@field:SerializedName("comment")
	val comment: Int? = null,

	@field:SerializedName("hearing_impaired")
	val hearingImpaired: Int? = null,

	@field:SerializedName("lyrics")
	val lyrics: Int? = null,

	@field:SerializedName("clean_effects")
	val cleanEffects: Int? = null
): Parcelable
