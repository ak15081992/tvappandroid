package com.app.couchpotato.ui.episode.model;

import android.widget.ImageView;

public class EpisodeItem {
    public ImageView itrans;
    public String id;
    public int episode_num;
    public String title;
    public String container_extension;
    public String movie_image;
    public String plot;
    public String releasedate;
    public double rating;
    public String name;
    public String custom_sid;
    public String added;
    public int season;
    public String direct_source;

    public EpisodeItem(){

    }
}
