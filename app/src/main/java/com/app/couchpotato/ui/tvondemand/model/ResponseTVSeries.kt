package com.app.couchpotato.ui.tvondemand.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResponseTVSeries(

	@field:SerializedName("youtube_trailer")
	val youtubeTrailer: String? = null,

	@field:SerializedName("releaseDate")
	val releaseDate: String? = null,

	@field:SerializedName("director")
	val director: String? = null,

	@field:SerializedName("num")
	val num: Int? = null,

	@field:SerializedName("rating")
	val rating: String? = null,

	@field:SerializedName("series_id")
	val seriesId: Int? = null,

	@field:SerializedName("cover")
	val cover: String? = null,

	@field:SerializedName("backdrop_path")
	val backdropPath: List<String?>? = null,

	@field:SerializedName("cast")
	val cast: String? = null,

	@field:SerializedName("category_id")
	val categoryId: String? = null,

	@field:SerializedName("plot")
	val plot: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("genre")
	val genre: String? = null,

	@field:SerializedName("episode_run_time")
	val episodeRunTime: String? = null,

	@field:SerializedName("last_modified")
	val lastModified: String? = null,

	@field:SerializedName("rating_5based")
	val rating5based: Double? = null
): Parcelable
