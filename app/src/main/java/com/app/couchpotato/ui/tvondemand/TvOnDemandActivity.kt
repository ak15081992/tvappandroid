package com.app.couchpotato.ui.tvondemand

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.couchpotato.R
import com.app.couchpotato.customview.recycleritemdecorator.LinearRecyclerItemDecorator
import com.app.couchpotato.databinding.ActivityTvOnDemandBinding
import com.app.couchpotato.ui.tvondemand.model.ResponseTVSeries
import com.app.couchpotato.ui.tvondemand.model.ResponseTVSeriesCategoryList
import com.app.couchpotato.ui.tvondemand.presenter.TvOnDemandEventHandler
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_tv_on_demand.*
import com.app.couchpotato.ui.tvondemand.adapter.TvSeriesAdapter as TvSeriesAdapter

class TvOnDemandActivity : AppCompatActivity() {

    /**
     * Binding
     */
    var mBinding: ActivityTvOnDemandBinding? = null

    /**
     * Linear Layout Manager
     */
    lateinit var mLinearLayoutManager: LinearLayoutManager

    /**
     * Adapter
     */
    lateinit var mAdapter: TvSeriesAdapter

    /**
     * Resposne TV Series
     */
    lateinit var mLatestSeries: ResponseTVSeries

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(
            this,
            R.layout.activity_tv_on_demand
        )
        mBinding?.mTvOnDemandEventHandler= TvOnDemandEventHandler(this)

        initUI()
    }

    private fun initUI() {
        mLinearLayoutManager = LinearLayoutManager(this)
        recyclerviewSeries.layoutManager = mLinearLayoutManager
        recyclerviewSeries.addItemDecoration(
            LinearRecyclerItemDecorator(
                _top = resources.getDimension(R.dimen._1sdp).toInt(),
                _bottom = resources.getDimension(R.dimen._8sdp).toInt(),
                _start = resources.getDimension(R.dimen._1sdp).toInt(),
                _end = resources.getDimension(R.dimen._1sdp).toInt(),
                _extra = resources.getDimension(R.dimen._18sdp).toInt(),
                totalSize = 0,
                type = 0
            )
        )
        mBinding?.mTvOnDemandEventHandler?.getSeriesCategoryListAPIImpl()
    }

    fun updateRecycler() {
        Thread(Runnable {
            for (items in ResponseTVSeriesCategoryList.getResponseSERIESCategoryList()) {
                if (items.categoryName.equals("Netflix/Amazon Prime/ Apple Original Tv Shows")) {
                    mLatestSeries= items.series?.get(0)!!
                }
            }
            this.runOnUiThread {
                tvLatestSeries.text=mLatestSeries.name
                ratingBar.rating= mLatestSeries.rating5based?.toFloat()!!
                tvYearOfRelease.text= mLatestSeries.releaseDate?.split("-")?.get(0)
                Glide.with(this)
                    .load(mLatestSeries.cover)
                    .centerCrop()
                    .into(ivLatestSeries)
                llHeader.visibility=View.VISIBLE
            }
        }).start()
        mAdapter= TvSeriesAdapter(this,ResponseTVSeriesCategoryList.getResponseSERIESCategoryList())
        recyclerviewSeries.adapter=mAdapter
    }
}
