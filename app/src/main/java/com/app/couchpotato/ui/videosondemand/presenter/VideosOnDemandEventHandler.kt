package com.app.couchpotato.ui.videosondemand.presenter

import android.content.Intent
import android.view.View
import com.app.couchpotato.MyApp
import com.app.couchpotato.customview.loader.LoadingDialog
import com.app.couchpotato.helperutils.Utils
import com.app.couchpotato.ui.moviesondemand.MoviesOnDemandActivity
import com.app.couchpotato.ui.tvondemand.TvOnDemandActivity
import com.app.couchpotato.ui.videosondemand.VideosOnDemandActivity

class VideosOnDemandEventHandler(private val mActivity: VideosOnDemandActivity) {

    /**
     * Utils
     */
    val mUtils= Utils.getUtils()

    /**
     * Assinging LoadingDialog's singleton object into @mProgressLoader
     */
    private val mProgressLoader = LoadingDialog.getLoader()

    /**
     * AppComponent of Dagger
     */
    var mAppComponent= MyApp.get(mActivity).getAppComponents()

    fun onMoviesOnDemandClicked(view: View) {
        mUtils.fireActivityIntent(
            mActivity,
            Intent(mActivity, MoviesOnDemandActivity::class.java),
            isFinish = true,
            isForward = true
        )
    }
    fun onTvOnDemandClicked(view: View) {
        mUtils.fireActivityIntent(
            mActivity,
            Intent(mActivity,TvOnDemandActivity::class.java),
            isFinish = true,
            isForward = true
        )
    }
}