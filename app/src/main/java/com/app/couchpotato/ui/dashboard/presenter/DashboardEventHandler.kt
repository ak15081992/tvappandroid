package com.app.couchpotato.ui.dashboard.presenter

import android.content.Intent
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import com.app.couchpotato.MyApp
import com.app.couchpotato.R
import com.app.couchpotato.customview.loader.LoadingDialog
import com.app.couchpotato.helperutils.Utils
import com.app.couchpotato.ui.dashboard.DashboardActivity
import com.app.couchpotato.ui.guide.GuideActivity
import com.app.couchpotato.ui.login.LoginActivity
import com.app.couchpotato.ui.setting.SettingActivity
import com.app.couchpotato.ui.videosondemand.VideosOnDemandActivity
import kotlinx.android.synthetic.main.activity_dashboard.*


class DashboardEventHandler(private val mActivity: DashboardActivity) {

    /**
     * Utils
     */
    val mUtils = Utils.getUtils()

    /**
     * Assinging LoadingDialog's singleton object into @mProgressLoader
     */
    private val mProgressLoader = LoadingDialog.getLoader()

    /**
     * AppComponent of Dagger
     */
    var mAppComponent = MyApp.get(mActivity).getAppComponents()

    fun onLiveTvClicked(view: View) {
        mActivity.llMenuContainer.visibility = View.GONE
        mUtils.fireActivityIntent(
            mActivity,
            Intent(mActivity,GuideActivity::class.java),
            false,
            true
        )
    }

    fun onWatchNowClicked(view: View) {

    }

    fun onVideosOnDemandClicked(view: View) {
        mActivity.llMenuContainer.visibility = View.GONE
        mActivity.startActivity(
            Intent(mActivity, VideosOnDemandActivity::class.java)
        )
        mActivity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
    }

    fun onCatchUpTvClicked(view: View) {
        mActivity.llMenuContainer.visibility = View.GONE
    }

    fun onMenuClicked(view: View) {

        if (mActivity.llMenuContainer.visibility == View.VISIBLE) {
            val slideinleft: Animation = AnimationUtils.loadAnimation(mActivity, R.anim.exits_to_right)
            mActivity.llMenuContainer.visibility = View.GONE
            mActivity.llMenuContainer.startAnimation(slideinleft)
        } else {
            val slidefromleft: Animation = AnimationUtils.loadAnimation(mActivity, R.anim.enter_from_right)
            mActivity.llMenuContainer.visibility = View.VISIBLE
            mActivity.llMenuContainer.startAnimation(slidefromleft)
        }
    }

    fun onRefreshClicked(view: View) {
        mActivity.llMenuContainer.visibility = View.GONE
        mActivity.frameMenu.requestFocus()
    }

    fun onSettingClicked(view: View) {
        mActivity.llMenuContainer.visibility = View.GONE
        mActivity.frameMenu.requestFocus()
        mUtils.fireActivityIntent(
            mActivity,
            Intent(mActivity,SettingActivity::class.java),
            isFinish = false,
            isForward = true
        )
    }

    fun onLogoutClicked(view: View) {
        mUtils.logoutWork()
        mUtils.fireActivityIntent(
            mActivity,
            Intent(mActivity, LoginActivity::class.java)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK),
            isFinish = true,
            isForward = false
        )
    }
}