package com.app.couchpotato.ui.tvondemand.presenter

import com.app.couchpotato.MyApp
import com.app.couchpotato.R
import com.app.couchpotato.commonmodel.UserModel
import com.app.couchpotato.customview.loader.LoadingDialog
import com.app.couchpotato.helperutils.Utils
import com.app.couchpotato.ui.tvondemand.TvOnDemandActivity
import com.app.couchpotato.ui.tvondemand.model.ResponseTVSeries
import com.app.couchpotato.ui.tvondemand.model.ResponseTVSeriesCategoryList
import com.tapadoo.alerter.Alerter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TvOnDemandEventHandler(private val mActivity: TvOnDemandActivity) {

    /**
     * Utils
     */
    val mUtils= Utils.getUtils()

    /**
     * Assinging LoadingDialog's singleton object into @mProgressLoader
     */
    private val mProgressLoader = LoadingDialog.getLoader()

    /**
     * AppComponent of Dagger
     */
    var mAppComponent= MyApp.get(mActivity).getAppComponents()

    fun getSeriesCategoryListAPIImpl() {
        val mAlerter=Alerter.create(mActivity)
        if (mUtils.isOnline(mActivity)) {
            mProgressLoader.showLoader(mActivity)
            mAppComponent.getApiHelper().getSeriesOnDemandCategoryList(
                username = UserModel.getUserModel().userInfo?.username.toString(),
                password = UserModel.getUserModel().userInfo?.password.toString()
            ).enqueue(object: Callback<ArrayList<ResponseTVSeriesCategoryList>>{
                override fun onFailure(
                    call: Call<ArrayList<ResponseTVSeriesCategoryList>>,
                    t: Throwable
                ) {
                    mProgressLoader.dismissLoader()
                    mUtils.showAlerter(
                        mAlerter,
                        R.color.colorRedError,
                        R.drawable.ic_error_white_24dp,
                        mActivity.resources.getString(R.string.somethingwentwrong)
                    )
                }

                override fun onResponse(
                    call: Call<ArrayList<ResponseTVSeriesCategoryList>>,
                    response: Response<ArrayList<ResponseTVSeriesCategoryList>>
                ) {
                    if (response.code()==200) {
                        ResponseTVSeriesCategoryList.saveSERIESCategoryList(response.body()!!)
                        getSeriesListAPIImple(mAlerter)
                    } else {
                        mProgressLoader.dismissLoader()
                        mUtils.showAlerter(
                            mAlerter,
                            R.color.colorRedError,
                            R.drawable.ic_error_white_24dp,
                            response.message()
                        )
                    }
                }
            })
        } else {
            mUtils.showAlerter(
                mAlerter,
                R.color.colorRedError,
                R.drawable.ic_error_white_24dp,
                mActivity.resources.getString(R.string.somethingwentwrong)
            )
        }
    }

    fun getSeriesListAPIImple(mAlerter: Alerter) {
        mAppComponent.getApiHelper().getSeriesList(
            username = UserModel.getUserModel().userInfo?.username.toString(),
            password = UserModel.getUserModel().userInfo?.password.toString()
        ).enqueue(object : Callback<ArrayList<ResponseTVSeries>> {
            override fun onFailure(call: Call<ArrayList<ResponseTVSeries>>, t: Throwable) {
                mProgressLoader.dismissLoader()
                mUtils.showAlerter(
                    mAlerter,
                    R.color.colorRedError,
                    R.drawable.ic_error_white_24dp,
                    mActivity.resources.getString(R.string.somethingwentwrong)
                )
            }

            override fun onResponse(
                call: Call<ArrayList<ResponseTVSeries>>,
                response: Response<ArrayList<ResponseTVSeries>>
            ) {
                if (response.code() == 200) {
                    try {
                        Thread(Runnable {
                            for (seriesCategory in ResponseTVSeriesCategoryList.getResponseSERIESCategoryList()) {
                                for (items in response.body()!!) {
                                    if (items.categoryId == seriesCategory.categoryId) {
                                        seriesCategory.series?.add(items)
                                    }
                                }
                            }
                            mActivity.runOnUiThread {
                                mProgressLoader.dismissLoader()
                                mActivity.updateRecycler()
                            }
                        }).start()
                    } catch (e: java.lang.Exception) {
                        mProgressLoader.dismissLoader()
                        e.printStackTrace()
                        mUtils.showAlerter(
                            mAlerter,
                            R.color.colorRedError,
                            R.drawable.ic_error_white_24dp,
                            mActivity.resources.getString(R.string.somethingwentwrong)
                        )
                    }
                } else {
                    mProgressLoader.dismissLoader()
                    mUtils.showAlerter(
                        mAlerter,
                        R.color.colorRedError,
                        R.drawable.ic_error_white_24dp,
                        response.message()
                    )
                }
            }

        })
    }

}