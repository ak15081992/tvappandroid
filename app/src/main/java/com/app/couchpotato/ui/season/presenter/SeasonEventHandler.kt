package com.app.couchpotato.ui.season.presenter

import com.app.couchpotato.MyApp
import com.app.couchpotato.R
import com.app.couchpotato.commonmodel.UserModel
import com.app.couchpotato.customview.loader.LoadingDialog
import com.app.couchpotato.helperutils.Utils
import com.app.couchpotato.ui.season.SeasonActivity
import com.app.couchpotato.ui.season.model.ResponseSeasonList
import com.tapadoo.alerter.Alerter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SeasonEventHandler(private val mActivity:SeasonActivity) {

    /**
     * Utils
     */
    val mUtils= Utils.getUtils()

    /**
     * Assinging LoadingDialog's singleton object into @mProgressLoader
     */
    private val mProgressLoader = LoadingDialog.getLoader()

    /**
     * AppComponent of Dagger
     */
    var mAppComponent= MyApp.get(mActivity).getAppComponents()

    fun getSeasonListAPIImpl() {
        val mAlerter=Alerter.create(mActivity)
        if (mUtils.isOnline(mActivity)) {
            mProgressLoader.setMessage(mActivity.resources.getString(R.string.loadingseasons))
            mProgressLoader.showLoader(mActivity)
            mAppComponent.getApiHelper().getSeasonList(
                username = UserModel.getUserModel().userInfo?.username.toString(),
                password = UserModel.getUserModel().userInfo?.password.toString(),
                seriesId = mActivity.mTVSeries?.seriesId.toString()
            ).enqueue(object: Callback<ResponseSeasonList>{
                override fun onFailure(call: Call<ResponseSeasonList>, t: Throwable) {
                    mProgressLoader.dismissLoader()
                    mUtils.showAlerter(
                        mAlerter,
                        R.color.colorRedError,
                        R.drawable.ic_error_white_24dp,
                        t.localizedMessage!!
                    )
                }

                override fun onResponse(
                    call: Call<ResponseSeasonList>,
                    response: Response<ResponseSeasonList>
                ) {
                    mProgressLoader.dismissLoader()
                    try {
                        if (response.code()==200) {
                            mActivity.mResponseSeasonList=response.body()
                            mActivity.updateUI()
                        } else {
                            mUtils.showAlerter(
                                mAlerter,
                                R.color.colorRedError,
                                R.drawable.ic_error_white_24dp,
                                response.message()
                            )
                        }
                    } catch (e:Exception) {
                        e.printStackTrace()
                        mUtils.showAlerter(
                            mAlerter,
                            R.color.colorRedError,
                            R.drawable.ic_error_white_24dp,
                            mActivity.resources.getString(R.string.somethingwentwrong)
                        )
                    }
                }
            })
        } else {
            mUtils.showAlerter(
                mAlerter,
                R.color.colorRedError,
                R.drawable.ic_error_white_24dp,
                mActivity.resources.getString(R.string.networkerror)
            )
        }
    }
}