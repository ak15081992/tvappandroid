package com.app.couchpotato.ui.tvondemand.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.couchpotato.R
import com.app.couchpotato.ui.tvondemand.TvOnDemandActivity
import com.app.couchpotato.ui.tvondemand.model.ResponseTVSeries
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_movie.view.*


class SeriesAdapter(
    private val mActivity: TvOnDemandActivity,
    private var mSeriesList: ArrayList<ResponseTVSeries>?,
    private val mListener: SeriesClickListener
) : RecyclerView.Adapter<SeriesAdapter.MyViewHolder>() {

    interface SeriesClickListener {
        fun onSeriesClicked(mMovie: ResponseTVSeries?)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_movie, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        Glide.with(mActivity)
            .load(mSeriesList?.get(position)?.cover)
            .centerCrop()
            .into(holder.itemView.ivMovie)
        holder.itemView.setOnClickListener {
            mListener.onSeriesClicked(mSeriesList?.get(position))
        }
    }

    override fun getItemCount(): Int = mSeriesList?.size!!

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view)
}