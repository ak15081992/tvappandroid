package com.app.couchpotato.ui.moviesondemand.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.couchpotato.R
import com.app.couchpotato.ui.moviesondemand.MoviesOnDemandActivity
import com.app.couchpotato.ui.moviesondemand.model.ResponseMovies
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_movie.view.*


class MovieAdapter(
    private val mActivity: MoviesOnDemandActivity,
    private var mMoviesList: ArrayList<ResponseMovies>?,
    private val mListener: MovieClickListener
) : RecyclerView.Adapter<MovieAdapter.MyViewHolder>() {

    interface MovieClickListener {
        fun onMovieClicked(movie: ResponseMovies?)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_movie, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        Glide.with(mActivity)
            .load(mMoviesList?.get(position)?.streamIcon)
            .centerCrop()
            .into(holder.itemView.ivMovie)
        holder.itemView.setOnClickListener {
            mListener.onMovieClicked(mMoviesList?.get(position))
        }
    }

    override fun getItemCount(): Int = mMoviesList?.size!!

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view)
}